const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.sass('resources/sass/app.scss', 'public/css');

mix.sass('resources/sass/global.scss', 'public/frontend/css')
    .sass('resources/sass/fontawesome.scss', 'public/frontend/css')
;

mix.js('resources/js/Frontend/Layouts/Footer/index.js', 'public/frontend/vue/footer.js')
    .js('resources/js/Frontend/Layouts/Header/Main/index.js', 'public/frontend/vue/header/main.js')
    .js('resources/js/Frontend/Layouts/Header/Consumer/index.js', 'public/frontend/vue/header/consumer.js')
    .js('resources/js/Frontend/Layouts/Header/Chef/index.js', 'public/frontend/vue/header/chef.js')
;

mix.js('resources/js/Frontend/Landing/index.js', 'public/frontend/vue/landing.js')
;

mix.js('resources/js/Frontend/Consumer/Index/index.js', 'public/frontend/vue/consumer/index.js')
    .js('resources/js/Frontend/Consumer/Search/index.js', 'public/frontend/vue/consumer/search.js')
    .js('resources/js/Frontend/Consumer/Menulisting/index.js', 'public/frontend/vue/consumer/menulisting.js')
    .js('resources/js/Frontend/Consumer/Menu/index.js', 'public/frontend/vue/consumer/menu.js')
    .js('resources/js/Frontend/Consumer/Chef/index.js', 'public/frontend/vue/consumer/chef.js')
    .js('resources/js/Frontend/Consumer/Promotion/Template/index.js', 'public/frontend/vue/consumer/promotion/template.js')
    .js('resources/js/Frontend/Consumer/Legal/Term/index.js', 'public/frontend/vue/consumer/legal/term.js')
    .js('resources/js/Frontend/Consumer/Legal/Privacy/index.js', 'public/frontend/vue/consumer/legal/privacy.js')
    .js('resources/js/Frontend/Consumer/Account/Register/index.js', 'public/frontend/vue/consumer/account/register.js')
    .js('resources/js/Frontend/Consumer/Contact/Kitchen/index.js', 'public/frontend/vue/consumer/contact/kitchen.js')
    .js('resources/js/Frontend/Consumer/Contact/Delivery/index.js', 'public/frontend/vue/consumer/contact/delivery.js')
    .js('resources/js/Frontend/Consumer/Account/Profile/index.js', 'public/frontend/vue/consumer/account/profile.js')
    .js('resources/js/Frontend/Consumer/Account/GCash/index.js', 'public/frontend/vue/consumer/account/gcash.js')
    .js('resources/js/Frontend/Consumer/Account/Order/index.js', 'public/frontend/vue/consumer/account/order.js')
    .js('resources/js/Frontend/Consumer/Order/Checkout/index.js', 'public/frontend/vue/consumer/order/checkout.js')
    .js('resources/js/Frontend/Consumer/Order/Failed/index.js', 'public/frontend/vue/consumer/order/failed.js')
    .js('resources/js/Frontend/Consumer/Order/Progress/index.js', 'public/frontend/vue/consumer/order/progress.js')
    .js('resources/js/Frontend/Consumer/Order/Review/index.js', 'public/frontend/vue/consumer/order/review.js')
    .js('resources/js/Frontend/Consumer/Cuisine/Template/index.js', 'public/frontend/vue/consumer/cuisine/template.js')
;

mix.js('resources/js/Frontend/Chef/Index/index.js', 'public/frontend/vue/chef/index.js')
;

mix.js('resources/js/Frontend/About/index.js', 'public/frontend/vue/about.js')
;

mix.js('resources/js/Frontend/Event/Index/index.js', 'public/frontend/vue/event/index.js')
;
