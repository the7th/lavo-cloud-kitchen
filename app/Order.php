<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Order extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id', 'promo_code_id', 'sst_amount', 'price', 'payment_status', 'order_status'
    ];
	
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
	public function orderDetail()
    {
        return $this->belongsTo('App\OrderDetail', 'id', 'order_id');
    }

	public function orderDetails()
    {
		if(Auth::user()->type == 'chef')
		{
			return $this->hasMany('App\OrderDetail', 'order_id', 'id')->where('chef_id',Auth::user()->id);
		}
        return $this->hasMany('App\OrderDetail', 'order_id', 'id');
    }
	
	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . strtolower($keyword) . "%";
			return $query->whereHas('user', function($q) use ($key){
								return $q->whereRaw("LOWER(users.name) LIKE ?", [$key]);
							});
        }
			
    }
	
	public function scopeDateRange($query, $from = null, $to = null)
    {
        return $query->where(function ($q) use ($from, $to) {
            if ($from) {
                $q->whereDate('created_at', '>=', $from);
            }
            if ($to) {
                $q->whereDate('created_at', '<=', $to);
            }
        });
    }
    
	public function lamboTrails()
    {
        return $this->hasMany('App\LamboTrail', 'merchant_txn_id', 'merchant_txn_id');
    }
	// public function scopeType($query, $type)
    // {
        // if ($type) {
			// return $query->where('booking_type', $type);
        // }
    // }
}
