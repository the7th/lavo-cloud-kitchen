<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kitchen extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
		'kitchen_area_id', 'name', 'description', 'session_price', 'weekly_price', 'monthly_price', 'settings', 'is_halal', 'room_type', 'category', 'has_settings', 'equipment_fixed', 'assign_automatically', 'equipment_info', 'status'
    ];
	
	public function images()
    {
        return $this->hasMany('App\KitchenImage', 'kitchen_id', 'id');
    }
	
	public function kitchenArea()
    {
        return $this->hasOne('App\KitchenArea', 'id','kitchen_area_id');
    }
}
