<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialTxn extends Model
{
    protected $table = 'financial_txn';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    //
}
