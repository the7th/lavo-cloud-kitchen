<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneTimePassword extends Model
{


    public function scopeExpiry($query, $date)
    {
        return $query->where('time_expiry', '>' , $date);
    }


}
