<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommissionCategoryRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\CommissionCategory;
use Auth;

class CommissionCategoryController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$data = CommissionCategory::keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.commission-categories.index', compact('data','request'));
    }
	
	
	public function create()
    {
		return view('dashboard.commission-categories.create');
    }
	
	public function store(CommissionCategoryRequest $request)
    {
		$data = new CommissionCategory;
		$data->name = $request->name;
		$data->type = $request->type;
		$data->value = $request->value;
		$data->status = $request->status;
		
		if($data->save())
		{
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('commission-categories.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	
	public function edit($id)
    {
		$data = CommissionCategory::find($id);
		return view('dashboard.commission-categories.edit', compact('data'));
    }
	
	public function update(CommissionCategoryRequest $request, $id)
    {
		$data = CommissionCategory::find($id);
		$data->name = $request->name;
		$data->type = $request->type;
		$data->value = $request->value;
		$data->status = $request->status;
		
		if($data->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('commission-categories.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$data = CommissionCategory::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('commission-categories.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
