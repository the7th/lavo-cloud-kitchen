<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Kitchen;
use App\KitchenImage;
use App\KitchenArea;
use App\Image;
use Auth;

class KitchenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/kitchens/images'; 
		$this->kitchen_areas = ['' => "Choose kitchen area"] + KitchenArea::where('status','active')->pluck('name', 'id')->toArray();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		$data = Kitchen::orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.kitchens.index', compact('data','request'));
    }
	
	public function create()
    {
		$kitchen_areas = $this->kitchen_areas;
		return view('dashboard.kitchens.create', compact('kitchen_areas'));
    }
	
	public function edit($id)
    {
		$kitchen_areas = $this->kitchen_areas;
		$data = Kitchen::find($id);
		return view('dashboard.kitchens.edit', compact('data','kitchen_areas'));
    }
	
	public function store(Request $request)
    {
		$kitchen = new Kitchen;
		$kitchen->kitchen_area_id = $request->kitchen_area;
		$kitchen->name = $request->name;
		$kitchen->description = $request->description;
		$kitchen->session_price = $request->session_price;
		$kitchen->weekly_price = $request->weekly_price;
		$kitchen->monthly_price = $request->monthly_price;
		$kitchen->has_settings = 'yes';
		$kitchen->is_halal = $request->is_halal;
		$kitchen->category = $request->category;
		$kitchen->room_type = $request->room_type;
		$kitchen->equipment_fixed = $request->equipment_fixed;
		$kitchen->assign_automatically = $request->assign_automatically;
		$kitchen->equipment_info = $request->equipment_info;
		$kitchen->status = $request->status;
		
		if($kitchen->save())
		{
			if ($images = $request->file('images')) {
				$destinationPath = $this->destinationPath;
				$image_counter = 0;
				foreach($images as $image)
				{
					$filename = $kitchen->id . time() . $image_counter . "." . $image->getClientOriginalExtension();
					$image->move($destinationPath, $filename);

					$image = new KitchenImage;
					$image->kitchen_id = $kitchen->id;
					$image->path = $this->destinationPath;
					$image->filename = $filename;
					$image->save();
					
					$image_library = new Image;
					$image_library->image_category_id = 2;
					$image_library->url = env('APP_URL').'/'.$image->path.'/'.$image->filename;
					$image_library->save();
					
					$image_counter++;
				}
			}
			
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('kitchens.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function update(Request $request, $id)
    {
		$kitchen = Kitchen::find($id);
		$kitchen->kitchen_area_id = $request->kitchen_area;
		$kitchen->name = $request->name;
		$kitchen->description = $request->description;
		$kitchen->session_price = $request->session_price;
		$kitchen->weekly_price = $request->weekly_price;
		$kitchen->monthly_price = $request->monthly_price;
		$kitchen->has_settings = 'yes';
		$kitchen->is_halal = $request->is_halal;
		$kitchen->category = $request->category;
		$kitchen->room_type = $request->room_type;
		$kitchen->equipment_fixed = $request->equipment_fixed;
		$kitchen->assign_automatically = $request->assign_automatically;
		$kitchen->equipment_info = $request->equipment_info;
		$kitchen->status = $request->status;
		
		if($kitchen->save())
		{
			if ($images = $request->file('images')) {
				// list($width, $height) = getimagesize($request->file('image'));
				
				$destinationPath = $this->destinationPath;
				$image_counter = 0;
				foreach($images as $image)
				{
					$filename = $kitchen->id . time() . $image_counter . "." . $image->getClientOriginalExtension();
					$image->move($destinationPath, $filename);

					$image = new KitchenImage;
					$image->kitchen_id = $kitchen->id;
					$image->path = $this->destinationPath;
					$image->filename = $filename;
					$image->save();
					
					$image_library = new Image;
					$image_library->image_category_id = 2;
					$image_library->url = env('APP_URL').'/'.$image->path.'/'.$image->filename;
					$image_library->save();
					
					$image_counter++;
				}
			}
			
			if($request->delete_images)
			{
				foreach($request->delete_images as $image_id)
				{
					$image = KitchenImage::find($image_id);
					$img_filePath = $image->path.'/'.$image->filename;
					// if(Storage::disk('s3')->exists($img_filePath)) {
						// Storage::disk('s3')->delete($img_filePath);
					// }
					if (file_exists($img_filePath)) {
						unlink($img_filePath);
					}
					$image->delete();
				}
				
			}
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('kitchens.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
