<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Review;
use Auth;

class ReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

		$data = Review::keyword($request->keyword)
					->type($request->type)
					->dateRange($request->from, $request->to)
					->orderBy('created_at','desc')
					->paginate(15);


		return view('dashboard.reviews.index', compact('data','request'));
    }

	public function edit($id)
    {
		$data = Review::find($id);
		return view('dashboard.reviews.edit', compact('data'));
    }

	public function update(Request $request, $id)
    {
		$data = Review::find($id);
		$data->approval_status = $request->approval_status;

		if($data->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('reviews.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function approval(Request $request, $id)
    {
		$data = Review::find($id);
		$data->approval_status = $request->approval;

		if($data->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('reviews.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
