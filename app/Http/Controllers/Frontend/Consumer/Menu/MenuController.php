<?php

    namespace App\Http\Controllers\Frontend\Consumer\Menu;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class MenuController extends Controller
    {

        public function index(Request $request, $menuId = null)
        {
            return view('Frontend/Consumer/Menu/index', ['menuId' => $menuId]);
        }
    }
