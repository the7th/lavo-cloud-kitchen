<?php

    namespace App\Http\Controllers\Frontend\Consumer\Index;

    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Index/index');
        }
    }
