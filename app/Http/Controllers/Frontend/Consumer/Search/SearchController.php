<?php

    namespace App\Http\Controllers\Frontend\Consumer\Search;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class SearchController extends Controller
    {
        public function index(Request $request, $searchStr = null)
        {
            return view('Frontend/Consumer/Search/index', ['searchStr' => $searchStr]);
        }
    }
