<?php

    namespace App\Http\Controllers\Frontend\Consumer\Contact\Delivery;

    use App\Http\Controllers\Controller;

    class DeliveryController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Contact/Delivery/index');
        }
    }
