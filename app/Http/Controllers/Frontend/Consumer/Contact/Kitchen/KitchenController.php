<?php

    namespace App\Http\Controllers\Frontend\Consumer\Contact\Kitchen;

    use App\Http\Controllers\Controller;

    class KitchenController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Contact/Kitchen/index');
        }
    }
