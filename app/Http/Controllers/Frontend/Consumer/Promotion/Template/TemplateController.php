<?php

    namespace App\Http\Controllers\Frontend\Consumer\Promotion\Template;

    use App\Http\Controllers\Controller;

    class TemplateController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Promotion/Template/index');
        }
    }
