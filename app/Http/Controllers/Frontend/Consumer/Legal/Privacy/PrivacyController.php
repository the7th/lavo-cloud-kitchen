<?php

    namespace App\Http\Controllers\Frontend\Consumer\Legal\Privacy;

    use App\Http\Controllers\Controller;

    class PrivacyController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Legal/Privacy/index');
        }
    }
