<?php

    namespace App\Http\Controllers\Frontend\Consumer\Legal\Term;

    use App\Http\Controllers\Controller;

    class TermController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Legal/Term/index');
        }
    }
