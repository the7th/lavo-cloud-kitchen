<?php

    namespace App\Http\Controllers\Frontend\Consumer\Order\Checkout;

    use App\Http\Controllers\Controller;

    class CheckoutController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Order/Checkout/index');
        }
    }
