<?php

    namespace App\Http\Controllers\Frontend\Consumer\Order\Failed;

    use App\Http\Controllers\Controller;

    class FailedController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Order/Failed/index');
        }
    }
