<?php

    namespace App\Http\Controllers\Frontend\Consumer\Order\Progress;

    use App\Http\Controllers\Controller;

    class ProgressController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Order/Progress/index');
        }
    }
