<?php

    namespace App\Http\Controllers\Frontend\Consumer\Order\Review;

    use App\Http\Controllers\Controller;

    class ReviewController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Order/Review/index');
        }
    }
