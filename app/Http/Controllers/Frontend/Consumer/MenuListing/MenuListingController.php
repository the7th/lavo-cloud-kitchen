<?php

    namespace App\Http\Controllers\Frontend\Consumer\MenuListing;

    use App\Http\Controllers\Controller;

    class MenuListingController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/MenuListing/index');
        }
    }
