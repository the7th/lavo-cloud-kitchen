<?php

    namespace App\Http\Controllers\Frontend\Consumer\Account\Profile;

    use App\Http\Controllers\Controller;

    class ProfileController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Account/Profile/index');
        }
    }
