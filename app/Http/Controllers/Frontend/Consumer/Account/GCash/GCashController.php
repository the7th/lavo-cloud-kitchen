<?php

    namespace App\Http\Controllers\Frontend\Consumer\Account\GCash;

    use App\Http\Controllers\Controller;

    class GCashController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Account/GCash/index');
        }
    }
