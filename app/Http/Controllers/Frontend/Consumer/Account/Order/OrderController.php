<?php

    namespace App\Http\Controllers\Frontend\Consumer\Account\Order;

    use App\Http\Controllers\Controller;

    class OrderController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Account/Order/index');
        }
    }
