<?php

    namespace App\Http\Controllers\Frontend\Consumer\Account\Register;

    use App\Http\Controllers\Controller;

    class RegisterController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Account/Register/index');
        }
    }
