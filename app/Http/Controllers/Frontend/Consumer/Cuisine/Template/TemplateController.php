<?php

    namespace App\Http\Controllers\Frontend\Consumer\Cuisine\Template;

    use App\Http\Controllers\Controller;

    class TemplateController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Cuisine/Template/index');
        }
    }
