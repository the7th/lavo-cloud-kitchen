<?php

    namespace App\Http\Controllers\Frontend\Consumer\Chef;

    use App\Http\Controllers\Controller;

    class ChefController extends Controller
    {
        public function index()
        {
            return view('Frontend/Consumer/Chef/index');
        }
    }
