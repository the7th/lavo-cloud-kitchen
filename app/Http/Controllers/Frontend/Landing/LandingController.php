<?php

    namespace App\Http\Controllers\Frontend\Landing;

    use App\Http\Controllers\Controller;

    class LandingController extends Controller
    {
        public function index()
        {
            return view('Frontend/Landing/index');
        }
    }
