<?php

    namespace App\Http\Controllers\Frontend\About;

    use App\Http\Controllers\Controller;

    class AboutController extends Controller
    {
        public function index()
        {
            return view('Frontend/About/index');
        }
    }
