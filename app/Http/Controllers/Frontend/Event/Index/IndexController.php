<?php

    namespace App\Http\Controllers\Frontend\Event\Index;

    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            return view('Frontend/Event/Index/index');
        }
    }
