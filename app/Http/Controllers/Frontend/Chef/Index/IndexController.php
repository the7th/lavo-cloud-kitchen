<?php

    namespace App\Http\Controllers\Frontend\Chef\Index;

    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            return view('Frontend/Chef/Index/index');
        }

        public function cubaan()
        {
            return view('Frontend/Chef/Cubaan/cubaan');
        }
    }
