<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BannerImageRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Menu;
use App\MenuSetting;
use App\MenuSettingItem;
use App\MenuSettingSubItem;
use Auth;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class MenuSettingSubItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/menu-settings/images';
    }

    public function index(Request $request, $menu_setting_item_id)
    {
		$menu_setting_item = MenuSettingItem::find($menu_setting_item_id);
		$data = MenuSettingSubItem::where('menu_setting_item_id',$menu_setting_item_id)->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.menus.settings.items.sub-items.index', compact('menu_setting_item','data','request'));
    }


	public function create($menu_setting_item_id)
    {
		$menu_setting_item = MenuSettingItem::find($menu_setting_item_id);
		return view('dashboard.menus.settings.items.sub-items.create',compact('menu_setting_item'));
    }

	public function store(Request $request, $menu_setting_item_id)
    {
		$menu_setting_sub_item = new MenuSettingSubItem;
		$menu_setting_sub_item->menu_setting_item_id = $menu_setting_item_id;
		$menu_setting_sub_item->name = $request->name;
		$menu_setting_sub_item->price = $request->price;

		if($menu_setting_sub_item->save())
		{
            $message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menu-setting-sub-items.index',$menu_setting_item_id)->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function edit($id)
    {
		$data = MenuSettingSubItem::find($id);
		$menu_setting_item = $data->menuSettingItem;
		return view('dashboard.menus.settings.items.sub-items.edit', compact('data','menu_setting_item'));
    }

	public function update(Request $request, $id)
    {
		$menu_setting_sub_item = MenuSettingSubItem::find($id);
		$menu_setting_sub_item->name = $request->name;
		$menu_setting_sub_item->price = $request->price;

		if($menu_setting_sub_item->save())
		{
            $message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menu-setting-sub-items.index',$menu_setting_sub_item->menu_setting_item_id)->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function delete($id)
    {
		$data = MenuSettingSubItem::find($id);
		$menu_setting_item_id = $data->menuSettingItem->id;
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menu-setting-sub-items.index',$menu_setting_item_id)->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
