<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChefRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\CuisineType;
use App\ChefCuisineType;
use App\ChefCompany;
use App\DeliveryTime;
use Auth;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ChefController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->cuisine_types = CuisineType::where('status','active')->pluck('name','id');
        $this->delivery_times = ['' => "Choose Delivery Schedule"] + DeliveryTime::where('status','active')->pluck('title','id')->toArray();
		$this->destinationPath = 'uploads/profiles/images'; 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		$data = User::where('type','chef')
					->keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		
		if($request->export) {
			return Excel::download(new UsersExport($request,'chef'), 'chefs.csv');
		}
		return view('dashboard.chefs.index', compact('data','request'));
    }
	
	public function create()
    {
		$cuisine_types = $this->cuisine_types;
		$delivery_times = $this->delivery_times;
		
		return view('dashboard.chefs.create', compact('cuisine_types','delivery_times'));
    }
	
	public function store(ChefRequest $request)
    {
		$chef = new User;
		$chef->type = 'chef';
		$chef->chef_account_type = $request->chef_account_type;
		$chef->brand_name = $request->brand_name;
		$chef->name = $request->name;
		$chef->contact_number = $request->contact_number;
		$chef->email = $request->email;
		$chef->position = $request->position;
		$chef->base_type = $request->base_type;
		$chef->password = Hash::make($request['password']);
		$chef->description = $request->description;
		$chef->url = $request->url;
		$chef->status = $request->status;
		
		if($chef->save())
		{
			if($request->base_type == 'Company')
			{
				$chef_company = new ChefCompany;
				$chef_company->user_id = $chef->id;
				$chef_company->company_name = $request->company_name;
				$chef_company->company_registration_number = $request->company_registration_number;
				$chef_company->delivery_time_id = $request->delivery_time_id ? $request->delivery_time_id : 0;
				$chef_company->save();
			}
			
			foreach($request->cuisine_types as $cuisine_type_id)
			{
				$chef_cuisine_type = new ChefCuisineType;
				$chef_cuisine_type->user_id = $chef->id;
				$chef_cuisine_type->cuisine_type_id = $cuisine_type_id;
				$chef_cuisine_type->save();
			}
			
			$destinationPath = $this->destinationPath;
			$image = $request->file('image');
			$filename = $chef->id . time() . "." . $image->getClientOriginalExtension();
			$image->move($destinationPath, $filename);
			
			$chef->image_path = $this->destinationPath;
			$chef->image_filename = $filename;
			$chef->save();
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('chefs.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function edit($id)
    {
		$data = User::find($id);
		$cuisine_types = $this->cuisine_types;
		$delivery_times = $this->delivery_times;
		
		return view('dashboard.chefs.edit', compact('data','cuisine_types','delivery_times'));
    }
	
	public function update(ChefRequest $request, $id)
    {
		$chef = User::find($id);
		$chef->type = 'chef';
		$chef->chef_account_type = $request->chef_account_type;
		$chef->brand_name = $request->brand_name;
		$chef->name = $request->name;
		$chef->contact_number = $request->contact_number;
		$chef->email = $request->email;
		$chef->position = $request->position;
		$chef->base_type = $request->base_type;
		$chef->description = $request->description;
		$chef->url = $request->url;
		$chef->status = $request->status;
		
		if($request['password'] != "")
		{
			$chef->password = Hash::make($request['password']);
		}
		
		if($chef->save())
		{
			if($request->base_type == 'Company')
			{
				$chef_company = ChefCompany::where('user_id',$chef->id)->first();
				if(!$chef_company)
				{
					$chef_company = new ChefCompany;
				}
				$chef_company->user_id = $chef->id;
				$chef_company->company_name = $request->company_name;
				$chef_company->company_registration_number = $request->company_registration_number;
				$chef_company->delivery_time_id = $request->delivery_time_id ? $request->delivery_time_id : 0;
				$chef_company->save();
			}
			
			$remove_cuisine_types = ChefCuisineType::where('user_id',$chef->id)->delete();
			foreach($request->cuisine_types as $cuisine_type_id)
			{
				$chef_cuisine_type = new ChefCuisineType;
				$chef_cuisine_type->user_id = $chef->id;
				$chef_cuisine_type->cuisine_type_id = $cuisine_type_id;
				$chef_cuisine_type->save();
			}
			
			if ($image = $request->file('image')) 
			{
				$img_filePath = $chef->path.'/'.$chef->filename;
				if ($chef->path && file_exists($img_filePath)) {
					unlink($img_filePath);
				}
				
				$destinationPath = $this->destinationPath;
				$image = $request->file('image');
				$filename = $chef->id . time() . "." . $image->getClientOriginalExtension();
				$image->move($destinationPath, $filename);
				
				$chef->image_path = $this->destinationPath;
				$chef->image_filename = $filename;
				$chef->save();
			}	
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('chefs.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$chef = User::find($id);
		$chef->brand_name = 'deleted-'.$chef->id;
		$chef->email = 'deleted-'.$chef->id;
		$chef->contact_number = 'deleted-'.$chef->id;
		$chef->save();
		if($chef->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('chefs.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function export(Request $request)
    {
		$data = User::where('type','chef')
					->keyword($request->keyword)
					->orderBy('created_at','desc')
					->get();
		return Excel::download(new UsersExport($data), 'users.csv');
    }
    // public function export() 
    // {
        // return Excel::download(new UsersExport, 'users.csv');
    // }
   
    public function import() 
    {
        Excel::import(new UsersImport,request()->file('file'));
           
        return back();
    }
}
