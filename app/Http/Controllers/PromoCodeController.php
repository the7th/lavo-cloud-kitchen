<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PromoCodeRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\PromoCode;
use Auth;

class PromoCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$data = PromoCode::keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.promo-codes.index', compact('data','request'));
    }
	
	
	public function create()
    {
		return view('dashboard.promo-codes.create');
    }
	
	public function store(PromoCodeRequest $request)
    {
		$promo_code = new PromoCode;
		$promo_code->code = $request->code;
		$promo_code->redemption_amount = $request->redemption_amount;
		$promo_code->discount_type = $request->discount_type;
		$promo_code->discount_value = $request->discount_value;
		$promo_code->start_date = $request->start_date;
		$promo_code->end_date = $request->end_date;
		$promo_code->status = $request->status;
		
		if($promo_code->save())
		{
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('promo-codes.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	
	public function edit($id)
    {
		$data = PromoCode::find($id);
		return view('dashboard.promo-codes.edit', compact('data'));
    }
	
	public function update(PromoCodeRequest $request, $id)
    {
		$promo_code = PromoCode::find($id);
		$promo_code->code = $request->code;
		$promo_code->redemption_amount = $request->redemption_amount;
		$promo_code->discount_type = $request->discount_type;
		$promo_code->discount_value = $request->discount_value;
		$promo_code->start_date = $request->start_date;
		$promo_code->end_date = $request->end_date;
		$promo_code->status = $request->status;
		
		if($promo_code->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('promo-codes.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$data = PromoCode::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('promo-codes.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
