<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KitchenAreaRequest;
use Illuminate\Support\Facades\Session;
use App\KitchenArea;
use Auth;

class KitchenAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/kitchens/images'; 
    }

    public function index(Request $request)
    {
		$data = KitchenArea::orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.kitchen-areas.index', compact('data','request'));
    }
	
	public function create()
    {
		return view('dashboard.kitchen-areas.create');
    }
	
	public function store(KitchenAreaRequest $request)
    {
		$kitchen_area = new KitchenArea;
		$kitchen_area->name = $request->name;
		$kitchen_area->address = $request->address;
		$kitchen_area->state = $request->state;
		$kitchen_area->city = $request->city;
		$kitchen_area->postal_code = $request->postal_code;
		$kitchen_area->long = $request->long;
		$kitchen_area->lat = $request->lat;
		$kitchen_area->status = $request->status;
		
		if($kitchen_area->save())
		{
			
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('kitchen-areas.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function edit($id)
    {
		$data = KitchenArea::find($id);
		return view('dashboard.kitchen-areas.edit', compact('data'));
    }
	
	public function update(KitchenAreaRequest $request, $id)
    {
		$kitchen_area = KitchenArea::find($id);
		$kitchen_area->name = $request->name;
		$kitchen_area->address = $request->address;
		$kitchen_area->state = $request->state;
		$kitchen_area->city = $request->city;
		$kitchen_area->postal_code = $request->postal_code;
		$kitchen_area->long = $request->long;
		$kitchen_area->lat = $request->lat;
		$kitchen_area->status = $request->status;
		
		if($kitchen_area->save())
		{
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('kitchen-areas.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
