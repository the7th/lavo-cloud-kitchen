<?php
   
namespace App\Http\Controllers;
   
use App\Event;
use App\Booking;
use Illuminate\Http\Request;
use Carbon\Carbon as Carbon;
use Redirect,Response;
   
class FullCalendarController extends Controller
{
 
    public function index(Request $request)
    {
		$date = Carbon::now()->format('Y-m-d');
		if($request->date)
		{
			$date = $request->date;
		}
		
		$data = Booking::where('booking_start_date', '<=', $date)
				->where('booking_end_date', '>=', $date)
				->get();
				
		$year = now()->format('Y');
		$month = now()->format('F');
		$month_no = now()->format('m');
		$current_day = now()->format('d');
		$days_in_month = Carbon::now()->daysInMonth;
		if($request->to)
		{
			switch ($request->to) {
				case "prev":
					$year = Carbon::parse($request->year.' '.$request->month)->subMonths(1)->format('Y');
					$month = Carbon::parse($request->year.' '.$request->month)->subMonths(1)->format('F');
					$month_no = Carbon::parse($request->year.' '.$request->month)->subMonths(1)->format('m');
					$current_day = Carbon::now()->format('Y F') == Carbon::parse($request->year.' '.$request->month)->subMonths(1)->format('Y F') ? now()->format('d') : null;
					$days_in_month = Carbon::parse($request->year.' '.$request->month)->subMonths(1)->daysInMonth;
					break;
				case "next":
					$year = Carbon::parse($request->year.' '.$request->month)->addMonths(1)->format('Y');
					$month = Carbon::parse($request->year.' '.$request->month)->addMonths(1)->format('F');
					$month_no = Carbon::parse($request->year.' '.$request->month)->addMonths(1)->format('m');
					$current_day = Carbon::now()->format('Y F') == Carbon::parse($request->year.' '.$request->month)->addMonths(1)->format('Y F') ? now()->format('d') : null;
					$days_in_month = Carbon::parse($request->year.' '.$request->month)->addMonths(1)->daysInMonth;
					break;
				case "none":
					$year = Carbon::parse($request->year.' '.$request->month)->format('Y');
					$month = Carbon::parse($request->year.' '.$request->month)->format('F');
					$month_no = Carbon::parse($request->year.' '.$request->month)->format('m');
					$current_day = Carbon::now()->format('Y F') == Carbon::parse($request->year.' '.$request->month)->format('Y F') ? now()->format('d') : null;
					$days_in_month = Carbon::parse($request->year.' '.$request->month)->daysInMonth;
					break;
			}
		}
			
		$calendar['year'] = $year;
		$calendar['month'] = $month;
		$calendar['month_no'] = $month_no;
		$calendar['current_day'] = $current_day;
		$calendar['current_date'] = Carbon::now()->format('Y-m-d');
		$counter = 0;
		for($day=1;$day<=$days_in_month;$day++)
		{
			$calendar['days'][$counter]['number'] = $day;
			$day_date = Carbon::parse($calendar['year'].'-'.$calendar['month_no'].'-'.$day)->format('Y-m-d');
			$calendar['days'][$counter]['day_date'] = $day_date;
			$calendar['days'][$counter]['has_booking'] = Booking::where('booking_start_date', '<=', $day_date)->where('booking_end_date','>=', $day_date)->exists() ? 'yes' : 'no';
			$calendar['days'][$counter]['name'] = substr(Carbon::parse($calendar['year'].'-'.$calendar['month_no'].'-'.$day)->format('l'),0,2);
			$counter++;
		}
		// return $calendar;
		$weekdays = [
			1=>'Mo',
			2=>'Tu',
			3=>'We',
			4=>'Th',
			5=>'Fr',
			6=>'Sa',
			7=>'Su',
		];
			
        return view('dashboard.bookings.calendar', compact('calendar','weekdays','data'));
    }
	
	// public function index()
    // {
        // if(request()->ajax()) 
        // {
 
         // $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
         // $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
 
         // $data = Event::whereDate('start', '>=', $start)->whereDate('end',   '<=', $end)->get(['id','title','start', 'end']);
         // return Response::json($data);
        // }
        // return view('dashboard.bookings.calendar');
    // }
    
   
    public function create(Request $request)
    {  
        $event = new Event; 
        $event->title = $request->title; 
        $event->start = $request->start; 
        $event->end = $request->end; 
        $event->save();    
        return Response::json($event);
    }
     
 
    public function update(Request $request)
    {   
        $where = array('id' => $request->id);
        $updateArr = ['title' => $request->title,'start' => $request->start, 'end' => $request->end];
        $event  = Event::where($where)->update($updateArr);
 
        return Response::json($event);
    } 
 
 
    public function delete(Request $request)
    {
        $event = Event::where('id',$request->id)->delete();
   
        return Response::json($event);
    }    
 
 
}