<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BannerImageRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Menu;
use App\MenuSetting;
use Auth;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class MenuSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/menu-settings/images';
    }

    public function index(Request $request, $menu_id)
    {
		$menu = Menu::find($menu_id);
		$data = MenuSetting::where('menu_id',$menu_id)->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.menus.settings.index', compact('menu','data','request'));
    }


	public function create($menu_id)
    {
		$menu = Menu::find($menu_id);
		return view('dashboard.menus.settings.create',compact('menu'));
    }

	public function store(Request $request, $menu_id)
    {
		$menu_setting = new MenuSetting;
		$menu_setting->menu_id = $menu_id;
		$menu_setting->name = $request->name;
		$menu_setting->description = $request->description;

		if($menu_setting->save())
		{
			if($request->file('image'))
			{
				$destinationPath = $this->destinationPath;
				$image = $request->file('image');
				$filename = $menu_setting->id . time() . "." . $image->getClientOriginalExtension();
				$image->move($destinationPath, $filename);

				$menu_setting->image_path = $this->destinationPath;
				$menu_setting->image_filename = $filename;
				$menu_setting->save();

				$optimizer_chain = OptimizerChainFactory::create();
				$optimizer_chain->optimize($menu_setting->image_path.'/'.$menu_setting->image_filename);
			}
            $message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menu-settings.index',$menu_id)->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function edit($id)
    {
		$data = MenuSetting::find($id);
		$menu = $data->menu;
		return view('dashboard.menus.settings.edit', compact('data','menu'));
    }

	public function update(Request $request, $id)
    {
		$menu_setting = MenuSetting::find($id);
		$menu_setting->name = $request->name;
		$menu_setting->description = $request->description;

		if($menu_setting->save())
		{
			if($request->delete_image)
			{
				$img_filePath = $menu_setting->image_path.'/'.$menu_setting->image_filename;
				if (file_exists($img_filePath)) {
					unlink($img_filePath);
				}
				$menu_setting->image_path = null;
				$menu_setting->image_filename = null;
				$menu_setting->save();
			}
			
			if ($image = $request->file('image'))
			{
				if($menu_setting->image_path)
				{
					$img_filePath = $menu_setting->image_path.'/'.$menu_setting->image_filename;
					if (file_exists($img_filePath)) {
						unlink($img_filePath);
					}
				}

				$destinationPath = $this->destinationPath;
				$image = $request->file('image');
				$filename = $menu_setting->id . time() . "." . $image->getClientOriginalExtension();
				$image->move($destinationPath, $filename);

				$menu_setting->image_path = $this->destinationPath;
				$menu_setting->image_filename = $filename;
				$menu_setting->save();

				$optimizer_chain = OptimizerChainFactory::create();
				$optimizer_chain->optimize($menu_setting->image_path.'/'.$menu_setting->image_filename);
			}
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menu-settings.index',$menu_setting->menu_id)->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function delete($id)
    {
		$data = MenuSetting::find($id);
		$menu_id = $data->menu->id;
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menu-settings.index',$menu_id)->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
