<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BannerImageRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\BannerImage;
use App\Image;
use Auth;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/banners/images';
    }

    public function index(Request $request)
    {
		$data = BannerImage::orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.banners.index', compact('data','request'));
    }


	public function create()
    {
		return view('dashboard.banners.create');
    }

	public function store(BannerImageRequest $request)
    {
		$banner_image = new BannerImage;
		$banner_image->start_date = $request->start_date;
		$banner_image->end_date = $request->end_date;
		$banner_image->url = $request->url;
		$banner_image->status = $request->status;

		if($banner_image->save())
		{
			$destinationPath = $this->destinationPath;
			$image = $request->file('image');
			$filename = $banner_image->id . time() . "." . $image->getClientOriginalExtension();
			$image->move($destinationPath, $filename);

			$banner_image->path = $this->destinationPath;
			$banner_image->filename = $filename;
			$banner_image->save();

			$image_library = new Image;
			$image_library->image_category_id = 3; //id for banner image category
			$image_library->url = env('APP_URL').'/'.$banner_image->path.'/'.$banner_image->filename;
			$image_library->save();

            $optimizer_chain = OptimizerChainFactory::create();
            $optimizer_chain->optimize($banner_image->path.'/'.$banner_image->filename);

            $message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('banners.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function edit($id)
    {
		$data = BannerImage::find($id);
		return view('dashboard.banners.edit', compact('data'));
    }

	public function update(BannerImageRequest $request, $id)
    {
		$banner_image = BannerImage::find($id);
		$banner_image->start_date = $request->start_date;
		$banner_image->end_date = $request->end_date;
		$banner_image->url = $request->url;
		$banner_image->status = $request->status;

		if($banner_image->save())
		{
			if ($image = $request->file('image'))
			{
				$img_filePath = $banner_image->path.'/'.$banner_image->filename;
				if (file_exists($img_filePath)) {
					unlink($img_filePath);
				}

				$destinationPath = $this->destinationPath;
				$filename = $banner_image->id . time() . "." . $image->getClientOriginalExtension();
				$image->move($destinationPath, $filename);

				$banner_image->path = $this->destinationPath;
				$banner_image->filename = $filename;
				$banner_image->save();

				$image_library = new Image;
				$image_library->image_category_id = 3; //id for banner image category
				$image_library->url = env('APP_URL').'/'.$banner_image->path.'/'.$banner_image->filename;
				$image_library->save();
			}
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('banners.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function delete($id)
    {
		$data = BannerImage::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('banners.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
