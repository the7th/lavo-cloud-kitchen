<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\UserAddress;
use Auth;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ConsumerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		$data = User::where('type','consumer')
					->keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		if($request->export) {
			return Excel::download(new UsersExport($request,'consumer'), 'consumers.csv');
		}
		return view('dashboard.consumers.index', compact('data','request'));
    }
	
	public function edit($id)
    {
		$data = User::find($id);
		return view('dashboard.consumers.edit', compact('data'));
    }
	
	public function update(ConsumerRequest $request, $id)
    {
		$consumer = User::find($id);
		$consumer->name = $request->name;
		$consumer->employee_id = $request->employee_id;
		$consumer->department = $request->department;
		$consumer->shift = $request->shift;
		$consumer->email = $request->email;
		$consumer->nric = $request->nric;
		$consumer->company = $request->company;
		if($request['password'] != "")
		{
			$consumer->password = Hash::make($request['password']);
		}
		$consumer->status = $request->status;
		
		if($consumer->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('consumers.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$data = User::find($id);
		$data->nric = 'deleted-'.$data->id;
		$data->email = 'deleted-'.$data->id;
		$data->save();
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('consumers.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
