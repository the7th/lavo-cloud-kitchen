<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Order;
use App\OrderDetail;
use App\FinancialTxn;
use App\UserWallet;
use Auth;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		if ($user->isAdmin()) {
			$data = Order::keyword($request->keyword)
					->dateRange($request->from, $request->to)
					->orderBy('created_at','desc')
					->paginate(15);
        }
		else
		{
			$data = Order::select('orders.*')
					->keyword($request->keyword)
					->dateRange($request->from, $request->to)
					->join('order_details', 'order_details.order_id', '=', 'orders.id')
					->where('order_details.chef_id',$user->id)
					->where('orders.payment_status','paid')
					->groupBy('orders.id')
					->orderBy('orders.created_at','desc')
					->paginate(15);
		}
		return view('dashboard.orders.index', compact('data','request'));
    }

	public function calendar()
    {

		if(request()->ajax())
        {

         $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
         $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');

         $data = Booking::whereDate('booking_start_date', '>=', $start)->whereDate('booking_end_date',   '<=', $end)->get(['id','user_id','booking_start_date', 'booking_end_date']);
         return Response::json($data);
        }
        return view('dashboard.bookings.calendar');
    }

	public function edit($id)
    {
		$deliveryInfo = [];

		$data = Order::find($id);
		if(!Auth::user()->isAdmin())
		{
			$items = $data->orderDetails->where('chef_id',Auth::user()->id);
		}
		else
		{
			$items = $data->orderDetails;
			$deliveryInfo = $data->lamboTrails()->orderBy('jobstatustime', 'asc')->get();
		}

		$options = [];
		$counter = 0;
		foreach($items as $item)
		{
			$options[$item->id] = json_decode($item->data,true);
			$counter++;
		}

		return view('dashboard.orders.edit', compact('data','items','options','deliveryInfo'));
    }

	public function update(Request $request, $id)
    {
		$order = Order::find($id);
		$order->payment_status = $request->payment_status;
		$order->order_status = $request->order_status;

		if($order->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('orders.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function updateStatus(Request $request, $id)
    {
		$order = Order::find($id);
		$order->order_status = $request->order_status;

		if($order->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->back()->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function refund(Request $request, $id)
    {
		$order = Order::find($id);
		if($order)
		{
			if($order->payment_status == 'unpaid')
			{
				$message = 'The order payment has not been made.';
				Session::flash('alert-class', 'alert-danger');
				return redirect()->back()->withMessage($message);
			}
			$finance_txn = FinancialTxn::where('order_id',$order->id)->first();

			if($finance_txn)
			{
				$finance_txn = new FinancialTxn;
				$finance_txn->order_id = $order->id;
				$finance_txn->user_id = $order->user_id;
				$finance_txn->type = 'debit';
				$finance_txn->amount = $order->price;
				$finance_txn->save();

				$user_wallet = UserWallet::where('user_id',$order->user_id)->first();
				if(!$user_wallet)
				{
					$user_wallet = new UserWallet;
					$user_wallet->user_id = $order->user_id;
					$user_wallet->balance += $order->price;
				}
				else
				{
					$user_wallet->balance += $order->price;
				}
				if($user_wallet->save())
				{
					$order->payment_status = 'refunded';
					$order->save();

					$message = 'Record has been updated successfully.';
					Session::flash('alert-class', 'alert-success');
					return redirect()->back()->withMessage($message);
				}
			}
			$message = 'Order has been refunded.';
			Session::flash('alert-class', 'alert-danger');
			return redirect()->back()->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
