<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PromotionRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Promotion;
use App\MenuCategory;
use App\ChefCompany;
use Auth;

class PromotionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$data = Promotion::keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.promotions.index', compact('data','request'));
    }
	
	
	public function create()
    {
		$menu_categories = ['' => "None"] + MenuCategory::where('status','active')->pluck('name', 'id')->toArray();
		$chef_companies = ['' => "None"] + ChefCompany::pluck('company_name', 'id')->toArray();

		return view('dashboard.promotions.create', compact('menu_categories','chef_companies'));
    }
	
	public function store(PromotionRequest $request)
    {
		$promotion = new Promotion;
		$promotion->title = $request->title;
		$promotion->type = $request->type;
		$promotion->ref_type = $request->cuisine_ref_id ? 'menu_category':'chef_company';
		$promotion->ref_id = $request->free_delivery_type == 'cuisine' ? $request->cuisine_ref_id:$request->brand_ref_id;
		$promotion->rate_type = $request->rate_type;
		$promotion->rate_value = $request->rate_value;
		$promotion->start_date = $request->start_date;
		$promotion->end_date = $request->end_date;
		$promotion->status = $request->status;
		
		if($promotion->save())
		{
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('promotions.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	
	public function edit($id)
    {
		$data = Promotion::find($id);
		$menu_categories = ['' => "None"] + MenuCategory::where('status','active')->pluck('name', 'id')->toArray();
		$chef_companies = ['' => "None"] + ChefCompany::pluck('company_name', 'id')->toArray();
		return view('dashboard.promotions.edit', compact('data','menu_categories','chef_companies'));
    }
	
	public function update(PromotionRequest $request, $id)
    {
		$promotion = Promotion::find($id);
		$promotion->title = $request->title;
		$promotion->type = $request->type;
		$promotion->ref_type = $request->free_delivery_type == 'cuisine' ? 'menu_category':'chef_company';
		$promotion->ref_id = $request->free_delivery_type == 'cuisine' ? $request->cuisine_ref_id:$request->brand_ref_id;
		$promotion->rate_type =$request->type != 'free_delivery' ? $request->rate_type:null;
		$promotion->rate_value = $request->type != 'free_delivery' ? $request->rate_value:null;
		$promotion->start_date = $request->start_date;
		$promotion->end_date = $request->end_date;
		$promotion->status = $request->status;
		
		if($promotion->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('promotions.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$data = Promotion::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('promotions.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
