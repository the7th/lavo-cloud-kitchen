<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PictureRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Picture;
use App\Image;
use Auth;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class PictureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/pictures/images';
    }

    public function index(Request $request)
    {
		$data = Picture::orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.pictures.index', compact('data','request'));
    }


	public function create()
    {
		return view('dashboard.pictures.create');
    }

	public function store(PictureRequest $request)
    {
		$picture = new Picture;
		$picture->start_date = $request->start_date;
		$picture->end_date = $request->end_date;
		$picture->url = $request->url;
		$picture->status = $request->status;

		if($picture->save())
		{



			$destinationPath = $this->destinationPath;
			$image = $request->file('image');
			$filename = $picture->id . time() . "." . $image->getClientOriginalExtension();
			$image->move($destinationPath, $filename);

			$picture->path = $this->destinationPath;
			$picture->filename = $filename;
			$picture->save();

			$image_library = new Image;
			$image_library->image_category_id = 4; //id for pictures category
			$image_library->url = env('APP_URL').'/'.$picture->path.'/'.$picture->filename;
			$image_library->save();

            $optimizer_chain = OptimizerChainFactory::create();
            $optimizer_chain->optimize($picture->path.'/'.$picture->filename);

			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('pictures.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function edit($id)
    {
		$data = Picture::find($id);
		return view('dashboard.pictures.edit', compact('data'));
    }

	public function update(PictureRequest $request, $id)
    {
		$picture = Picture::find($id);
		$picture->start_date = $request->start_date;
		$picture->end_date = $request->end_date;
		$picture->url = $request->url;
		$picture->status = $request->status;

		if($picture->save())
		{
			if ($image = $request->file('image'))
			{
				$img_filePath = $picture->path.'/'.$picture->filename;
				if (file_exists($img_filePath)) {
					unlink($img_filePath);
				}

				$destinationPath = $this->destinationPath;
				$filename = $picture->id . time() . "." . $image->getClientOriginalExtension();
				$image->move($destinationPath, $filename);

				$picture->path = $this->destinationPath;
				$picture->filename = $filename;
				$picture->save();

				$image_library = new Image;
				$image_library->image_category_id = 4; //id for banner image category
				$image_library->url = env('APP_URL').'/'.$picture->path.'/'.$picture->filename;
				$image_library->save();
			}
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('pictures.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function delete($id)
    {
		$data = Picture::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('pictures.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
