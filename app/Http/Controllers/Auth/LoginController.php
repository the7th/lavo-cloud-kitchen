<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserWallet;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToFacebook() {
        //auth_type: 'reauthenticate'
        return Socialite::driver('facebook')->redirect();
    }



    public function handleFacebookCallback(Request $request) {

        try {

            $user = Socialite::driver('facebook')->user();
            $findUser = User::where('facebook_id', $user->id)->first();

            if ($findUser) {

                Auth::login($findUser, false);

                if (Auth::check())
                {

                    $tokenResult = $findUser->createToken('Personal Access Token');
                    $token = $tokenResult->accessToken;

                    Session::put('token',$token);

                    $wallet = UserWallet::firstOrCreate(
                        ['user_id' => $findUser->id],
                        ['user_id' => $findUser->id, 'balance' => 0]
                    );

                    return redirect()->route('frontend.consumer');
                }

            } else {

                $rand = rand(1000,9999);

                $hash = Hash::make('PsD#v!@#'. (string) $rand);

                $new = [];
                $new['email'] = $user->email;
                $new['name'] = $user->name;

                $newUser = User::create(['type' => 'consumer', 'name' => $user->name, 'email' => $user->email, 'facebook_id' => $user->user['id'], 'password' => $hash]);

                Auth::login($newUser, false);

                return redirect()->route('frontend.consumer');

            }
        }
        catch(Exception $e) {
            return redirect('auth/facebook');
        }
    }


}
