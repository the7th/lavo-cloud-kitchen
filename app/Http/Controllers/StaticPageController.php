<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;
use DB;

class StaticPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show']);
    }

    public function index(Request $request)
    {
		$data = DB::table('static_pages')->get();
		return view('dashboard.static-pages.index', compact('data'));
    }
	
	public function create()
    {
		return view('dashboard.static-pages.create');
    }
	
	public function store(Request $request)
    {
		$detail=$request->input('html');
		$dom = new \DomDocument();
		$dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    

		$images = $dom->getElementsByTagName('img');
		
		foreach($images as $k => $img){
			$data = $img->getAttribute('src');
			if ( base64_encode(base64_decode($data, true)) === $data)
			{
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
				$image_name= "uploads/summernote/" . time().$k.'.png';
				$path = $image_name;
				file_put_contents($path, $data);
				$img->removeAttribute('src');
				$img->setAttribute('src', $image_name);
			}
		}

		$detail = $dom->saveHTML();
		$data_array = [
			'title' => $request->title,
			'html' => $detail
		];
		
		if(DB::table('static_pages')->insert($data_array))
		{
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('static-pages.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function edit($id)
    {
		$data = DB::table('static_pages')->where('id',$id)->first();
		return view('dashboard.static-pages.edit', compact('data'));
    }
	
	public function update(Request $request, $id)
    {
		$detail=$request->input('html');
		$dom = new \DomDocument();
		$dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    

		$images = $dom->getElementsByTagName('img');
		
		foreach($images as $k => $img){
			$data = $img->getAttribute('src');
			if ( base64_encode(base64_decode($data, true)) === $data)
			{
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
				$image_name= "uploads/summernote/" . time().$k.'.png';
				$path = $image_name;
				file_put_contents($path, $data);
				$img->removeAttribute('src');
				$img->setAttribute('src', $image_name);
			}
		}

		$detail = $dom->saveHTML();
		$data_array = [
			'title' => $request->title,
			'html' => $detail
		];
		
		if(DB::table('static_pages')->where('id',$id)->update($data_array))
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('static-pages.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function show($title)
    {
		$data = DB::table('static_pages')->where('title',$title)->first();
		return view('static-page', compact('data'));
    }
	
	public function delete($id)
    {
		if(DB::table('static_pages')->where('id',$id)->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('static-pages.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
