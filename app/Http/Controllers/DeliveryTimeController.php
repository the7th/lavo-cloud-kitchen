<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\DeliveryTime;
use Auth;

class DeliveryTimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$data = DeliveryTime::keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.delivery-times.index', compact('data','request'));
    }
	
	
	public function create()
    {
		return view('dashboard.delivery-times.create');
    }
	
	public function store(Request $request)
    {
		$delivery_time = new DeliveryTime;
		$delivery_time->title = $request->title;
		$delivery_time->monday_enabled = $request->monday_enabled ? 'yes':'no';
		$delivery_time->monday_times = $request->monday_times ? json_encode($request->monday_times,true) : '[]';
		$delivery_time->tuesday_enabled = $request->tuesday_enabled ? 'yes':'no';
		$delivery_time->tuesday_times =  $request->tuesday_times ? json_encode($request->tuesday_times,true) : '[]';
		$delivery_time->wednesday_enabled = $request->wednesday_enabled ? 'yes':'no';
		$delivery_time->wednesday_times =  $request->wednesday_times ? json_encode($request->wednesday_times,true) : '[]';
		$delivery_time->thursday_enabled = $request->thursday_enabled ? 'yes':'no';
		$delivery_time->thursday_times =  $request->thursday_times ? json_encode($request->thursday_times,true) : '[]';
		$delivery_time->friday_enabled = $request->friday_enabled ? 'yes':'no';
		$delivery_time->friday_times =  $request->friday_times ? json_encode($request->friday_times,true) : '[]';
		$delivery_time->saturday_enabled = $request->saturday_enabled ? 'yes':'no';
		$delivery_time->saturday_times =  $request->saturday_times ? json_encode($request->saturday_times,true) : '[]';
		$delivery_time->sunday_enabled = $request->sunday_enabled ? 'yes':'no';
		$delivery_time->sunday_times =  $request->sunday_times ? json_encode($request->sunday_times,true) : '[]';
		$delivery_time->status = $request->status;
		
		if($delivery_time->save())
		{
			$message = 'Record has been created successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('delivery-times.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	
	public function edit($id)
    {
		$data = DeliveryTime::find($id);
		return view('dashboard.delivery-times.edit', compact('data'));
    }
	
	public function update(Request $request, $id)
    {
		$delivery_time = DeliveryTime::find($id);
		$delivery_time->title = $request->title;
		$delivery_time->monday_enabled = $request->monday_enabled ? 'yes':'no';
		$delivery_time->monday_times = $request->monday_times ? json_encode($request->monday_times,true) : '[]';
		$delivery_time->tuesday_enabled = $request->tuesday_enabled ? 'yes':'no';
		$delivery_time->tuesday_times =  $request->tuesday_times ? json_encode($request->tuesday_times,true) : '[]';
		$delivery_time->wednesday_enabled = $request->wednesday_enabled ? 'yes':'no';
		$delivery_time->wednesday_times =  $request->wednesday_times ? json_encode($request->wednesday_times,true) : '[]';
		$delivery_time->thursday_enabled = $request->thursday_enabled ? 'yes':'no';
		$delivery_time->thursday_times =  $request->thursday_times ? json_encode($request->thursday_times,true) : '[]';
		$delivery_time->friday_enabled = $request->friday_enabled ? 'yes':'no';
		$delivery_time->friday_times =  $request->friday_times ? json_encode($request->friday_times,true) : '[]';
		$delivery_time->saturday_enabled = $request->saturday_enabled ? 'yes':'no';
		$delivery_time->saturday_times =  $request->saturday_times ? json_encode($request->saturday_times,true) : '[]';
		$delivery_time->sunday_enabled = $request->sunday_enabled ? 'yes':'no';
		$delivery_time->sunday_times =  $request->sunday_times ? json_encode($request->sunday_times,true) : '[]';
		$delivery_time->status = $request->status;
		
		if($delivery_time->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('delivery-times.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$data = DeliveryTime::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('delivery-times.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
