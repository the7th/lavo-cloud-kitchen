<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NotificationRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\NotificationData;
use Auth;
use DB;
use Notification;
use App\Notifications\SendNotification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$data = NotificationData::keyword($request->keyword)
					->status($request->status)
					// ->type($request->type)
					->createdDate($request->date)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.notifications.index', compact('data','request'));
    }
	
	public function create()
    {
		return view('dashboard.notifications.create');
    }
	
	public function store(NotificationRequest $request)
    {
		$data = new NotificationData;
		$data->title = $request->title;
		$data->message = $request->message;
		// $data->status = $request->status;
		
		if($data->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('notifications.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function edit($id)
    {
		$data = NotificationData::find($id);
		return view('dashboard.notifications.edit', compact('data'));
    }
	
	public function update(NotificationRequest $request, $id)
    {
		$data = NotificationData::find($id);
		$data->title = $request->title;
		$data->message = $request->message;
		// $data->status = $request->status;
		
		if($data->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('notifications.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function activation(Request $request,$id)
    {
		$status = $request->status;
		$data = NotificationData::find($id);
		$data->status = $status;
		if($data->save())
		{
			$message = 'Record status has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('notifications.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function publish(Request $request, $id)
    {
		$data = NotificationData::find($id);
		$data->status = 'published';
		
		if($data->save())
		{
			$this->sendMessage($data->title, $data->message);
			$message = 'Record has been published successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('notifications.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function delete($id)
    {
		$data = NotificationData::find($id);
		
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('notifications.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	// public function sendNotification()
    // {
        // $user = User::find(8);
        // $details = [
            // 'greeting' => 'Hi Artisan',
            // 'body' => 'This is my first notification from ItSolutionStuff.com',
            // 'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
            // 'actionText' => 'View My Site',
            // 'actionURL' => url('/'),
            // 'order_id' => 101
        // ];

        // $user->notify(new SendNotification($details));
        // dd('done');
    // }
	
	public function sendMessage($title, $message) 
	{
		// return 'test-onesignal';
		$content      = array(
			"en" => $message
		);
		$headings      = array(
			"en" => $title
		);
		// $hashes_array = array();
		// array_push($hashes_array, array(
			// "id" => "like-button",
			// "text" => "Like",
			// "icon" => "http://i.imgur.com/N8SN8ZS.png",
			// "url" => "https://yoursite.com"
		// ));
		// array_push($hashes_array, array(
			// "id" => "like-button-2",
			// "text" => "Like2",
			// "icon" => "http://i.imgur.com/N8SN8ZS.png",
			// "url" => "https://yoursite.com"
		// ));
		$fields = array(
			'app_id' => env('ONESIGNAL_APP_ID'),
			'included_segments' => array(
				'All'
			),
			'data' => array(
				"foo" => "bar"
			),
			'headings' => $headings,
			'contents' => $content,
			// 'web_buttons' => $hashes_array
		);
		
		$fields = json_encode($fields);
		// print("\nJSON sent:\n");
		// print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Basic '.env('ONESIGNAL_API_KEY')
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}

	// $response = sendMessage();
	// $return["allresponses"] = $response;
	// $return = json_encode($return);

	// $data = json_decode($response, true);
	// print_r($data);
	// $id = $data['id'];
	// print_r($id);

	// print("\n\nJSON received:\n");
	// print($return);
	// print("\n");
}
