<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Order;
use App\OrderDetail;
use Auth;

class OrderDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function update(Request $request)
    {
		if($order_detail = OrderDetail::where('chef_id', Auth::user()->id)->update(['order_status' => $request->order_status]))
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->back()->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
