<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Kitchen;
use App\KitchenImage;
use App\Booking;
use Auth;
use PDF;

class BookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/kitchens/images'; 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		$data = Booking::keyword($request->keyword)
					->type($request->type)
					->dateRange($request->from, $request->to)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.bookings.index', compact('data','request'));
    }
	
	public function calendar()
    {
		
		if(request()->ajax()) 
        {
 
         $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
         $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
 
         $data = Booking::whereDate('booking_start_date', '>=', $start)->whereDate('booking_end_date',   '<=', $end)->get(['id','user_id','booking_start_date', 'booking_end_date']);
         return Response::json($data);
        }
        return view('dashboard.bookings.calendar');
    }
	
	public function edit($id)
    {
		$data = Booking::find($id);
		return view('dashboard.bookings.edit', compact('data'));
    }
	
	public function update(Request $request, $id)
    {
		$booking = Booking::find($id);
		$booking->payment_status = $request->payment_status;
		
		if($booking->save())
		{
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('bookings.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
	
	public function createPDF($id) {
      $data = Booking::find($id);

      view()->share('booking',$data);
      $pdf = PDF::loadView('receipts.pdf_view', $data);

      return $pdf->download('receipt-'.$data->id.'.pdf');
    }
}
