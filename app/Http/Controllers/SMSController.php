<?php

namespace App\Http\Controllers;

use App\OneTimePassword;
use App\SmsTxn;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SMSController extends Controller
{

    public function sendSMS($data)
    {

        // $data is array. array is populated with the following info:
        // $data['phone_num'], $data['aMsg']

        $url = 'https://m3tech.my/imp/impv2demo/webservice/submitsm.asmx?wsdl';

        $user_id = 'impv2demo';
        $password = '1022ba94a768962820e19c16638adc52';
        $service_id = 'impv2demo';
//        $phone_num = '60129506315';

        $date = Carbon::now()->format('Ymdhis');

        $msg_id = rand(10000,99999);


        // message msg_id
        $sms_txn = new SmsTxn;
        $sms_txn->message = $data['aMsg'];
        $sms_txn->msg_id = $msg_id;
        $sms_txn->phone_number = $data['phone_num'];


        $array = array(
            'Userkey' => $user_id,
            'Password' => $password,
            'MsgID' => $msg_id,
            'TimeStamp' => $date,
            'ServiceID' => $service_id,
            'aMsg' => $data['aMsg'],
            'Mobile' => $data['phone_num'],
            'MCN' => '66600',
            'ChargeCode' => '000',
            'MsgType' => '01',
        );

        $client = new \SoapClient($url);

        $client->deliverMessage($array);
    }

    public function sendOTP(Request $request)
    {

        $random = rand(1000,9999);


        // need to change this to getter url
        $phone_num = $request->phone_number;


        $data['aMsg'] = 'Hi from Lavo Cloud Kitchen. This is the OTP for your login ' . $random;
        $data['phone_num'] = $phone_num;

        SMSController::instance()->sendSMS($data);

        $time = Carbon::now()->addMinutes(10);

        $new = new OneTimePassword;
        $new->phone_number = $phone_num;
        $new->temp_numbers = $random;
        $new->time_expiry = $time;
        $new->save();

        // + 10 minutes for time expiry of current datetime

    }

    public function checkOTP(Request $request)
    {


        // check phone number and temp numbers are
        // true or false (within time expiry date)

        $otp =
            [
                'phone_number' => $request->phone_number,
                'temp_numbers' => $request->temp_numbers
            ];

        $getPhoneNumber = OneTimePassword::where('phone_number', '=', $otp['phone_number'])->get();

        $oneTimePasswordTrueOrNot = "";

        $user_id = 0;

        if ($getPhoneNumber)
        {

            $date = Carbon::now();

            $oneTimePasswordTrueOrNot = OneTimePassword::where($otp)
                ->expiry($date)
                ->first();

            if ($oneTimePasswordTrueOrNot)
            {
                $searchUser = User::contactNumber($otp['phone_number'])
                    ->first();

                $user_id = $searchUser->id;

                Auth::loginUsingId($user_id, true);
            }
        }

        $new = [];

        if ($oneTimePasswordTrueOrNot)
        {

            $new['status'] = 'success';
            $new['data'] = [
                'user_id' => $user_id,
            ];

        } else {

            $new['status'] = 'fail';
            $new['data'] = [
                'user_id' => $user_id
            ];

        }

        return $new;

    }


    public static function instance()
    {
        return new self;
    }




}
