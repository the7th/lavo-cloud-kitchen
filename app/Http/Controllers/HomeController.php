<?php

namespace App\Http\Controllers;

use App\CommissionCategory;
use Illuminate\Http\Request;
use Auth;
use App\Order;
use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod as CarbonPeriod;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		if(Auth::user()->isAdmin() || Auth::user()->type == 'chef')
		{
			$today_date = Carbon::today()->format('Y-m-d');
			$past_7_days_date = Carbon::today()->subDays(6)->format('Y-m-d');
			$period = CarbonPeriod::create($past_7_days_date,$today_date);

			$dates = [];
			$data = [];
			$total_sales = 0;

			foreach ($period as $date) {
				$sum_price = Order::where('payment_status','paid')->whereDate('created_at',$date)->sum('price');
				$total_sales += $sum_price;
				array_push($data, $sum_price);
				array_push($dates, '"'.$date->format('d M').'"');
			}
			$dates = implode(',',$dates);
			$max_data = ceil(max($data));
			$data = implode(',',$data);
			$paid_orders = Order::where('payment_status','paid')->whereDate('created_at', '>=', $past_7_days_date)->get();

			$comm = CommissionCategory::find(1)->value;

//			dd($comm);



			return view('dashboard.index', compact('dates', 'data', 'total_sales', 'past_7_days_date', 'today_date', 'paid_orders','max_data', 'comm'));
		}
        return view('welcome');
    }
}
