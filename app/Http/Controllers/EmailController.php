<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;
use App\Mail\UserRegisteredEmail;

class EmailController extends Controller
{
    public function sendmail()
    {
        //Fetch Sample User By ID
        $user = User::find(6);

        Mail::to($user)->send(new UserRegisteredEmail($user));

        if (Mail::failures()) {
            return 'Sorry! Please try again latter';
        } else {
            return 'Great! Successfully send in your mail';
        }
    }
}
