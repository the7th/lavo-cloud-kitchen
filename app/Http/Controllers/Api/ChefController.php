<?php

namespace App\Http\Controllers\Api;

use App\Card;
use App\FinancialTxn;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\SiteController;
use App\LamboTrail;
use App\Mail\NewOrder;
use App\MenuCategory;
use App\UserWallet;
use Faker\Factory;

use Illuminate\Http\Request;
use App\Menu;
use App\User;
use App\UserAddress;
use App\Order;
use App\OrderDetail;
use App\BannerImage;
use App\Picture;
use App\DeliveryTime;
use App\Review;
use App\PromoCode;
use App\Promotion;
use App\Kitchen;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\Mail;
use PHPHtmlParser\Dom;
use Validator;
use DB;
use Session;




class ChefController extends Controller
{
	public function __construct()
    {
        // $this->hours = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00"];
    }

    public function kitchenList(Request $request)
    {
        $page = $request->page;
		$per_page = $request->per_page;
		$date = Carbon::now()->format('Y-m-d');

        $status = 'success';

        $list = [];
		$counter = 0;

		$kitchens = Kitchen::where('status', 'active')
                ->orderBy('created_at', 'desc')
                ->paginate($per_page);


		foreach($kitchens as $kitchen)
		{
			$first_image = $kitchen->images->first();
			$list[$counter]['id'] = $kitchen->id;
			$list[$counter]['name'] = $kitchen->name;
			$list[$counter]['img_url'] = env('APP_URL').'/'.$first_image->path.'/'.$first_image->filename;
			$list[$counter]['session_price'] = $kitchen->session_price;
			$list[$counter]['weekly_price'] = $kitchen->weekly_price;
			$list[$counter]['monthly_price'] = $kitchen->monthly_price;
			$counter++;
		}

		return response()->json([
			'message' => 'Kitchen list.',
			'data' => $list,
			'status' => $status
		], 200);

    }

    public function menuList(Request $request)
    {
		$user = Auth::user();
		$page = $request->page;
		$per_page = $request->per_page;

        $status = 'success';

        $list = [];
		$counter = 0;
		$menus = Menu::orderBy('created_at', 'desc')
						->where('user_id',$user->id)
						->paginate($per_page);

		foreach($menus as $menu)
		{
			$list[$counter]['id'] = $menu->id;
			$list[$counter]['name'] = $menu->name;
			$list[$counter]['category'] = $menu->menuCategory->name;
			$list[$counter]['price'] = $menu->price;
			$image = $menu->images->first();
			$list[$counter]['image'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;
			$counter++;
		}

		return response()->json([
			'message' => 'Menu list.',
			'data' => $list,
			'status' => $status
		], 200);
    }

    public function menuInfo(Request $request)
    {
		$user = Auth::user();
		$id = $request->id;

        $status = 'success';

        $data = [];
		$menu = Menu::where('id',$id)
					->where('user_id',$user->id)
					->where('status','active')
					->where('approval_status','approved')
					->first();

		if($menu)
		{
			$data['id'] = $menu->id;
			$data['cover_img_url'] =  $menu->cover_image_path ? env('APP_URL').'/'.$menu->cover_image_path.'/'.$menu->cover_image_filename : null;
			$image = $menu->images->first();
			$data['img_url'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;


			$data['title'] = $menu->name;
			// $data['chef_name'] = $menu->user->name;
			// $data['chef_img_url'] = $menu->user->image_path ? env('APP_URL').'/'.$menu->user->image_path.'/'.$menu->user->image_filename : null;
			$data['rating'] = $menu->approvedReviews->count() > 0 ? ($menu->approvedReviews->sum('rate') / $menu->approvedReviews->count()) : 0;

			$cuisine_promotion = $menu->menuCategory->activePromotions;
			$data['promotion_info']['cuisine'] = $cuisine_promotion;
			$brand_promotion = $menu->user->chefCompany ? $menu->user->chefCompany->activePromotions : [];
			$data['promotion_info']['brand'] = $brand_promotion;
			$data['original_price'] = $menu->price;



			$total_discount = 0;
			foreach($cuisine_promotion as $cp)
			{
				if($cp->rate_type == 'fixed_rate')
				{
					$total_discount += $cp->rate_value;
				}
				if($cp->rate_type == 'percentage')
				{
					$total_discount += ($menu->price * $cp->rate_value)/100;
				}
			}
			foreach($brand_promotion as $bp)
			{
				if($bp->rate_type == 'fixed_rate')
				{
					$total_discount += $bp->rate_value;
				}
				if($bp->rate_type == 'percentage')
				{
					$total_discount += ($menu->price * $bp->rate_value)/100;
				}
			}

			$data['total_discount'] = number_format((float)$total_discount, 2, '.', '');
			$data['price'] = number_format((float)$menu->price - $total_discount, 2, '.', '');

			$data['settings'] = null;

			$setting_counter = 0;
			foreach($menu->menuSettings as $setting)
			{
				$data['settings'][$setting_counter]['id'] = $setting->id;
				$data['settings'][$setting_counter]['name'] = $setting->name;
				$data['settings'][$setting_counter]['description'] = $setting->description;
				$data['settings'][$setting_counter]['image'] = $setting->image_path ? env('APP_URL').'/'.$setting->image_path.'/'.$setting->image_filename : null;

				$setting_item_counter = 0;
				$data['settings'][$setting_counter]['items'] = null;
				foreach($setting->menuSettingItems as $setting_item)
				{
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['id'] = $setting_item->id;

					$total_discount = 0;
					foreach($cuisine_promotion as $cp)
					{
						if($cp->rate_type == 'fixed_rate')
						{
							$total_discount += $cp->rate_value;
						}
						if($cp->rate_type == 'percentage')
						{
							$total_discount += ($setting_item->price * $cp->rate_value)/100;
						}
					}
					foreach($brand_promotion as $bp)
					{
						if($bp->rate_type == 'fixed_rate')
						{
							$total_discount += $bp->rate_value;
						}
						if($bp->rate_type == 'percentage')
						{
							$total_discount += ($setting_item->price * $bp->rate_value)/100;
						}
					}

					$data['settings'][$setting_counter]['items'][$setting_item_counter]['name'] = $setting_item->name;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['price'] = number_format((float)$setting_item->price - $total_discount, 2, '.', '');
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['description'] = $setting_item->description;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['image'] = $setting_item->image_path ? env('APP_URL').'/'.$setting_item->image_path.'/'.$setting_item->image_filename : null;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'] = $setting_item->menuSettingSubItems;

					$setting_sub_item_counter = 0;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'] = null;
					foreach($setting_item->menuSettingSubItems as $setting_sub_item)
					{
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['id'] = $setting_sub_item->id;
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['name'] = $setting_sub_item->name;
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['price'] = $setting_sub_item->price;

						$setting_secondary_sub_item_counter = 0;
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'] = null;
						foreach($setting_sub_item->menuSettingSecondarySubItems as $setting_secondary_sub_item)
						{
							$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'][$setting_secondary_sub_item_counter]['id'] = $setting_secondary_sub_item->id;
							$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'][$setting_secondary_sub_item_counter]['name'] = $setting_secondary_sub_item->name;
							$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'][$setting_secondary_sub_item_counter]['price'] = $setting_secondary_sub_item->price;
							$setting_secondary_sub_item_counter++;
						}
						$setting_sub_item_counter++;
					}

					$setting_item_counter++;
				}
				$setting_counter++;
			}

			return response()->json([
				'message' => 'Food info.',
				'data' => $data,
				'status' => $status
			], 200);
		}
		return response()->json([
			'message' => 'Record not found.',
			'status' => 'failed'
		], 400);
    }

	public function checkBookingTime($request)
    {
        $kitchen = Kitchen::find($request['kitchen_id']);
        $booking = Booking::where('ref_type','kitchen')->where('ref_id',$request['kitchen_id'])->where('payment_status','paid')->first();
		
		if($booking)
		{
			if($request['booking_type'] == 'half_day_am')
			{
				if($booking['booking_type'] == 'half_day_am')
				{
					return false;
				}
				return true;
			}
			elseif($request['booking_type'] == 'half_day_pm')
			{
				if($booking['booking_type'] == 'half_day_pm')
				{
					return false;
				}
				return true;
			}
			elseif($request['booking_type'] == 'weekly')
			{
				if($booking['booking_type'] == 'weekly')
				{
					$finishing_date = Carbon::parse($request['booking_date'])->addWeeks(1)->format('Y-m-d');
					$check_starting_date = Carbon::parse($request['booking_date'])->between($booking['booking_start_date'],$booking['booking_end_date']);
					$check_finishing_date = Carbon::parse($finishing_date)->between($booking['booking_start_date'],$booking['booking_end_date']);
					
					if($check_starting_date || $check_finishing_date || $request['booking_date'] == $booking['booking_start_date'] || $request['booking_date'] == $booking['booking_end_date'])
					{
						return false;
					}
					else
					{
						return true;
					}
				}
				return true;
			}
			elseif($request['booking_type'] == 'monthly')
			{
				if($booking['booking_type'] == 'monthly')
				{
					$finishing_date = Carbon::parse($request['booking_date'])->addMonths(1)->format('Y-m-d');
					$check_starting_date = Carbon::parse($request['booking_date'])->between($booking['booking_start_date'],$booking['booking_end_date']);
					$check_finishing_date = Carbon::parse($finishing_date)->between($booking['booking_start_date'],$booking['booking_end_date']);
					
					if($check_starting_date || $check_finishing_date || $request['booking_date'] == $booking['booking_start_date'] || $request['booking_date'] == $booking['booking_end_date'])
					{
						return false;
					}
					else
					{
						return true;
					}
				}
				return true;
			}
		}
		return true;
    }

	public function createBooking(Request $request)
    {
		$user = Auth::user();
		if($user->type != 'chef')
		{
			return response()->json([
				'message' => 'Booking is only for chef.',
				'status' => 'failed'
			], 400);
		}
		$kitchen_id = $request->kitchen_id;
		$booking_date = $request->booking_date;
		$booking_type = $request->booking_type; //half_day_am, half_day_pm, full_day, weekly, monthly

		$kitchen = Kitchen::find($request['kitchen_id']);
		if(!$this->checkBookingTime($request))
		{
			return response()->json([
				'message' => 'Booking is not available.',
				'status' => 'failed'
			], 400);
		}
		
		if($request['booking_type'] == 'half_day_am')
		{
			$finishing_date = $request['booking_date'];
			$price = $kitchen->session_price;
		}
		elseif($request['booking_type'] == 'half_day_pm')
		{
			$finishing_date = $request['booking_date'];
			$price = $kitchen->session_price;
		}
		elseif($request['booking_type'] == 'weekly')
		{
			$finishing_date = Carbon::parse($request['booking_date'])->addWeeks(1)->format('Y-m-d');
			$price = $kitchen->weekly_price;
		}
		elseif($request['booking_type'] == 'monthly')
		{
			$finishing_date = Carbon::parse($request['booking_date'])->addMonths(1)->format('Y-m-d');
			$price = $kitchen->monthly_price;
		}
		
		$booking = new Booking;
		$booking->user_id = $user->id;
		$booking->ref_type = 'kitchen';
		$booking->ref_id = $kitchen_id;
		$booking->booking_type = $booking_type;
		$booking->booking_start_date = $request['booking_date'];
		$booking->booking_end_date = $finishing_date;
		$booking->price = $price;


		if($booking->save())
		{
			return response()->json([
				'id' => $booking->id,
				'message' => 'Booking has been created.',
				'data' => $booking,
				'status' => 'success'
			], 200);
		}
    }
}
