<?php

namespace App\Http\Controllers\Api;

use App\Card;
use App\FinancialTxn;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\SiteController;
use App\LamboTrail;
use App\Mail\NewOrder;
use App\MenuCategory;
use App\UserWallet;
use Faker\Factory;

use Illuminate\Http\Request;
use App\Menu;
use App\User;
use App\UserAddress;
use App\Order;
use App\OrderDetail;
use App\BannerImage;
use App\Picture;
use App\DeliveryTime;
use App\Review;
use App\PromoCode;
use App\Promotion;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\Mail;
use PHPHtmlParser\Dom;
use Validator;
use DB;
use Session;




class ConsumerController extends Controller
{

//    this one will output API for the site user to consume

	public function __construct()
    {
        $this->hours = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00"];
    }

    public function siteNews()
    {
        // where to save the site news?
        // title // desc // url to the story

        $status = 'success';
        $site_news = [];
        $num = 1;

        $faker = Factory::create();

        for ($x = 0; $x < $num; $x++) {
            array_push($site_news, [
                'title' => 'Our Story',
                'desc' => $faker->text(200),
                'url' => 'https://google.com'
            ]);
        }

        $new = [];
        $new['status'] = $status;
        $new['data'] = $site_news;

        return $new;

    }

    public function siteHeroImage(Request $request)
    {

        $faker = Factory::create();

        $status = 'success';

        $hero = [
            'id' => 1,
            'name' => 'Banner ' . $faker->name,
            'img_url' => 'https://picsum.photos/300/200',
            'url' => 'https://google.com'
        ];

        $new = [];

        $new['status'] = $status;
        $new['data'] = $hero;

        return $new;

    }


    public function bannerDeliveryLandingPage(Request $request)
    {
        $page = $request->page;
		$per_page = $request->per_page;
		$date = Carbon::now()->format('Y-m-d');

        $status = 'success';

        $list = [];
		$counter = 0;

		$banners = BannerImage::dateRange($date)
                ->where('status', 'active')
                ->orderBy('created_at', 'desc')
                ->paginate($per_page);


		foreach($banners as $banner)
		{
			$list[$counter]['id'] = $banner->id;
			$list[$counter]['name'] = 'Banner ' . $banner->id;
			$list[$counter]['img_url'] = env('APP_URL').'/'.$banner->path.'/'.$banner->filename;
			$list[$counter]['url'] = $banner->url;
			$counter++;
		}

		return response()->json([
			'message' => 'Banner list.',
			'data' => $list,
			'status' => $status
		], 200);

    }

    public function heroDeliveryLandingPage(Request $request)
    {
		$page = $request->page;
		$per_page = $request->per_page;
		$date = Carbon::now()->format('Y-m-d');

        $status = 'success';

        $list = [];
		$counter = 0;

		$pictures = Picture::dateRange($date)
                ->where('status', 'active')
                ->orderBy('created_at', 'desc')
                ->paginate($per_page);


		foreach($pictures as $picture)
		{
			$list[$counter]['id'] = $picture->id;
			$list[$counter]['name'] = 'Hero ' . $picture->id;
			$list[$counter]['img_url'] = env('APP_URL').'/'.$picture->path.'/'.$picture->filename;
			$list[$counter]['url'] = $picture->url;
			$counter++;
		}

		return response()->json([
			'message' => 'Picture list.',
			'data' => $list,
			'status' => $status
		], 200);

    }

    public function categoryList()
    {
        $menus = MenuCategory::active()
        ->get();

        return $menus;
    }

	public function distance($lat2, $lon2)
	{
		//set to Menara Lien Hoe as starting point
		$lon1 = 101.594982;
		$lat1 = 3.130820;

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;

		return ($miles * 1.609344);
	}

    public function listOfFoodsWSummaryWRatingWPrice(Request $request)
    {
        $search_query = $request->query('search_query');

        $page = $request->page;
        $per_page = $request->per_page;
        $menu_category_id = $request->menu_category_id;

		$long = $request->long;
		$lat = $request->lat;

        $status = 'success';

        $list = [];
        $counter = 0;

        $menus = "";

        if ($search_query) {
            $menus = Menu::cuisine($menu_category_id)
                ->keyword($search_query)
				->promotion($request->promotion_id)
				->where('status','active')
				->where('approval_status','approved')
                ->orderBy('created_at', 'desc')
                ->paginate($per_page);
        } else {
            $menus = Menu::cuisine($menu_category_id)
                ->promotion($request->promotion_id)
				->where('status','active')
				->where('approval_status','approved')
				->orderBy('created_at', 'desc')
                ->paginate($per_page);
        }

		$distance = $this->distance($lat, $long);

		foreach($menus as $menu)
		{
			if(($long && $lat) && ($distance > 6))
			{
				continue;
			}
			$list[$counter]['id'] = $menu->id;
			$list[$counter]['menu_category_id'] = $menu->menu_category_id;
			$list[$counter]['name'] = $menu->name;
			$list[$counter]['food_desc'] = $menu->description;
			$list[$counter]['original_price'] = $menu->price;
			$list[$counter]['cover_img_url'] =  $menu->cover_image_path ? env('APP_URL').'/'.$menu->cover_image_path.'/'.$menu->cover_image_filename : null;
			$image = $menu->images->first();
			$list[$counter]['img_url'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;
			$list[$counter]['rating'] = $menu->approvedReviews->count() > 0 ? ($menu->approvedReviews->sum('rate') / $menu->approvedReviews->count()) : 0;
			$list[$counter]['distance'] = $distance;
			$cuisine_promotion = $menu->menuCategory->activePromotions;
			$list[$counter]['promotion_info']['cuisine'] = $cuisine_promotion;
			$brand_promotion = $menu->user->chefCompany ? $menu->user->chefCompany->activePromotions : [];
			$list[$counter]['promotion_info']['brand'] = $brand_promotion;

			$total_discount = 0;
			foreach($cuisine_promotion as $cp)
			{
				if($cp->rate_type == 'fixed_rate')
				{
					$total_discount += $cp->rate_value;
				}
				if($cp->rate_type == 'percentage')
				{
					$total_discount += ($menu->price * $cp->rate_value)/100;
				}
			}
			foreach($brand_promotion as $bp)
			{
				if($bp->rate_type == 'fixed_rate')
				{
					$total_discount += $bp->rate_value;
				}
				if($bp->rate_type == 'percentage')
				{
					$total_discount += ($menu->price * $bp->rate_value)/100;
				}
			}

			$list[$counter]['total_discount'] = number_format((float)$total_discount, 2, '.', '');
			$list[$counter]['price'] = number_format((float)$menu->price - $total_discount, 2, '.', '');

			$counter++;
		}

		return response()->json([
			'message' => 'Food list.',
			'data' => $list,
			'distance' => number_format($distance, 2, '.', '').' KM',
			'status' => $status
		], 200);
    }

    public function mostPopularFoods(Request $request)
    {
		$page = $request->page;
		$per_page = $request->per_page;

        $status = 'success';

        $list = [];
		$counter = 0;
		$menus = Menu::withCount('orders')
						->orderBy('orders_count', 'desc')
						->paginate($per_page);

		foreach($menus as $menu)
		{
			$list[$counter]['id'] = $menu->id;
			$list[$counter]['name'] = $menu->name;
			$list[$counter]['food_desc'] = $menu->description;
			$list[$counter]['price'] = $menu->price;
			$list[$counter]['cover_img_url'] =  $menu->cover_image_path ? env('APP_URL').'/'.$menu->cover_image_path.'/'.$menu->cover_image_filename : null;
			$image = $menu->images->first();
			$list[$counter]['image'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;
			$list[$counter]['rating'] = $menu->approvedReviews->count() > 0 ? ($menu->approvedReviews->sum('rate') / $menu->approvedReviews->count()) : 0;
			$counter++;
		}

		return response()->json([
			'message' => 'Most popular food list.',
			'data' => $list,
			'status' => $status
		], 200);
    }


    public function listOfOrdersNotCheckedOutYet()
    {

        $status = 'success';

        $list_of_foods = [];

        array_push($list_of_foods, [
            'id' => '1',
            'name' => 'Nasi Lemak',
            'qty' => 3,
            'img_url' => 'https://picsum.photos/400/400'
        ]);

        array_push($list_of_foods, [
            'id' => '7',
            'name' => 'Ayam Goreng',
            'qty' => 1,
            'img_url' => 'https://picsum.photos/400/400'
        ]);

        $new = [];

        $new['status'] = $status;
        $new['data'] = $list_of_foods;

        return $new;

    }

    public function foodInfo(Request $request)
    {
		$id = $request->id;

        $status = 'success';

        $data = [];
		$menu = Menu::where('id',$id)
					->where('status','active')
					->where('approval_status','approved')
					->first();

		if($menu)
		{
			$data['id'] = $menu->id;
			$data['cover_img_url'] =  $menu->cover_image_path ? env('APP_URL').'/'.$menu->cover_image_path.'/'.$menu->cover_image_filename : null;
			$image = $menu->images->first();
			$data['img_url'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;


			$data['title'] = $menu->name;
			$data['chef_name'] = $menu->user->name;
			$data['chef_img_url'] = $menu->user->image_path ? env('APP_URL').'/'.$menu->user->image_path.'/'.$menu->user->image_filename : null;
			$data['rating'] = $menu->approvedReviews->count() > 0 ? ($menu->approvedReviews->sum('rate') / $menu->approvedReviews->count()) : 0;

			$cuisine_promotion = $menu->menuCategory->activePromotions;
			$data['promotion_info']['cuisine'] = $cuisine_promotion;
			$brand_promotion = $menu->user->chefCompany ? $menu->user->chefCompany->activePromotions : [];
			$data['promotion_info']['brand'] = $brand_promotion;
			$data['original_price'] = $menu->price;



			$total_discount = 0;
			foreach($cuisine_promotion as $cp)
			{
				if($cp->rate_type == 'fixed_rate')
				{
					$total_discount += $cp->rate_value;
				}
				if($cp->rate_type == 'percentage')
				{
					$total_discount += ($menu->price * $cp->rate_value)/100;
				}
			}
			foreach($brand_promotion as $bp)
			{
				if($bp->rate_type == 'fixed_rate')
				{
					$total_discount += $bp->rate_value;
				}
				if($bp->rate_type == 'percentage')
				{
					$total_discount += ($menu->price * $bp->rate_value)/100;
				}
			}

			$data['total_discount'] = number_format((float)$total_discount, 2, '.', '');
			$data['price'] = number_format((float)$menu->price - $total_discount, 2, '.', '');

			$data['settings'] = null;

			$setting_counter = 0;
			foreach($menu->menuSettings as $setting)
			{
				$data['settings'][$setting_counter]['id'] = $setting->id;
				$data['settings'][$setting_counter]['name'] = $setting->name;
				$data['settings'][$setting_counter]['description'] = $setting->description;
				$data['settings'][$setting_counter]['image'] = $setting->image_path ? env('APP_URL').'/'.$setting->image_path.'/'.$setting->image_filename : null;

				$setting_item_counter = 0;
				$data['settings'][$setting_counter]['items'] = null;
				foreach($setting->menuSettingItems as $setting_item)
				{
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['id'] = $setting_item->id;

					$total_discount = 0;
					foreach($cuisine_promotion as $cp)
					{
						if($cp->rate_type == 'fixed_rate')
						{
							$total_discount += $cp->rate_value;
						}
						if($cp->rate_type == 'percentage')
						{
							$total_discount += ($setting_item->price * $cp->rate_value)/100;
						}
					}
					foreach($brand_promotion as $bp)
					{
						if($bp->rate_type == 'fixed_rate')
						{
							$total_discount += $bp->rate_value;
						}
						if($bp->rate_type == 'percentage')
						{
							$total_discount += ($setting_item->price * $bp->rate_value)/100;
						}
					}

					$data['settings'][$setting_counter]['items'][$setting_item_counter]['name'] = $setting_item->name;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['price'] = number_format((float)$setting_item->price - $total_discount, 2, '.', '');
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['description'] = $setting_item->description;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['image'] = $setting_item->image_path ? env('APP_URL').'/'.$setting_item->image_path.'/'.$setting_item->image_filename : null;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'] = $setting_item->menuSettingSubItems;

					$setting_sub_item_counter = 0;
					$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'] = null;
					foreach($setting_item->menuSettingSubItems as $setting_sub_item)
					{
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['id'] = $setting_sub_item->id;
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['name'] = $setting_sub_item->name;
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['price'] = $setting_sub_item->price;

						$setting_secondary_sub_item_counter = 0;
						$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'] = null;
						foreach($setting_sub_item->menuSettingSecondarySubItems as $setting_secondary_sub_item)
						{
							$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'][$setting_secondary_sub_item_counter]['id'] = $setting_secondary_sub_item->id;
							$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'][$setting_secondary_sub_item_counter]['name'] = $setting_secondary_sub_item->name;
							$data['settings'][$setting_counter]['items'][$setting_item_counter]['sub_items'][$setting_sub_item_counter]['secondary_sub_items'][$setting_secondary_sub_item_counter]['price'] = $setting_secondary_sub_item->price;
							$setting_secondary_sub_item_counter++;
						}
						$setting_sub_item_counter++;
					}

					$setting_item_counter++;
				}
				$setting_counter++;
			}

			return response()->json([
				'message' => 'Food info.',
				'data' => $data,
				'status' => $status
			], 200);
		}
		return response()->json([
			'message' => 'Record not found.',
			'status' => 'failed'
		], 400);
    }

    public function foodInfoOptions(Request $request)
    {
        $id = $request->id;

        $status = 'success';

        $menu = Menu::find($id);

		if($menu)
		{
			$options = json_decode($menu->settings, true);

			return response()->json([
				'message' => 'Option for '.$menu->name.'.',
				'data' => $options,
				'status' => $status
			], 200);
		}


    }

    public function generateFoods($num)
    {

        $faker = Factory::create();

        $status = 'success';

        $foods = [];

        for ($x = 1; $x <= $num; $x++) {

            array_push($foods, [
                'id' => $x,
                'name' => 'Nasi Lomak ' .  $faker->word,
                'food_desc' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.',
                'price' => 'RM ' . 10.90,
                'rating' => Factory::create()->randomFloat(2,0,5),
                'img_url' => 'https://picsum.photos/320/220',
                'url' => 'https://google.com'
            ]);
        }

        $new = [];

        $new['status'] = $status;
        $new['data'] = $foods;

        return $new;
    }

    public function listOfChefs(Request $request)
    {
		$page = $request->page;
		$per_page = $request->per_page;

        $status = 'success';

        $list = [];
		$counter = 0;
		$chefs = User::chef()
						->withCount('orders')
						->where('status','active')
						->orderBy('orders_count', 'desc')
						->paginate($per_page);

		foreach($chefs as $chef)
		{
			$list[$counter]['id'] = $chef->id;
			$list[$counter]['name'] = $chef->name;
			$list[$counter]['chef_desc'] = $chef->description;
			$list[$counter]['img_url'] = $chef->image_path ? env('APP_URL').'/'.$chef->image_path.'/'.$chef->image_filename : null;
			$list[$counter]['url'] = $chef->url;
			$counter++;
		}

		return response()->json([
			'message' => 'Chef list.',
			'data' => $list,
			'status' => $status
		], 200);

    }

    public function setOrder(Request $request)
    {
        Session::put('orders', $request->orders);
        $orders = Session::get('orders');
        return $orders;
    }

    public function clearOrder(Request $request)
    {
        Session::forget('orders');
        return 'success';
    }

    public function viewSetOrder(Request $request)
    {

        $orders = Session::get('orders');
        return $orders;
    }

	public function checkDeliveryTime($received_orders, $requested_delivery_date, $requested_delivery_timing)
    {
        foreach($received_orders['data'] as $item)
		{
			$menu = Menu::find($item['menu_id']);
			$delivery_time_id = $menu->user->chefCompany->delivery_time_id;

			if($delivery_time_id != 0)
			{
				$delivery_time = DeliveryTime::find($delivery_time_id);
				$day_name = Carbon::parse($requested_delivery_date)->format('l');
				$day_enabled = $delivery_time[strtolower($day_name).'_enabled'];
				if($day_enabled == 'no')
				{
					return false;
				}
				else
				{
					$day_times = json_decode($delivery_time[strtolower($day_name).'_times'],true);
					if($requested_delivery_timing == 'now')
					{
						$time_now = Carbon::now()->format('H:00');
						$time_now_add_hour = Carbon::now()->addHours(1)->format('H:00');
						$check_timing = [$time_now,$time_now_add_hour];

						foreach ($check_timing as $check_timing_value)
						{
							if (in_array($check_timing_value, $day_times)) {
								 return true;
							}
							else{
								 return false;
							}
						}
					}
					elseif($requested_delivery_timing == 'morning')
					{
						$check_timing = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00"];

						foreach ($check_timing as $check_timing_value)
						{
							if (in_array($check_timing_value, $day_times)) {
								 return true;
							}
							else{
								 return false;
							}
						}
					}
					elseif($requested_delivery_timing == 'afternoon')
					{
						$check_timing = ["12:00","13:00","14:00","15:00","16:00"];

						foreach ($check_timing as $check_timing_value)
						{
							if (in_array($check_timing_value, $day_times)) {
								 return true;
							}
							else{
								 return false;
							}
						}
					}
					elseif($requested_delivery_timing == 'evening')
					{
						$check_timing = ["17:00","18:00","19:00","20:00"];

						foreach ($check_timing as $check_timing_value)
						{
							if (in_array($check_timing_value, $day_times)) {
								 return true;
							}
							else{
								 return false;
							}
						}
					}
					elseif($requested_delivery_timing == 'night')
					{
						$check_timing = ["21:00","22:00","23:00"];

						foreach ($check_timing as $check_timing_value)
						{
							if (in_array($check_timing_value, $day_times)) {
								 return true;
							}
							else{
								 return false;
							}
						}
					}
				}
			}
		}
		return true;
    }

	public function createOrder(Request $request)
    {
		$user = Auth::user();
		$received_orders = $request->order;

		if(!$this->checkDeliveryTime($received_orders, $request->delivery_date, $request->delivery_timing))
		{
			return response()->json([
				'message' => 'Delivery time is not available.',
				'status' => 'failed'
			], 400);
		}

		$order = new Order;
		$order->user_id = $user->id;
		$order->address = $request->address;
		$order->state = $request->state;
		$order->city = $request->city;
		$order->postal_code = $request->postal_code;
		$order->delivery_date = $request->delivery_date;
		$order->delivery_timing = $request->delivery_timing;


		if($order->save())
		{
			$total_price = 0.00;
			foreach($received_orders['data'] as $item)
			{
				$menu = Menu::find($item['menu_id']);
				$total_price += $item['total_price_exc_sst'];

				foreach($item['settings'] as $setting)
				{
					$total_order_detail_price = $menu->price;

					$order_detail = new OrderDetail;
					$order_detail->order_id = $order->id;
					$order_detail->chef_id = $menu->user->id;
					$order_detail->ref_type = 'menu';
					$order_detail->ref_id = $menu->id;
					$order_detail->quantity = $setting['quantity'];
					$order_detail->price = $setting['total_price'];
					$order_detail->data = $setting ? json_encode($setting,true) : null;
					$order_detail->remarks = isset($setting['remarks']) ? $setting['remarks'] : null;
					$order_detail->save();
				}


			}
		}

		$order->price = $total_price;
		if($order->save())
		{



		    // to send data to Lambo

            $padded_order = str_pad($order->id, 10, "0", STR_PAD_LEFT);
            $merchantTxnId = "LCK" . $padded_order;

            $set_merchant_txn_order_id = Order::find($order->id);
            $set_merchant_txn_order_id->merchant_txn_id = $merchantTxnId;
            $set_merchant_txn_order_id->save();

		    if ($request->payment_method == "credit_debit")
            {

                $get_order_id = $order->id;
                $data = [];

                if ($request->selected_card != 0)
                {
                    // need to get WHICH credit card.



                    $return_url =  env('APP_URL') . "/payment-redirect";
                    $callback_url = env('APP_URL') . "/payment-callback";

                    $data['orderID'] = $order->id;
                    $data['amount'] = intval(($total_price*100).'');

                    $this_user_id = $order->user->id;

                    $card = Card::where('user_id', $this_user_id)->first();
                    // get according to credit card


                    $epkey = $card->token;

                    $hash = SiteController::instance()->hashGen($data);

                    $curl = curl_init($hash);

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api-staging.pay.asia/api/payment/submit",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "version=1.5.4&v_currency=MYR&v_amount=" .$total_price. "&v_cartid=".$data['orderID']. "&signature=". $hash ."&CID=M161-U-20197&recurringtype=ANNUAL&returnurl=" . $return_url . "&callbackurl=".$callback_url."&epkey=" . $epkey,
                        CURLOPT_HTTPHEADER => array(
                        ),
                    ));

                    // RESPONSE FROM GKASH

                    $response = curl_exec($curl);
                    curl_close($curl);


                    // SINI HOI KENA TUKAR BILA DAH PAKAI MULTIPLE CARD

                    $new2 = json_decode($response);

//                $card = Card::where('user_id', $user->id)->first();
                    $cards = Card::where('user_id', $user->id)->get();

                    foreach ($cards as $card)
                    {
                        $card->token = $new2->epkey;
                        $card->save();
                    }


                    $financial = new FinancialTxn;
                    $financial->type = 'credit';
                    $financial->order_id = $order->id;
                    $financial->amount = (float) $total_price;
                    $financial->payment_callback = $response;
                    $financial->user_id = $user->id;
                    $financial->source = 'card';
                    $financial->save();

                    $order2 = Order::find($order->id);
                    $order2->payment_status = 'paid';
                    $order2->save();

                    Mail::to($user)->send(new NewOrder($order2, $user->name));;

                    Session::forget('orders');


                } else {

                    $data['orderID'] = $order->id;
                    $data['amount'] = intval(($total_price*100).'');
                    $hash = SiteController::instance()->hashGen($data);

                    $return_url =  env('APP_URL') . "/payment-redirect";
                    $callback_url = env('APP_URL') . "/payment-callback";

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api-staging.pay.asia/api/PaymentForm.aspx',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'version=1.5.4&v_currency=MYR&v_amount=' . $total_price . '&v_cartid='.$data['orderID']. '&signature=' . $hash . '&preselection=ECOMM&CID=M161-U-20197&returnurl=' . $return_url . "&callbackurl=" . $callback_url,
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/x-www-form-urlencoded',
                        ),
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);



                    $financial = new FinancialTxn;
                    $financial->type = 'credit';
                    $financial->order_id = $order->id;
                    $financial->amount = (float) $total_price;
                    $financial->payment_callback = $response;
                    $financial->user_id = $user->id;
                    $financial->source = 'card';
                    $financial->save();

                    $order2 = Order::find($order->id);
                    $order2->payment_status = 'paid';
                    $order2->save();

//                    Mail::to($user)->send(new NewOrder($order2, $user->name));

                    Session::forget('orders');

                    $dom = new Dom();
                    $dom->loadStr($response);
                    $a = $dom->getElementsByTag('form')[1];
                    $m = $a->getAttribute('action');
                    $m = str_replace("./","",$m);

//                    return 'https://api-staging.pay.asia/api/' . $m ;

                    return response()->json([
                        'id' => $order->id,
                        'message' => 'Order has been created.',
                        'data' => 'https://api-staging.pay.asia/api/' . $m,
                        'status' => 'success'
                    ], 200);


                }






            }

            if ($request->payment_method == "gcash")
            {
                $wallet = UserWallet::where('user_id', $user->id)->first();
                $wallet->balance = $wallet->balance - $total_price;
                $wallet->save();

                $financial = new FinancialTxn;
                $financial->type = 'credit';
                $financial->order_id = $order->id;
                $financial->amount = (float) $total_price;
                $financial->user_id = $user->id;
                $financial->source = 'wallet';
                $financial->save();

                $order2 = Order::find($order->id);
                $order2->payment_status = 'paid';
                $order2->save();

                Mail::to($user)->send(new NewOrder($order2, $user->name));

                Session::forget('orders');

            }

            if ($request->payment_method == "online_banking")
            {

                $data['orderID'] = $order->id;
                $data['amount'] = intval(($total_price*100).'');
                $hash = SiteController::instance()->hashGen($data);

                $return_url =  env('APP_URL') . "/frontend/consumer/order/progress?id=" . $order->id;
                $callback_url = env('APP_URL') . "/payment-callback";

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api-staging.pay.asia/api/PaymentForm.aspx',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => 'version=1.5.4&v_currency=MYR&v_amount=' . $total_price . '&v_cartid='.$data['orderID']. '&signature=' . $hash . '&preselection=ECOMM&CID=M161-U-20197&returnurl=' . $return_url . "&callbackurl=" . $callback_url,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/x-www-form-urlencoded',
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                $financial = new FinancialTxn;
                $financial->type = 'credit';
                $financial->order_id = $order->id;
                $financial->amount = (float) $total_price;
                $financial->payment_callback = $response;
                $financial->user_id = $user->id;
                $financial->source = 'card';
                $financial->save();

                $order2 = Order::find($order->id);
                $order2->payment_status = 'paid';
                $order2->save();

//                    Mail::to($user)->send(new NewOrder($order2, $user->name));

                Session::forget('orders');

                $dom = new Dom();
                $dom->loadStr($response);
                $a = $dom->getElementsByTag('form')[1];
                $m = $a->getAttribute('action');
                $m = str_replace("./","",$m);

//                    return 'https://api-staging.pay.asia/api/' . $m ;

                return response()->json([
                    'id' => $order->id,
                    'message' => 'Order has been created.',
                    'data' => 'https://api-staging.pay.asia/api/' . $m,
                    'status' => 'success'
                ], 200);
                // above is for online banking
            }



			return response()->json([
			    'id' => $order->id,
				'message' => 'Order has been created.',
				'data' => $order,
				'status' => 'success'
			], 200);
		}

    }

	public function reOrder(Request $request)
    {
		$order_id = $request->order_id;
		$order = Order::find($order_id);

		$received_orders = [];
		$counter = 0;
		$total_amount = 0;

		foreach($order->orderDetails as $detail)
		{
			$image = $detail->menu->images->first();
			$received_orders['data'][$counter]['menu_id'] = $detail->ref_id;
			$received_orders['data'][$counter]['name'] = $detail->menu->name;
			$received_orders['data'][$counter]['image_url'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;
			$received_orders['data'][$counter]['menu_price'] = $detail->menu->price;
			$received_orders['data'][$counter]['total_price_exc_sst'] = $detail->menu->price;
			$received_orders['data'][$counter]['qty'] = $detail->quantity;
			$received_orders['data'][$counter]['settings'] = json_decode($detail->data,true);

			// $total_amount += $received_orders['data'][$counter]['total_price_exc_sst'] * $received_orders['data'][$counter]['qty'];
			$counter++;
		}

		if(!$this->checkDeliveryTime($received_orders, $request->delivery_date, $request->delivery_timing))
		{
			return response()->json([
				'message' => 'Delivery time is not available.',
				'status' => 'failed'
			], 400);
		}

		$newOrder = $order->replicate();
		$newOrder->address = $request->address;
		$newOrder->state = $request->state;
		$newOrder->city = $request->city;
		$newOrder->postal_code = $request->postal_code;
		$newOrder->delivery_date = $request->delivery_date;
		$newOrder->delivery_timing = $request->delivery_timing;
		$newOrder->payment_status = 'unpaid';
		$newOrder->order_status = 'pending';
		$newOrder->save();

		$order_details = $order->orderDetails;
		foreach($order_details as $order_detail)
		{
			$newOrderDetail = $order_detail->replicate();
			$newOrderDetail->order_id = $newOrder->id;
			$newOrderDetail->order_status = 'pending';
			$newOrderDetail->save();
		}

		return response()->json([
			'id' => $order->id,
			'message' => 'Order has been created.',
			'data' => $order,
			'status' => 'success'
		], 200);
    }

	public function checkPromoCode($promo_code)
    {
		if($promo_code)
		{
			$dt = Carbon::now()->format('Y-m-d');
			$promo_code_start_date = Carbon::parse($promo_code->start_date)->format('Y-m-d');
			$promo_code_end_date = Carbon::parse($promo_code->end_date)->format('Y-m-d');

			if ( Carbon::parse($promo_code_start_date)->lte(Carbon::now()) && Carbon::parse($promo_code_end_date)->gte(Carbon::now()) )
			{
				return true;
			}
		}
		return false;
	}

	public function promoCodeInfo(Request $request)
    {
		$promo_code = PromoCode::where('code',$request->promo_code)->first();
		if($promo_code)
		{
			if($this->checkPromoCode($promo_code))
			{
				$data['type'] = $promo_code->discount_type;
				$data['value'] = $promo_code->discount_type != 'free_delivery' ? $promo_code->discount_value : null;
				$data['redemption_amount'] = $promo_code->redemption_amount;
				return response()->json([
					'message' => 'Promo code info.',
					'data' => $data,
					'status' => 'success'
				], 200);
			}
		}
		return response()->json([
			'message' => 'Invalid promo code.',
			'status' => 'failed'
		], 200);
	}

    public function orderList(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;

        $data = [];
        $data['status'] = 'success';

        $orders = Order::where(
            [
                ['user_id', '=', $user_id],
                ['payment_status', '=', 'paid'],
            ]
        )
            ->get();





        foreach($orders as $key => $order)
        {

            $description = "";

            $order_status = $order->order_status;

            switch ($order_status) {
                case "pending":
                    $description = "Your order is still pending.";
                    break;
                case "received":
                    $description = "Your order has been received.";
                    break;
                case "preparing":
                    $description = "Our chef is preparing your food.";
                    break;
                case "completed":
                    $description = "Enjoy your food!";
                    break;
                case "delivering":
                    $description = "Your order is out for delivery.";
                    break;
                case "pickup":
                    $description = "Your order has arrived.";
                    break;
                case "cancelled":
                    $description = "Your order has been cancelled.";
                    break;
                default:
                    $description = "Your order is still pending.";
            }


            $data['data'][$key]['status'] = $order->order_status;
            $data['data'][$key]['status_description'] = $description;
            $data['data'][$key]['order_id'] = $order->id;
            $data['data'][$key]['price'] = $order->price;
            $data['data'][$key]['created_at'] = $order->created_at->format('d-m-Y h:i:s');

            foreach ($order->orderDetails as $key2 => $detail)
            {
                $data['data'][$key]['macro'][$key2]['menu_id'] = $detail->menu->id;
                $data['data'][$key]['macro'][$key2]['name'] = $detail->menu->name;
                $data['data'][$key]['macro'][$key2]['quantity'] = $detail->quantity;
                $data['data'][$key]['macro'][$key2]['price'] = $detail->price;

                $order_details = json_decode($detail->data);

                $data['data'][$key]['macro'][$key2]['settings'] = $order_details;

            }
        }

        return $data;
    }

	public function orderCheckout(Request $request)
    {
		$user = $request->user();
		$order_id = $request->order_id;
		$promo_code = $request->promo_code;
		$discount_type = null;
		$discount_value = 0;
		$discount_amount = 0;

		if($promo_code)
		{
			$promo_code = PromoCode::where('code',$promo_code)->where('status','active')->first();

			if(!$this->checkPromoCode($promo_code))
			{
				return response()->json([
					'message' => 'Invalid promo code.',
					'status' => 'failed'
				], 400);
			}

			$discount_type = $promo_code->discount_type;
			$discount_value = $promo_code->discount_value;

		}

		$order = Order::where('id',$order_id)->where('user_id',$user->id)->first();

		if($order)
		{
			$data = [];
			$counter = 0;
			$sst_amount = 0;
			$total_amount = $order->price;

			foreach($order->orderDetails as $detail)
			{
				$image = $detail->menu->images->first();
				$data[$counter]['menu_id'] = $detail->ref_id;
				$data[$counter]['name'] = $detail->menu->name;
				$data[$counter]['image_url'] = $image ? env('APP_URL').'/'.$image->path.'/'.$image->filename : null;
				$data[$counter]['menu_price'] = $detail->menu->price;
				$data[$counter]['total_price_exc_sst'] = $detail->menu->price;
				$data[$counter]['qty'] = $detail->quantity;
				$data[$counter]['settings'] = json_decode($detail->data,true);
				$counter++;
			}

			if($discount_type == 'fixed_rate')
			{
				$discount_amount = $discount_value;
			}
			elseif($discount_type == 'percentage')
			{
				$discount_amount = ($total_amount * $discount_value)/100;
			}

			$sst_amount = ($total_amount * 6)/100; /*hard code sst to 6%*/
			$total_amount += $sst_amount;
			$total_amount -+ $discount_amount;
			if($user->wallet->balance > 0)
			{
				$total_amount -= $user->wallet->balance;
			}
			return response()->json([
				'status' => 'success',
				'sst_amount' => $sst_amount,
				'discount_type' => $discount_type,
				'discount_value' => $discount_value,
				'discount_amount' => $discount_amount,
				'wallet_amount' => (float) $user->wallet->balance,
				'total_amount' => $total_amount,
				'final_total_amount' => $total_amount > 0 ? $total_amount : 0,
				'data' => $data
			], 200);
		}

		return response()->json([
			'message' => 'Record not found.',
			'status' => 'failed'
		], 400);
    }

	public function orderStatus(Request $request)
    {
        $user = $request->user();
        $order_id = $request->order_id;

        $order = Order::where(
		[
			['id', '=', $order_id],
			['user_id', '=', $user->id],
			['payment_status', '=', 'paid']
		]
        )->first();

		if(!$order)
		{
			return response()->json([
				'message' => 'Record not found.',
				'status' => 'failed'
			], 400);
		}


		$order_status = $order->order_status;

		switch ($order_status) {
		  case "pending":
			$description = "Your order is still pending.";
			break;
		  case "received":
			$description = "Your order has been received.";
			break;
		  case "processing":
			$description = "Our chef is preparing your food.";
			break;
		  case "completed":
			$description = "Your order is ready for delivery.";
			break;
		  case "delivering":
			$description = "Your order is out for delivery.";
			break;
		  case "pickup":
			$description = "Your order has arrived.";
			break;
		  case "cancelled":
			$description = "Your order has been cancelled.";
			break;
		  default:
			$description = "Your order is still pending.";
		}

		$data = [
			'order_status' => $order_status,
			'description' => $description
		];
		return response()->json([
			'message' => 'Order status.',
			'data' => $data,
			'status' => 'success'
		], 200);
    }



    public static function instance()
    {
        return new self;
    }

	public function reviewList(Request $request)
    {
		$user = $request->user();
		$ref_type = $request->ref_type;
		$ref_id = $request->ref_id;
		$page = $request->page;
		$per_page = $request->per_page;
		$status = 'success';

		$query = Review::where('ref_type',$ref_type)
						->where('ref_id',$ref_id)
						->where('approval_status','approved')
						->paginate($per_page);;
		$list = [];
		$counter = 0;
		foreach($query as $data)
		{
			$list[$counter]['id'] = $data->id;
			$list[$counter]['reviewed_by'] = $data->user->name;
			$list[$counter]['rating'] = $data->rate;
			$list[$counter]['message'] = $data->text;
			$counter++;
		}

		return response()->json([
			'message' => 'Review list.',
			'data' => $list,
			'status' => $status
		], 200);
    }

	public function createReview(Request $request)
    {
		$user = $request->user();
		$ref_type = $request->ref_type;
		$ref_id = $request->ref_id;
		$rating = $request->rating;
		$message = $request->message;

		$validator = Validator::make($request->all(), [
            'ref_type' => 'required|string',
            'ref_id' => 'required|numeric',
            'rating' => 'required|numeric',
            'message' => 'required|string'
        ]);


        if($validator->fails()){
            $response = [
				'message' => 'The given data was invalid.',
				'errors' => $validator->errors(),
				'status' => false,
			];
			return response()->json($response, 400);
        }

        $data = new Review([
            'user_id' => $user->id,
            'ref_type' => $request->ref_type,
            'ref_id' => $request->ref_id,
            'rate' => $request->rating,
            'text' => $request->message,
        ]);

        if($data->save())
		{
			return response()->json([
				'message' => 'Successfully created record.',
				'data' => $data,
				'status' => true
			], 201);
		}
		return response()->json([
			'message' => 'Something went wrong, please try again.',
			'status' => false
		], 400);
    }

	public function staticPageInfo(Request $request)
    {
		$id = $request->id;

        $status = 'success';

        $data = [];
		$page = DB::table('static_pages')->where('id',$id)->first();

		if($page)
		{
			$data['id'] = $page->id;
			$data['title'] = $page->title;
			$data['html'] = $page->html;

			return response()->json([
				'message' => 'Static page info.',
				'data' => $data,
				'status' => $status
			], 200);
		}
		return response()->json([
			'message' => 'Something went wrong, please try again.',
			'status' => false
		], 400);
    }

    public function cards(Request $request)
    {
        $user = $request->user();

        $cards = Card::where('user_id', $user->id)
        ->get();

        $new = [];

        if ($cards){
            $new['status'] = 'success';
            $new['data'] = $cards;
        } else {
            $new['status'] = 'failed';
            $new['data'] = [];
        }

        return $new;

    }

    public function deleteCard(Request $request)
    {
        $user = $request->user();
        $card_id = $request->card_id;
        $card = Card::where('id',$card_id)->first();
        if(!$card){
            $response = [
                'message' => 'Card not found.',
                'status' => 'failed',
            ];
            return response()->json($response, 400);
        }

        if($card->delete())
        {
            $new_data = Card::where('user_id', $user->id)
                ->get();

            return response()->json([
                'message' => 'Card has been deleted.',
                'data' => $new_data,
                'status' => 'success'
            ], 200);
        }
    }

    public function paymentPreAuth(Request $request)
    {

        $user = $request->user();
        $user_id = $user->id;

//        return $user_id;

        //$data['orderID']
        //$data['amount']
        $data = [];
        $data['orderID'] = rand(99,9999);
        $orderID = $data['orderID'];

        $data['amount'] = 100;
//        $url = 'https://9ed665f81423.ngrok.io';
        $return_url = env('APP_URL') . "/preauth-payment-redirect";
//        $callback_url = "https://8f7b23be7702.ngrok.io/preauth-payment-callback"; // NEED TO CHANGE THIS TO THE SERVER ONE
        $callback_url = env('APP_URL') . "/preauth-payment-callback";

        $card = new Card();
        $card->preauth_cart_id = 'a' . $orderID;
        $card->four_digits = $request->four_digits;
        $card->expiry_date = $request->expiry_dates;
        $card->card_name = $request->card_name;
        $card->user_id = $user_id; // CHANGE to the right one
        $card->save();

        $hash = SiteController::instance()->hashGen($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-staging.pay.asia/api/PaymentForm.aspx",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "version=1.5.4&v_currency=MYR&v_amount=1.00&v_cartid=" . $orderID . "&signature=" . $hash . "&CID=M161-U-20197&recurringtype=ANNUAL&v_preauth=PREAUTH&returnurl=". $return_url . "&callbackurl=" . $callback_url,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $payment = curl_exec($curl);

        curl_close($curl);

        $dom = new Dom();
        $dom->loadStr($payment);
        $a = $dom->getElementsByTag('form')[1];
        $m = $a->getAttribute('action');
        $m = str_replace("./","",$m);

        return 'https://api-staging.pay.asia/api/' . $m ;

    }

    public function transaction(Request $request)
    {
        $user = $request->user();

        $txn = FinancialTxn::where('user_id', $user->id)
            ->get();

        $wallet = UserWallet::where('user_id', $user->id)->first();


        $new = [];

        if ($txn){
            $new['status'] = 'success';
            $new['balance'] = $wallet->balance;
            $new['data'] = $txn;
        } else {
            $new['status'] = 'failed';
            $new['balance'] = 0;
            $new['data'] = [];
        }

        return $new;

    }

    public function userWallet(Request $request)
    {
        $user = $request->user();

        $wallet = UserWallet::where('user_id', $user->id)->get();

        $new = [];

        if ($wallet) {
            $new['status'] = 'success';
            $new['data'] = $wallet;
        } else {
            $new['status'] = 'failed';
            $new['data'] = $wallet;
        }

        return $new;
    }

    public function delivery(Request $request)
    {
        $user = $request->user();

        $lambo = LamboTrail::where('user_id', $user->id)->get();

        $new = [];

        if ($lambo) {
            $new['status'] = 'success';
            $new['data'] = $lambo;
        } else {
            $new['status'] = 'failed';
            $new['data'] = $lambo;
        }

        return $new;
    }

    public function orderDetail(Request $request)
    {

        $user = $request->user();
        $user_id = $user->id;
        $order_id = $request->order_id;

        $data = [];
        $data['status'] = 'success';

        $order = Order::where(
            [
                ['id', '=', $order_id],
            ]
        )
            ->first();


        foreach ($order->orderDetails as $key => $detail)
        {
            $ada = false;

            $data['data'][$key]['menu_id'] = $detail->menu->id;
            $data['data'][$key]['name'] = $detail->menu->name;
            $data['data'][$key]['quantity'] = $detail->quantity;
            $data['data'][$key]['price'] = $detail->price;

            $order_details = json_decode($detail->data);

            $data['data'][$key]['settings'] = $order_details;

        }



        return $data;
    }












}
