<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\User;
use App\UserAddress;
use Validator;
use Hash;

class AuthController extends Controller
{
	use ResetsPasswords;

    public function signup(Request $request)
    {
		/*
        $request->validate([
            'name' => 'required|string',
            'nric' => 'required|string|unique:users',
            'company' => 'required|string',
            'email' => 'required|string|email|unique:users',
			'device_platform' => 'required|string',
            // 'password' => 'required|string|confirmed'
            'password' => 'required|string'
        ]);
		*/

		$validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'nric' => 'required|string|unique:users',
            'company' => 'required|string',
            'email' => 'required|string|email|unique:users',
			'device_platform' => 'required|string',
            // 'password' => 'required|string|confirmed'
            'password' => 'required|string'
        ]);


        if($validator->fails()){
            $response = [
				'message' => 'The given data was invalid.',
				'errors' => $validator->errors(),
				'status' => false,
			];
			return response()->json($response, 400);
        }

        $user = new User([
            'name' => $request->name,
            'nric' => $request->nric,
            'company' => $request->company,
            'email' => $request->email,
            'device_platform' => $request->device_platform,
            'password' => bcrypt($request->password)
        ]);
        $user->save();

		// $user->employee_id = $user->id + 100;
		// $user->save();

        // return response()->json([
            // 'message' => 'Successfully created user!'
        // ], 201);
		return response()->json([
            'message' => 'Successfully created user.',
			'status' => true
        ], 201);
    }

    public function login(Request $request)
    {
		/*
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'device_platform' => 'required|string',
            // 'remember_me' => 'boolean'
        ]);
		*/

        $fb_id = $request->fb_id; // this should be only on the first screen
//        $fb_id = "10157976651713878";
        $user = "";


        if (!$fb_id)
        {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email',
                'password' => 'required|string',
                // 'device_platform' => 'required|string',
            ]);


            if($validator->fails()){
                $response = [
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors(),
                    'status' => false,
                ];
                return response()->json($response, 400);
            }

            $credentials = request(['email', 'password']);

            if(!Auth::attempt($credentials))
                return response()->json([
                    'message' => 'Invalid email or password for login.',
                    'status' => false
                ], 401);
            $user = $request->user();

            if($user->status == "inactive")
                return response()->json([
                    'message' => 'Account has not been approved yet.',
                    'status' => false
                ], 401);
        } else {

            $user = User::fb($fb_id)
            ->first();

            if (!$user)
            {
                return response()->json([
                    'message' => 'Invalid FB ID',
                    'status' => false
                ], 401);
            };

        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        // if ($request->remember_me)
            // $token->expires_at = Carbon::now()->addWeeks(1);
        // $token->save();
		// $user->device_platform = $request->device_platform;
		$user->save();

        // return response()->json([
            // 'access_token' => $tokenResult->accessToken,
            // 'token_type' => 'Bearer',
            // 'expires_at' => Carbon::parse(
                // $tokenResult->token->expires_at
            // )->toDateTimeString()
        // ]);

		return response()->json([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
			'access_token' => $tokenResult->accessToken,
			'status' => true
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {

        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out',
			'status' => true
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {

		$user = $request->user();

		return response()->json([
			'message' => 'User details.',
			'data' => $user,
			'status' => 'success'
		], 200);
    }

	public function update(Request $request)
    {
		$user = $request->user();

		if($user->type == 'employee')
		{
			return response()->json([
			'message' => 'Invalid account.',
			'status' => 'failed',
			], 401);
		}

		$validator = Validator::make($request->all(), [
            'name' => 'required|string',
			'email' => 'required|email|unique:users,email,'.$user->id,
            'birthdate' => 'required|string',
			'gender' => 'required|string'
        ]);


        if($validator->fails()){
            $response = [
				'message' => 'The given data was invalid.',
				'errors' => $validator->errors(),
				'status' => 'failed',
			];
			return response()->json($response, 400);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->birthdate = $request->birthdate;
        $user->gender = $request->gender;
        if($user->save())
		{
			if($request->image!="")
			{
				$img_filePath = $user->image_path.'/'.$user->image_filename;
				if ($user->image_path && file_exists($img_filePath)) {
					unlink($img_filePath);
				}
				
				$destinationPath = 'uploads/profiles/images'; 
				$image = $request->file('image');
				$filename = $user->id . time() . "." . $image->getClientOriginalExtension();
				$image->move($destinationPath, $filename);
				
				$user->image_path = $destinationPath;
				$user->image_filename = $filename;
				$user->save();
			}
			return response()->json([
				'message' => 'User has been updated.',
				'data' => $user,
				'status' => 'success'
			], 200);
		}
    }

	public function changePassword(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'current_password' => 'required|string',
            'new_password' => 'required|string|min:8'
        ]);


        if($validator->fails()){
            $response = [
				'message' => $validator->errors()->first(),
				'status' => 'failed',
			];
			return response()->json($response, 400);
        }

        $user = $request->user();
		if(Hash::check($request->current_password, $user->password))
		{
			$user->password = bcrypt($request->new_password);
			if($user->save())
			{
				return response()->json([
					'message' => 'Successfully changed password.',
					'status' => true
				], 201);
			}
		}
		return response()->json([
			'message' => 'Invalid current password.',
			'status' => 'failed',
		], 201);
    }

	public function addressList(Request $request)
    {
		$user = $request->user();
		$addresses = $user->addresses;
		return response()->json([
			'message' => 'Address List.',
			'data' => $addresses,
			'status' => 'success'
		], 200);
    }

	public function addAddress(Request $request)
    {
		$user = $request->user();
		$validator = Validator::make($request->all(), [
            'title' => 'required|string',
			'address' => 'required|string',
            'state' => 'required|string',
			'city' => 'required|string',
			'postal_code' => 'required|numeric',
        ]);


        if($validator->fails()){
            $response = [
				'message' => 'The given data was invalid.',
				'errors' => $validator->errors(),
				'status' => 'failed',
			];
			return response()->json($response, 400);
        }

		$address = new UserAddress;
        $address->title = $request->title;
        $address->user_id = $user->id;
        $address->address = $request->address;
        $address->state = $request->state;
        $address->city = $request->city;
        $address->postal_code = $request->postal_code;

        if($address->save())
		{


		    $new_data = UserAddress::where('user_id', $user->id)
                ->get();

			return response()->json([
				'message' => 'Address has been added.',
				'data' => $new_data,
				'status' => 'success'
			], 200);
		}
    }

	public function deleteAddress(Request $request)
    {
		$user = $request->user();
		$address_id = $request->address_id;
		$address = UserAddress::where('id',$address_id)->where('user_id',$user->id)->first();
        if(!$address){
            $response = [
				'message' => 'Address not found.',
				'status' => 'failed',
			];
			return response()->json($response, 400);
        }

		if($address->delete())
		{
            $new_data = UserAddress::where('user_id', $user->id)
                ->get();

			return response()->json([
				'message' => 'Address has been deleted.',
				'data' => $new_data,
				'status' => 'success'
			], 200);
		}
    }

}
