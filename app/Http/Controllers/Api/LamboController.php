<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LamboTrail;

class LamboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bodyContent = $request->getContent();
        $status = json_decode($bodyContent);
        
        if (json_last_error() === JSON_ERROR_NONE) {
            $lambotrail = new LamboTrail;
            $lambotrail->merchant_txn_id = $status->merchantTransactionId;
            $lambotrail->jobstatus = $status->jobStatus;
            $lambotrail->jobdescription = $status->jobStatusDescr;
            $lambotrail->jobstatustime = $status->jobStatusTime;
            $lambotrail->ridername = $status->riderName != null ? $status->riderName : '';
            $lambotrail->ridercarplatenumber = $status->riderCarPlateNo != null ? $status->riderCarPlateNo : '';
            $lambotrail->ridercontact = $status->riderContact != null ? $status->riderContact : '';
            $lambotrail->save();

            return response()->json(['status'=>true]);
        }else{
            return response()->json(['status'=>false]);
        }


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
