<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller as Controller;
use App\User;
use App\NotificationData;
use App\UserNotification;
use Validator;
use Hash;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
		$user = $request->user();
		$query = NotificationData::where('status','published')->orderBy('updated_at','desc')->paginate(15);
		$list = [];
		$counter = 0;
		foreach($query as $data)
		{
			if(!UserNotification::where('user_id',$user->id)->where('notification_id',$data->id)->exists())
			{
				$user_notification = new UserNotification;
				$user_notification->user_id = $user->id;
				$user_notification->notification_id = $data->id;
				$user_notification->status = 'viewed';
				$user_notification->save();
			}
			$list[$counter]['notification_id'] = $data->id;
			$list[$counter]['notification_title'] = $data->title;
			$list[$counter]['notification_message'] = $data->message;
			$list[$counter]['timestamp'] = strtotime($data->updated_at);
			$list[$counter]['notification_type'] = $data->type;
			$counter++;
		}
		
		return $list;
    }
	
	public function show(Request $request)
    {
		$user = $request->user();
		$notification = NotificationData::find($request->notification_id);
		
		if(!UserNotification::where('user_id',$user->id)->where('notification_id',$request->notification_id)->exists())
		{
			$user_notification = new UserNotification;
			$user_notification->user_id = $user->id;
			$user_notification->notification_id = $notification->id;
			$user_notification->status = 'viewed';
			$user_notification->save();
		}
		return response()->json([
			'message' => 'Successfully updated record.',
			'data' => $notification,
			'status' => true
		], 201);
    }
}