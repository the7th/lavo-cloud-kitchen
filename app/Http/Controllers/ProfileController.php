<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\CuisineType;
use App\ChefCuisineType;
use App\ChefCompany;
use Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->cuisine_types = CuisineType::where('status','active')->pluck('name','id');
    }
	
	public function edit()
    {
		$data = Auth::user();
		$cuisine_types = $this->cuisine_types;
		
		return view('dashboard.profile.edit', compact('data','cuisine_types'));
    }
	
	public function update(ProfileRequest $request)
    {
		$user = Auth::user();
		$user->name = $request->name;
		$user->contact_number = $request->contact_number;
		$user->email = $request->email;
		$user->position = $request->position;
		if($user->type == 'chef')
		{
			$user->type = 'chef';
			$user->brand_name = $request->brand_name;
			$user->base_type = $request->base_type;
		}
		if($request['password'] != "")
		{
			$user->password = Hash::make($request['password']);
		}
		
		if($user->save())
		{
			if($user->type == 'chef')
			{
				if($request->base_type == 'Company')
				{
					$chef_company = ChefCompany::where('user_id',$user->id)->first();
					if(!$chef_company)
					{
						$chef_company = new ChefCompany;
					}
					$chef_company->user_id = $user->id;
					$chef_company->company_name = $request->company_name;
					$chef_company->company_registration_number = $request->company_registration_number;
					$chef_company->save();
				}
				
				$remove_cuisine_types = ChefCuisineType::where('user_id',$user->id)->delete();
				foreach($request->cuisine_types as $cuisine_type_id)
				{
					$chef_cuisine_type = new ChefCuisineType;
					$chef_cuisine_type->user_id = $user->id;
					$chef_cuisine_type->cuisine_type_id = $cuisine_type_id;
					$chef_cuisine_type->save();
				}
			}
			
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success'); 
			return redirect()->route('profile.edit')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger'); 
		return redirect()->back()->withMessage($message);
    }
}
