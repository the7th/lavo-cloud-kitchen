<?php

namespace App\Http\Controllers;

use App\FinancialTxn;
use App\UserWallet;
use Illuminate\Http\Request;

class FinanceController extends Controller
{

    public function walletIndex()
    {

        $users = UserWallet::all();

        return view('dashboard.finance.wallets.index', compact('users'));

    }

    public function txnIndex()
    {

        $transactions = FinancialTxn::all();

        return view('dashboard.finance.transactions.index', compact('transactions'));
    }
}
