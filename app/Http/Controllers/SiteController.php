<?php

namespace App\Http\Controllers;

use App\Card;
use App\FinancialTxn;
use App\Order;
use GuzzleHttp\Client;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PHPHtmlParser\Dom;

class SiteController extends Controller
{

// this one is controller for site that doesn't need login.
// e.g. check data first when first enter site

    public function auth()
    {

        $new = [];

        if (Auth::check())
        {

            $user = Auth::user();

            $tokenResult = $user->createToken('Personal Access Token');

            $new['status'] = true;
            $new['id'] = $user->id;
            $new['name'] = $user->name;
            $new['email'] = $user->email;
            $new['access_token'] = $tokenResult->accessToken;

        } else {
            $new['status'] = false;
        }

        return $new;
    }

    public function getToken(Request $request)
    {

        $new = [];

        if ($request->session()->has('token'))
        {
            $new['status'] = true;
            $new['token'] = $request->session()->get('token');
        } else {
            $new['status'] = false;
            $new['token'] = "";
        }

        Session::forget('token');

        return $new;
    }

    public function logout()
    {

        if (Auth::check())
        {
            Auth::logout();

        }

        return response()->json([
            'message' => 'Successfully logged out',
            'status' => true
        ]);
    }

    public function hashGen($data)
    {
        //$data['orderID']
        //$data['amount']
        return hash('sha512','FCVCCQ6WMKNCN3F;M161-U-20197;'.$data['orderID'].';'. $data['amount'] . ';MYR');

    }

    public function accessHashGen()
    {

        $data['orderID'] = rand(46,9999);
        $data['amount'] = 100;

        return  $data['orderID'] . ' ' . SiteController::instance()->hashGen($data);


    }

    public function paymentCallback(Request $request)
    {

        $new2 = json_encode($request->all());

//        $card = Card::where('preauth_cart_id', 'a' . json_decode($new2)->cartid)->first();

//        $card->preauth_callback_data = $request->all();

//        if (json_decode($new2)->epkey)
//        {
//            $card->token = json_decode($new2)->epkey;
//        }

//        $card->save();

        // this one is when receiving after place order.
        $order_id = json_decode($new2)->cartid;

        $order = Order::find($order_id);

//        if($request->description)
//        {







            $order->payment_status = 'paid';
            $order->save();

            $finance = FinancialTxn::where('order_id', $order_id)->first();

            $finance->type = 'credit';
            $finance->amount = $request->amount;
            $finance->payment_callback = $new2;
            $finance->save();

            $user_id = $finance->user->id;
            $card = Card::where('user_id', $user_id)->first();
            $card->token = json_decode($new2)->epkey;
            $card->preauth_callback_data = json_encode($request->all());
            $card->save();



//        }
//        else {
//            $finance = FinancialTxn::where('order_id', $order_id)
//                ->first();
//
//            $finance->type = 'credit';
//            $finance->amount = $request->amount;
//            $finance->payment_callback = json_encode($request->all());
//            $finance->save();
//        }

    }

    public function redirectPreAuth(Request $request)
    {
        return redirect()->route('redirector');
//        return redirect()->route('frontend.consumer.profile');
    }



    public function paymentCallbackPreAuth(Request $request)
    {

        $new2 = json_encode($request->all());

        $card = Card::where('preauth_cart_id', 'a' . json_decode($new2)->cartid)->first();

        $card->preauth_callback_data = $request->all();

        if (json_decode($new2)->epkey)
        {
            $card->token = json_decode($new2)->epkey;
        }

        $card->save();

        return $card;





    }


    public function redirectPayment(Request $request)
    {
        if ($request->description == "00 - Approved") {
            return redirect()->route('redirector');
        } else {
            return redirect()->route('frontend.consumer.order.failed');
        }

    }

    public function googleAutocomplete(Request $request)
    {

        if ($request->search_term)
        {
            $search_term = $request->search_term;
        } else {
            $search_term = 'Menara Lien Hoe';
        }


        $client = new Client();
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=3.1305,101.5951&radius=5000&type=regions&key=AIzaSyC9ciir8FyuSNCm7JrCJk5d7xpOoNC2ghk
        $result = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' .  $search_term .'&location=3.1305,101.5951&radius=5000&strictbounds&key=AIzaSyC9ciir8FyuSNCm7JrCJk5d7xpOoNC2ghk');


        $info = json_decode($result->getBody());


        $new = [];
        $new['status'] = $info->status;

        if ($info->status == "OK")
        {
            $new['data'] = [
              [
                  'id' => 1,
                  'name' => 'Yes, we covered ' . ucwords($request->search_term)
              ]
            ];

            if (!$request->search_term)
            {
                $new['data'] = [
                    [
                        'id' => 1,
                        'name' => 'Please search for your city to start'
                    ]
                ];
            }

        } else {
            //ZERO_RESULTS status
            $new['data'] = [
                [
                    'id' => 1,
                'name' => "We don't cover "  . ucwords($request->search_term) . " yet :("
                ]
            ];
        }

//
//        foreach ($info->results as $key => $result)
//        {
//            $new['data'][$key]['result'] = $result;
////            $new['data'][$key]['id'] = $key+1;
////            $new['data'][$key]['name'] = $result->name;
//        }


        return $new;


//
//
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' . $search_term . '&origin=3.1305,101.5951&location=3.1305,101.5951&radius=5000&strictbounds&key=AIzaSyC9ciir8FyuSNCm7JrCJk5d7xpOoNC2ghk',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'GET',
//        ));
//
//        $response = curl_exec($curl);
//
//        curl_close($curl);
//
//        dd($response);



//        return $response;


    }


    public static function instance()
    {
        return new self;
    }

}
