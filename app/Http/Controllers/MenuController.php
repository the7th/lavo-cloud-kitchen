<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsumerRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Menu;
use App\MenuImage;
use App\MenuCategory;
use App\Image;
use App\ImageCategory;
use Auth;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		$this->destinationPath = 'uploads/menus/images';
		$this->coverDestinationPath = 'uploads/menus/cover-images';
		$this->menu_categories = MenuCategory::where('status','active')->pluck('name','id');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$data = Menu::ownedBy($user)
					->keyword($request->keyword)
					->orderBy('created_at','desc')
					->paginate(15);
		return view('dashboard.menus.index', compact('data','request'));
    }


	public function create()
    {
		$menu_categories = $this->menu_categories;
		return view('dashboard.menus.create',compact('menu_categories'));
    }

	public function store(Request $request)
    {
		$menu = new Menu;
		$menu->menu_category_id = $request->menu_category;
		$menu->user_id = Auth::user()->id;
		$menu->name = $request->name;
		$menu->description = $request->description;
		$menu->price = $request->price;
		$menu->status = $request->status;

		if($menu->save())
		{
			if ($cover_image = $request->file('cover_image')) {
				$destinationPath = $this->coverDestinationPath;
				
				$filename = $menu->id . time() . "." . $cover_image->getClientOriginalExtension();
				$cover_image->move($destinationPath, $filename);

				$menu->cover_image_path = $this->coverDestinationPath;
				$menu->cover_image_filename = $filename;
				$menu->save();

				$optimizer_chain = OptimizerChainFactory::create();
				$optimizer_chain->optimize($menu->cover_image_path.'/'.$menu->cover_image_filename);
			}
			
			if ($images = $request->file('images')) {
				// list($width, $height) = getimagesize($request->file('image'));

				$destinationPath = $this->destinationPath;
				$image_counter = 0;
				foreach($images as $image)
				{
					$filename = $menu->id . time() . $image_counter . "." . $image->getClientOriginalExtension();
					$image->move($destinationPath, $filename);

					$image = new MenuImage;
					$image->menu_id = $menu->id;
					$image->path = $this->destinationPath;
					$image->filename = $filename;
					$image->save();

					$image_library = new Image;
					$image_library->image_category_id = 1;
					$image_library->url = env('APP_URL').'/'.$image->path.'/'.$image->filename;
					$image_library->save();

                    $optimizer_chain = OptimizerChainFactory::create();
                    $optimizer_chain->optimize($image->path.'/'.$image->filename);

                    $image_counter++;

				}
			}

			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menus.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function edit($id)
    {
		$data = Menu::find($id);
		$menu_categories = $this->menu_categories;
		$settings = json_decode($data->settings,true);
		$meal_settings = json_decode($data->meal_settings,true);
		return view('dashboard.menus.edit', compact('data','menu_categories','settings','meal_settings'));
    }

	public function update(Request $request, $id)
    {
		$menu = Menu::find($id);
		$menu->menu_category_id = $request->menu_category;
		$menu->name = $request->name;
		$menu->description = $request->description;
		$menu->price = $request->price;
		$menu->status = $request->status;

		if($menu->save())
		{
			if ($cover_image = $request->file('cover_image')) {
				
				$img_filePath = $menu->cover_image_path.'/'.$menu->cover_image_filename;
				
				if ($menu->cover_image_path && file_exists($img_filePath)) {
					unlink($img_filePath);
				}
					
				$destinationPath = $this->coverDestinationPath;
				
				$filename = $menu->id . time() . "." . $cover_image->getClientOriginalExtension();
				$cover_image->move($destinationPath, $filename);

				$menu->cover_image_path = $this->coverDestinationPath;
				$menu->cover_image_filename = $filename;
				$menu->save();

				$optimizer_chain = OptimizerChainFactory::create();
				$optimizer_chain->optimize($menu->cover_image_path.'/'.$menu->cover_image_filename);
			}
			
			if ($images = $request->file('images')) {
				// list($width, $height) = getimagesize($request->file('image'));

				$destinationPath = $this->destinationPath;
				$image_counter = 0;
				foreach($images as $image)
				{
					$filename = $menu->id . time() . $image_counter . "." . $image->getClientOriginalExtension();
					$image->move($destinationPath, $filename);

					$image = new MenuImage;
					$image->menu_id = $menu->id;
					$image->path = $this->destinationPath;
					$image->filename = $filename;
					$image->save();

					$image_library = new Image;
					$image_library->image_category_id = 1;
					$image_library->url = env('APP_URL').'/'.$image->path.'/'.$image->filename;
					$image_library->save();

                    $optimizer_chain = OptimizerChainFactory::create();
                    $optimizer_chain->optimize($image->path.'/'.$image->filename);

                    $image_counter++;
				}
			}

			if($request->delete_images)
			{
				foreach($request->delete_images as $image_id)
				{
					$image = MenuImage::find($image_id);
					$img_filePath = $image->path.'/'.$image->filename;
					// if(Storage::disk('s3')->exists($img_filePath)) {
						// Storage::disk('s3')->delete($img_filePath);
					// }
					if (file_exists($img_filePath)) {
						unlink($img_filePath);
					}
					$image->delete();
				}

			}

			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menus.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function delete($id)
    {
		$data = Menu::find($id);
		if($data->delete())
		{
			$message = 'Record has been deleted successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menus.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }

	public function approval(Request $request, $id)
    {
		$data = Menu::find($id);
		$data->approval_status = $request->approval;

		if($data->save())
		{
			$message = 'Record has been updated successfully.';
			Session::flash('alert-class', 'alert-success');
			return redirect()->route('menus.index')->withMessage($message);
		}
		$message = 'There is something wrong, please try again.';
		Session::flash('alert-class', 'alert-danger');
		return redirect()->back()->withMessage($message);
    }
}
