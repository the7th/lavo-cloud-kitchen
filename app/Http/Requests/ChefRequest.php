<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ChefRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$employee = User::find($this->id);
		
        return [
			'brand_name' => ['required', 'string', 'max:255', Rule::unique((new User)->getTable())->ignore($this->id)],
            'name' => ['required', 'string', 'max:255'],
            'contact_number' => ['required', 'string', 'max:25', Rule::unique((new User)->getTable())->ignore($this->id)],
			'email' => ['required', 'string', 'email', 'max:255', Rule::unique((new User)->getTable())->ignore($this->id)],
			'position' => ['required', 'string', 'max:255'],
			'base_type' => ['required', 'string', 'max:255'],
			'password' => $employee ? ['nullable', 'string', 'min:8'] : ['required', 'string', 'min:8'],
			'status' => ['required', 'string', 'max:25'],
        ];
    }
	
	// public function messages()
	// {
		// return [
			// 'image.max' => 'Image size cannot be more than 2MB.'
		// ];
	// }
}
