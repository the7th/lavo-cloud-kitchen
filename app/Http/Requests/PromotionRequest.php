<?php

namespace App\Http\Requests;

use App\Promotion;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$promotion = Promotion::find($this->id);
		
        return [
            'title' => ['required'],
            'type' => ['required'],
            // 'ref_id' => ['required'],
            // 'rate_type' => ['required'],
            // 'rate_value' => ['required','numeric'],
            'start_date' => 'required|date',
			'end_date' => 'date|after:start_date',
			'status' => ['required', 'string', 'max:25'],
        ];
    }
}
