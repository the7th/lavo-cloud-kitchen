<?php

namespace App\Http\Requests;

use App\KitchenArea;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class KitchenAreaRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$kitchen_area = KitchenArea::find($this->id);
		
        return [
            'name' => ['required', 'string', 'max:255'],
			'address' => ['required', 'string', 'max:255'],
			'state' => ['required', 'string', 'max:255'],
			'city' => ['required', 'string', 'max:255'],
			'long' => ['required', 'numeric'],
			'lat' => ['required', 'numeric'],
			'postal_code' => ['required', 'numeric'],
			'status' => ['required', 'string', 'max:25'],
        ];
    }
}
