<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ProfileRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$user = Auth::user();
		
        return [
			'brand_name' =>  $user->type == 'chef' ? ['required', 'string', 'max:255', Rule::unique((new User)->getTable())->ignore($user->id)] : '',
            'name' => ['required', 'string', 'max:255'],
            'contact_number' => ['required', 'string', 'max:25', Rule::unique((new User)->getTable())->ignore($user->id)],
			'email' => ['required', 'string', 'email', 'max:255', Rule::unique((new User)->getTable())->ignore($user->id)],
			'position' =>  $user->type == 'chef' ? ['required', 'string', 'max:255'] : '',
			'base_type' => $user->type == 'chef' ? ['required', 'string', 'max:255'] : '',
			'password' => $user ? ['nullable', 'string', 'min:8'] : ['required', 'string', 'min:8']
        ];
    }
	
	// public function messages()
	// {
		// return [
			// 'image.max' => 'Image size cannot be more than 2MB.'
		// ];
	// }
}
