<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ConsumerRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$consumer = User::find($this->id);
		
        return [
            'name' => ['required', 'string', 'max:255'],
            // 'employee_id' => ['required', 'string', 'max:255', Rule::unique((new User)->getTable())->ignore($this->id)],
			// 'department' => ['required', 'string', 'max:255'],
			// 'shift' => ['required', 'string', 'max:255'],
			'nric' => ['required','digits:12', Rule::unique((new User)->getTable())->ignore($this->id)],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique((new User)->getTable())->ignore($this->id)],
			'password' => $consumer ? ['nullable', 'string', 'min:8'] : ['required', 'string', 'min:8'],
			'status' => ['required', 'string', 'max:25'],
        ];
    }
	
	// public function messages()
	// {
		// return [
			// 'image.max' => 'Image size cannot be more than 2MB.'
		// ];
	// }
}
