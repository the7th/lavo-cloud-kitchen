<?php

namespace App\Http\Requests;

use App\NotificationData;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$notification = NotificationData::find($this->id);
		
        return [
            'title' => ['required', 'string', 'max:255'],
            'message' => ['required', 'string', 'max:255'],
			// 'status' => ['required', 'string', 'max:25'],
        ];
    }
	
	// public function messages()
	// {
		// return [
			// 'image.max' => 'Image size cannot be more than 2MB.'
		// ];
	// }
}
