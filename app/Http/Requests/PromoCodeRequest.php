<?php

namespace App\Http\Requests;

use App\PromoCode;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PromoCodeRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$promo_code = PromoCode::find($this->id);
		
        return [
            'code' => ['required', 'string', 'max:255', Rule::unique((new PromoCode)->getTable())->ignore($this->id)],
            'redemption_amount' => ['required'],
            'discount_type' => ['required'],
            'discount_value' => ['required','numeric'],
            'start_date' => 'required|date',
			'end_date' => 'date|after:start_date',
			'status' => ['required', 'string', 'max:25'],
        ];
    }
}
