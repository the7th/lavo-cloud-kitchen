<?php

namespace App\Http\Requests;

use App\BannerImage;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class BannerImageRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$banner_image = BannerImage::find($this->id);
		
        return [
            'start_date' => ['required'],
            'end_date' => ['required'],
			'image' => $banner_image ? ['image','mimes:jpeg,png,jpg,gif,svg'] : ['required','image','mimes:jpeg,png,jpg,gif,svg'],
			'status' => ['required', 'string', 'max:25'],
        ];
    }
}
