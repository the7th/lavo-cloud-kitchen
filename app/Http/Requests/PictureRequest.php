<?php

namespace App\Http\Requests;

use App\Picture;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PictureRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$picture = Picture::find($this->id);
		
        return [
            'start_date' => ['required'],
            'end_date' => ['required'],
			'image' => $picture ? ['image','mimes:jpeg,png,jpg,gif,svg'] : ['required','image','mimes:jpeg,png,jpg,gif,svg'],
			'status' => ['required', 'string', 'max:25'],
        ];
    }
}
