<?php

namespace App\Http\Requests;

use App\CommissionCategory;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CommissionCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
		$commission_category = CommissionCategory::find($this->id);
		
        return [
            'name' => ['required', 'string', 'max:255', Rule::unique((new CommissionCategory)->getTable())->ignore($this->id)],
            'type' => ['required'],
            'value' => ['required','numeric'],
			'status' => ['required', 'string', 'max:25'],
        ];
    }
}
