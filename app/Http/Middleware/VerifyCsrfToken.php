<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
		'/set-order',
        '/payment-redirect',
        '/payment-callback',
        '/preauth-card-start',
        '/preauth-payment-callback',
        '/preauth-payment-redirect',
        '/redirecting'
    ];
}
