<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Promotion;
use App\ChefCompany;
use Carbon\Carbon as Carbon;

class Menu extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'menu_category_id', 'user_id', 'name', 'description', 'price', 'meal_settings', 'settings', 'status', 'approval_status'
    ];

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
	
	public function menuCategory()
    {
        return $this->belongsTo('App\MenuCategory', 'menu_category_id', 'id');
    }
	
	public function menuSettings()
    {
        return $this->hasMany('App\MenuSetting', 'menu_id', 'id');
    }

	public function mainImage()
    {
        return $this->hasMany('App\MenuImage', 'menu_id', 'id')->first();
    }
	
	public function images()
    {
        return $this->hasMany('App\MenuImage', 'menu_id', 'id');
    }

	public function approvedReviews()
    {
        return $this->hasMany('App\Review', 'ref_id', 'id')->where('ref_type','menu')->where('approval_status','approved');
    }

	public function orders()
    {
        return $this->hasMany('App\OrderDetail', 'ref_id', 'id')->where('ref_type','menu');
    }

	public function scopeOwnedBy($query, $user)
    {
        if ($user->isAdmin()) {
			return $query;
        }
		else
		{
			return $query->where("user_id", $user->id);
		}

    }

	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . $keyword . "%";

			return $query->whereRaw("menus.name LIKE ?", [$key])
							->orWhereHas('user', function($q) use ($key){
								return $q->whereRaw("users.name LIKE ?", [$key]);
							});
        }
    }

	public function scopeCuisine($query, $menu_category_id = null)
    {
        if ($menu_category_id) {
			$exploded_menu_category_id = explode(',',$menu_category_id);
			return $query->whereIn("menu_category_id", $exploded_menu_category_id);
        }
    }
	
	public function scopePromotion($query, $promotion_id = null)
    {
        if ($promotion_id) {
			$promotion = Promotion::where('id', $promotion_id)->first();
			$dt = Carbon::now()->format('Y-m-d');
			
			if($promotion->type == 'free_delivery')
			{
				if ($dt >= $promotion->start_date && $dt <= $promotion->end_date)
				{
					return $query;
				}
			}
			elseif($promotion->type == 'cuisine')
			{
				if ($dt >= $promotion->start_date && $dt <= $promotion->end_date)
				{
					return $query->where('menu_category_id',$promotion->ref_id);
				}
			}
			elseif($promotion->type == 'brand')
			{
				if ($dt >= $promotion->start_date && $dt <= $promotion->end_date)
				{
					$chef_company = ChefCompany::find($promotion->ref_id);
					return $query->where('user_id',$chef_company->user->id);
				}
			}
			else
			{
				return $query;
			}
        }
		return $query;
    }

	// public function scopeStatus($query, $status)
    // {
        // if ($status) {
			// return $query->where("status", $status);
        // }

    // }

	// public function scopeType($query, $type)
    // {
        // if ($type) {
			// return $query->where("type", $type);
        // }

    // }

	// public function scopeCreatedDate($query, $date)
    // {
        // if ($date) {
			// return $query->whereDate('created_at',$date);
        // }
    // }
}
