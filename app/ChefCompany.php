<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon as Carbon;

class ChefCompany extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id', 'company_name', 'company_registration_number', 'delivery_time_id'
    ];
	
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
	
	public function activePromotions()
    {
		
        return $this->hasMany('App\Promotion', 'ref_id', 'id')
					->where('ref_type','chef_company')
					->where('start_date', '<=', Carbon::now())
					->where('end_date', '>=', Carbon::now())
					->where('status','active')
					->select('title','type','ref_type','ref_id','rate_type','rate_value','start_date','end_date');
    }
}
