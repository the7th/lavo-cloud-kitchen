<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KitchenArea extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
		'name', 'address', 'state', 'city', 'postal_code', 'long', 'lat', 'status'
    ];
	
	public function kitchens()
    {
        return $this->hasMany('App\Kitchen', 'kitchen_area_id', 'id');
    }
	
	public function bookings()
    {
        return $this->hasMany('App\Booking', 'kitchen_area_id', 'id');
    }
}
