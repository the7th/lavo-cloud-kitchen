<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'path', 'filename', 'start_date', 'end_date', 'status'
    ];
	
	public function scopeDateRange($query, $date = null)
    {
        return $query->where(function ($q) use ($date) {
            if ($date) {
                $q->whereRaw('"'.$date.'" between `start_date` and `end_date`');
            }
        });
    }
}
