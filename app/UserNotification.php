<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotification extends Model
{
    use SoftDeletes;

	// protected $table = 'notification_data';
    
    protected $fillable = [
		'user_id', 'notification_id', 'status'
    ];
	
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
