<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon as Carbon;

class MenuSetting extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'menu_id', 'name', 'description', 'price', 'image_path', 'image_filename'
    ];

	public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id', 'id');
    }
	
	public function menuSettingItems()
    {
        return $this->hasMany('App\MenuSettingItem', 'menu_setting_id', 'id');
    }
}
