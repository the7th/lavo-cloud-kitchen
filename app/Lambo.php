<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Symfony\Component\VarDumper\Cloner\Data;

class Lambo extends Model
{
    protected $table = 'lambo';
    public $jobType = 'SCHEDULE';
    public $deliveryType = 'FOOD';
    public $vehicleType = 'MOTORBIKE';


    public static function getToken($state = '')
    {
        $lambo = Lambo::find(1);
        $token = $lambo->token;

        if ($state == 'refresh') {
            $response = Http::withHeaders([
                'apiKey' => $_ENV['LAMBO_API_KEY'],
                'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
            ])
                ->withBody(
                    json_encode(["password" => $_ENV['LAMBO_PASSWORD']]),
                    'application/json'
                )
                ->post($_ENV['LAMBO_API_URL'].'api/MerchantLoginAPI');

            $data = json_decode($response->getBody());
            $token = $data->token;

            $lambo->token = $token;
            $lambo->token_expiry = $data->tokenExpiryUTC;
            $lambo->save();
        }

        return $token;
    }

    public function MerchantJobRequestAPI($payload)
    {
        $lambo = Lambo::find(1);
        $token = $lambo->token;

        $payload['jobType']= $this->jobType;
        $payload['vehicleType']= $this->vehicleType;
        $payload['deliveryType']= $this->deliveryType;

        $endpoint = $_ENV['LAMBO_API_URL'].'api/MerchantJobRequestAPI';
        $response = Http::withHeaders([
            'apiKey' => $_ENV['LAMBO_API_KEY'], 
            'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
        ])
            ->withToken($token)
            ->withBody(
                json_encode($payload),
                'application/json'
            )
            ->post($endpoint);
        if ($response->getStatusCode() == 401) {
            $token = Lambo::getToken('refresh');
            $response = Http::withHeaders([
                'apiKey' => $_ENV['LAMBO_API_KEY'], 
                'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
            ])
                ->withToken($token)
                ->withBody(
                    json_encode($payload),
                    'application/json'
                )
                ->post($endpoint);
        }

        //$response->throw();
        //echo $response->getReasonPhrase();  

        $data = json_decode($response->getBody());
        return $data;
    }

    public static function MerchantCheckJobStatusAPI($trx_id)
    {
        $lambo = Lambo::find(1);
        $token = $lambo->token;

        $endpoint = $_ENV['LAMBO_API_URL'].'api/MerchantCheckJobStatusAPI';

        $payload['merchantTransactionId'] = $trx_id;

        $response = Http::withHeaders([
            'apiKey' => $_ENV['LAMBO_API_KEY'], 
            'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
        ])
            ->withToken($token)
            ->withBody(
                json_encode($payload),
                'application/json'
            )
            ->post($endpoint);
        if ($response->getStatusCode() == 401) {
            $token = Lambo::getToken('refresh');
            $response = Http::withHeaders([
                'apiKey' => $_ENV['LAMBO_API_KEY'], 
                'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
            ])
                ->withToken($token)
                ->withBody(
                    json_encode($payload),
                    'application/json'
                )
                ->post($endpoint);
        }

        //$response->throw();
        //echo $response->getReasonPhrase();  

        $data = json_decode($response->getBody());
        return $data;
    }

    public static function MerchantJobCancelAPI($trx_id)
    {
        $lambo = Lambo::find(1);
        $token = $lambo->token;

        $endpoint = $_ENV['LAMBO_API_URL'].'/api/MerchantJobCancelAPI';

        $payload['merchantTransactionId'] = $trx_id;

        $response = Http::withHeaders([
            'apiKey' => $_ENV['LAMBO_API_KEY'], 
            'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
        ])
            ->withToken($token)
            ->withBody(
                json_encode($payload),
                'application/json'
            )
            ->post($endpoint);
        if ($response->getStatusCode() == 401) {
            $token = Lambo::getToken('refresh');
            $response = Http::withHeaders([
                'apiKey' => $_ENV['LAMBO_API_KEY'], 
                'merchantCode' => $_ENV['LAMBO_MERCHANT_ID']
            ])
                ->withToken($token)
                ->withBody(
                    json_encode($payload),
                    'application/json'
                )
                ->post($endpoint);
        }

        //$response->throw();
        //echo $response->getReasonPhrase();  

        $data = json_decode($response->getBody());
        return $data;
    }
}
