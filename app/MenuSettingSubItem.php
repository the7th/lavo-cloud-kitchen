<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon as Carbon;

class MenuSettingSubItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'menu_setting_item_id', 'name', 'price'
    ];

	public function menuSettingItem()
    {
        return $this->belongsTo('App\MenuSettingItem', 'menu_setting_item_id', 'id');
    }
	
	public function menuSettingSecondarySubItems()
    {
        return $this->hasMany('App\MenuSettingSecondarySubItem', 'menu_setting_sub_item_id', 'id');
    }
}
