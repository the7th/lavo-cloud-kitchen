<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon as Carbon;

class MenuSettingItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'menu_setting_id', 'name', 'price'
    ];

	public function menuSetting()
    {
        return $this->belongsTo('App\MenuSetting', 'menu_setting_id', 'id');
    }
	
	public function menuSettingSubItems()
    {
        return $this->hasMany('App\MenuSettingSubItem', 'menu_setting_item_id', 'id');
    }
}
