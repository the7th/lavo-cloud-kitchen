<?php
  
namespace App\Exports;
  
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromQuery, WithHeadings, WithMapping, WithEvents
{
	use Exportable;

    public function __construct($request, $type)
    {
        $this->request = $request;
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
				$event->sheet->freezePane('B3');
                $event->sheet->getDelegate()->mergeCells('A1:F1');
            }
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
		$request = $this->request;
		
		if($this->type == 'chef')
		{
			$query = User::query()
							->where('type', $this->type)
							->keyword($request->keyword)
							->leftJoin('chef_companies', 'chef_companies.user_id', '=', 'users.id')
							->select(
								'users.id AS chef_id',
								'users.name AS chef_name',
								'users.email AS chef_email',
								'users.contact_number AS chef_contact',
								'chef_companies.company_name AS company'
							);
		}
		elseif($this->type == 'consumer')
		{
			$query = User::query()
							->where('type', $this->type)
							->keyword($request->keyword)
							->select(
								'users.id AS consumer_id',
								'users.name AS consumer_name',
								'users.gender AS consumer_gender',
								'users.email AS consumer_email',
								'users.contact_number AS consumer_contact'
							);
		}

        return $query;
    }

    public function headings(): array
    {
		if($this->type == 'chef')
		{
			return [
				[ucfirst($this->type)." List"],
				['Chef ID', 'Chef Name', 'Chef Email', 'Chef Phone', 'Company']
			];
		}
		elseif($this->type == 'consumer')
		{
			return [
				[ucfirst($this->type)." List"],
				['Consumer ID', 'Consumer Name', 'Consumer Email', 'Consumer Phone', 'Gender']
			];
		}
    }

    /**
     * @var UserPoint $user
     */
    public function map($user): array
    {
		if($this->type == 'chef')
		{
			return [
				$user->chef_id,
				$user->chef_name,
				$user->chef_email,
				$user->chef_contact,
				$user->company
			];
		}
		elseif($this->type == 'consumer')
		{
			return [
				$user->consumer_id,
				$user->consumer_name,
				$user->consumer_gender,
				$user->consumer_email,
				$user->consumer_contact
			];
		}
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return ucfirst($this->type)." List";
    }
}