<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id', 'ref_type', 'ref_id', 'rate', 'text', 'approval_status'
    ];

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

	public function chef()
    {
        return $this->belongsTo('App\User', 'ref_id', 'id')->where('type','chef');
    }

	public function menu()
    {
        return $this->belongsTo('App\Menu', 'ref_id', 'id');
    }

	// public function category()
    // {
        // return $this->belongsTo('App\FeedbackCategory', 'category_id', 'id');
    // }

	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . strtolower($keyword) . "%";
			return $query->whereHas('user', function($q) use ($key){
								return $q->whereRaw("LOWER(users.name) LIKE ?", [$key])->where('type','user');
							})
							->orWhereHas('menu', function($q) use ($key){
								return $q->whereRaw("LOWER(menues.name) LIKE ?", [$key]);
							})
							->orWhereHas('chef', function($q) use ($key){
								return $q->whereRaw("LOWER(users.name) LIKE ?", [$key])->where('type','chef');
							});
        }

    }

	public function scopeDateRange($query, $from = null, $to = null)
    {
        return $query->where(function ($q) use ($from, $to) {
            if ($from) {
                $q->whereDate('created_at', '>=', $from);
            }
            if ($to) {
                $q->whereDate('created_at', '<=', $to);
            }
        });
    }

	public function scopeType($query, $type)
    {
        if ($type) {
			return $query->where('ref_type', $type);
        }
    }
}
