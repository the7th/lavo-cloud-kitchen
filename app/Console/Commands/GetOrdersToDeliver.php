<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Jobs\sendLamboMerchantJobRequest;
use App\Order;
use App\OrderDetail;
use App\User;
use App\Lambo;
use App\LamboTrail;

class GetOrdersToDeliver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getorderstodeliver';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the list of order that is ready to set delivery job to Lambo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::where('order_status', 'pickup')->doesnthave('lamboTrails')->get();
        echo "Found: " . count($orders) . " \n";

        foreach ($orders as $order) {
            sendLamboMerchantJobRequest::dispatch($order);
        }
        return 0;
    }
}
