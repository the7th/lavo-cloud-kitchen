<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Lambo;
use App\LamboTrail;

class CheckLamboStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CheckLamboStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check the delivery status from Lambo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::whereIn('order_status', ['pickup', 'delivering'])->has('lamboTrails')->get();
        echo "Found: " . count($orders) . " \n";

        foreach ($orders as $order) {
            echo $order->merchant_txn_id;
            echo "::";
            $lambo = new Lambo;
            $status = $lambo->MerchantCheckJobStatusAPI($order->merchant_txn_id);
            $latestStatus = $order->lamboTrails()->orderBy('created_at', 'DESC')->first();

            if ($status->StatusCode == 1) {
                if ($status->jobStatus !== $latestStatus->jobstatus) {
                    $lambotrail = new LamboTrail;
                    $lambotrail->merchant_txn_id = $status->merchantTransactionId;
                    $lambotrail->jobstatus = $status->jobStatus;
                    $lambotrail->jobdescription = $status->jobStatusDescr;
                    $lambotrail->jobstatustime = $status->jobStatusTime;
                    $lambotrail->save();


                    if ($status->jobStatus == 'P') {
                        $order->order_status = 'delivering';
                        $order->save();
                    } elseif ($status->jobStatus == 'F') {
                        $order->order_status = 'completed';
                        $order->save();
                    }

                    echo "UPDATED\n";
                } else {
                    echo "NOT UPDATED\n";
                }
            } else {
                $lambotrail = new LamboTrail;
                $lambotrail->merchant_txn_id = $order->merchant_txn_id;
                $lambotrail->jobdescription = "[$status->StatusCode]" . "[$status->SubStatusCode] " . $status->ErrorMessage;
                $lambotrail->save();

                $order->order_status = 'dlvr_req_err';
                $order->save();

                echo "NOT UPDATED:: "."[$status->StatusCode]" . "[$status->SubStatusCode] " . $status->ErrorMessage."\n";
            }
        }
        return true;
    }
}
