<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon as Carbon;

class MenuSettingSecondarySubItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'menu_setting_sub_item_id', 'name', 'price'
    ];

	public function menuSettingSubItem()
    {
        return $this->belongsTo('App\MenuSettingSubItem', 'menu_setting_sub_item_id', 'id');
    }
}
