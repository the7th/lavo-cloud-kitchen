<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'order_id', 'ref_type', 'ref_id', 'quantity', 'price', 'data', 'remarks', 'order_status'
    ];
	
	public function order()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }
	
	public function menu()
    {
        return $this->belongsTo('App\Menu', 'ref_id', 'id');
    }

	public function chef()
    {
        return $this->belongsTo('App\User', 'chef_id', 'id');
    }

	public function getData($value)
	{
		return json_decode($value, true);
	}
	
	public function scopeDateRange($query, $from = null, $to = null)
    {
        return $query->where(function ($q) use ($from, $to) {
            if ($from) {
                $q->whereDate('created_at', '>=', $from);
            }
            if ($to) {
                $q->whereDate('created_at', '<=', $to);
            }
        });
    }
}
