<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryTime extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'title', 'monday_enabled', 'monday_times', 'monday_enabled', 'tuesday_times', 'tuesday_enabled', 'wednesday_times', 'wednesday_enabled', 'thursday_times', 'thursday_enabled', 'friday_times', 'friday_enabled', 'saturday_times', 'saturday_enabled', 'sunday_times', 'sunday_enabled', 'status'
    ];
	

	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . $keyword . "%";

			return $query->whereRaw("delivery_times.title LIKE ?", [$key]);
        }
    }
}
