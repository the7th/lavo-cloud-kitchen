<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Order;
use App\Lambo;
use App\LamboTrail;
use App\KitchenArea;

class sendLamboMerchantJobRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $kitchen = KitchenArea::find(1);

        $detail = array(
            'merchantTransactionId' => $this->order->merchant_txn_id, //[M]
            'merchantRefNo' => '',
            'jobType' => '', //[M]
            'jobScheduledTime' => $this->order->delivery_date . ' 12:00:00', //[M]
            'deliveryType' => '', //[M]
            'vehicleType' => '', //[M]
            'pickupName' => substr($this->order->orderDetail->chef->name, 0, 100), //[M]
            'pickupMobile' => $this->order->orderDetail->chef->contact_number, //[M]
            'pickupEmail' => $this->order->orderDetail->chef->email,
            'pickupAddress' => $kitchen->address .", ". $kitchen->postal_code .", ".  $kitchen->city .", ".  $kitchen->state .", Malaysia" , //[M]
            'pickupAddr1' => $kitchen->address,
            'pickupAddr2' => '',
            'pickupPostCode' => $kitchen->postal_code, //[M]
            'pickupCountry' => 'Malaysia',
            'pickupState' => $kitchen->state,
            'pickupCity' => $kitchen->city,
            'pickupNoteToRider' => '',
            'pickupAdditionalAddressInfo1' => '',
            'dropOffList' => 
            array(  
                array(
                'seq' => 1, //[M]
                'merchantDropOffRefNo' => $this->order->merchant_txn_id, //[M]
                'recipientName' => $this->order->user->name, //[M]
                'recipientMobile' => $this->order->user->contact_number, //[M]
                'recipientEmail' => $this->order->user->email,
                'dropOffAddress' => $this->order->address .", ". $this->order->postal_code .", ". $this->order->city .", ". $this->order->state .", Malaysia" , //[M]
                'dropOffAddr1' => $this->order->address,
                'dropOffAddr2' => '',
                'dropOffPostCode' => $this->order->postal_code, //[M]
                'dropOffCountry' => 'Malaysia',
                'dropOffState' => $this->order->state,
                'dropOffCity' => $this->order->city,
                'dropOffNoteToRider' => '',
                'dropOffAdditionalAddressInfo1' => '',
                'itemDescription' => 'order delivery for order #'.$this->order->id, //[M]
                'itemCount' => $this->order->orderDetail->quantity //[M]
                )
            ),
        );

        $lambo = new Lambo;
        $job = $lambo->MerchantJobRequestAPI($detail);

        if($job->StatusCode == 1){
            $lambotrail = new LamboTrail;
            $lambotrail->merchant_txn_id = $job->merchantTransactionId;
            $lambotrail->jobstatus = $job->jobStatus;
            $lambotrail->jobdescription = $job->jobStatusDescr;
            $lambotrail->jobstatustime = $job->jobStatusTime;
            $lambotrail->save();
        }else{
            $lambotrail = new LamboTrail;
            $lambotrail->merchant_txn_id = $this->order->merchant_txn_id;
            $lambotrail->jobdescription = "[$job->StatusCode]" . "[$job->SubStatusCode] " . $job->ErrorMessage;
            $lambotrail->save();
        }

    }
}
