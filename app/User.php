<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Auth;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'chef_account_type', 'brand_name', 'name', 'gender', 'contact_number', 'email', 'position', 'base_type', 'password', 'status', 'facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function myWallet()
    {
        return $this->hasOne('App\UserWallet', 'user_id', 'id');
    }

    public function cards()
    {
        return $this->hasMany('App\Card', 'user_id', 'id');
    }

	public function dailySelfDeclarations()
    {
        return $this->hasMany('App\DailySelfDeclaration', 'user_id', 'id');
    }

	public function cuisineTypes()
    {
        return $this->hasMany('App\ChefCuisineType', 'user_id', 'id');
    }

	public function chefCompany()
    {
        return $this->hasOne('App\ChefCompany', 'user_id', 'id')->limit(1);
    }

	public function addresses()
    {
        return $this->hasMany('App\UserAddress', 'user_id', 'id');
    }

	public function orders()
    {
        return $this->hasMany('App\OrderDetail', 'chef_id', 'id');
    }
	
	public function wallet()
    {
        return $this->hasOne('App\UserWallet', 'user_id', 'id');
    }

	public function isAdmin() {
	   if(Auth::user()->type == 'admin')
	   {
		   return true;
	   }
	   return false;
	}

	public function userNotifications()
    {
        return $this->hasMany('App\UserNotification', 'user_id', 'id');
    }

	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . strtolower($keyword) . "%";

			return $query->whereRaw("LOWER(name) LIKE ?", [$key])
						->orWhereRaw("LOWER(email) LIKE ?", [$key]);
        }

    }

	public function scopeChef($query)
    {
        return $query->where('type','chef');
    }

    public function scopeContactNumber($query, $contact_number)
    {
        return $query->where('contact_number', $contact_number);
    }

    public function scopeFb($query, $fb_id)
    {
        return $query->where('facebook_id', $fb_id);
    }
}
