<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon as Carbon;

class MenuCategory extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'name', 'status'
    ];


    /**
     * Scope a query to only include active menu category.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', '=', 'active');
    }
	
	public function activePromotions()
    {
		
        return $this->hasMany('App\Promotion', 'ref_id', 'id')
					->where('ref_type','menu_category')
					->where('start_date', '<=', Carbon::now())
					->where('end_date', '>=', Carbon::now())
					->where('status','active')
					->select('title','type','ref_type','ref_id','rate_type','rate_value','start_date','end_date');
    }
}
