<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
   
    protected $fillable = [
		'user_id', 'ref_type', 'ref_id', 'kitchen_area_id', 'booking_type', 'booking_start_date', 'booking_end_date', 'session_timeslots', 'price', 'payment_status'
    ];
	
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
	
	public function kitchen()
    {
        return $this->belongsTo('App\Kitchen', 'ref_id', 'id');
    }
	
	public function kitchenArea()
    {
        return $this->belongsTo('App\KitchenArea', 'kitchen_area_id', 'id');
    }
	
	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . strtolower($keyword) . "%";
			return $query->whereHas('user', function($q) use ($key){
								return $q->whereRaw("LOWER(users.name) LIKE ?", [$key]);
							})
							->orWhereHas('kitchen', function($q) use ($key){
								return $q->whereRaw("LOWER(kitchens.name) LIKE ?", [$key]);
							});
        }
			
    }
	
	public function scopeDateRange($query, $from = null, $to = null)
    {
        return $query->where(function ($q) use ($from, $to) {
            if ($from) {
                $q->whereDate('booking_start_date', '>=', $from);
            }
            if ($to) {
                $q->whereDate('booking_end_date', '<=', $to);
            }
        });
    }
	
	public function scopeType($query, $type)
    {
        if ($type) {
			return $query->where('booking_type', $type);
        }
    }
}
