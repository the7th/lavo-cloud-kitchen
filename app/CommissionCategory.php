<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommissionCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'name', 'type', 'value', 'status'
    ];
	
	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . $keyword . "%";

			return $query->whereRaw("commisson_categories.code LIKE ?", [$key]);
        }
    }
}
