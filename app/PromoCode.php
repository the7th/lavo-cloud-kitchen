<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use SoftDeletes;

    protected $fillable = [
		'code', 'redemption_amount', 'discount_type', 'discount_value', 'start_date', 'end_date', 'status'
    ];

	public function menu()
    {
        return $this->belongsTo('App\Menu', 'ref_id', 'id');
    }
	
	public function chefCompany()
    {
        return $this->belongsTo('App\ChefCompany', 'ref_id', 'id');
    }

	

	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . $keyword . "%";

			return $query->whereRaw("promo_codes.code LIKE ?", [$key]);
        }
    }

	// public function scopeStatus($query, $status)
    // {
        // if ($status) {
			// return $query->where("status", $status);
        // }

    // }

	// public function scopeType($query, $type)
    // {
        // if ($type) {
			// return $query->where("type", $type);
        // }

    // }

	// public function scopeCreatedDate($query, $date)
    // {
        // if ($date) {
			// return $query->whereDate('created_at',$date);
        // }
    // }
}
