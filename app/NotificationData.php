<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationData extends Model
{
    use SoftDeletes;

	protected $table = 'notification_data';
    
    protected $fillable = [
		'title', 'message', 'type', 'status'
    ];
	
	public function scopeKeyword($query, $keyword)
    {
        if ($keyword) {
            $key = "%" . strtolower($keyword) . "%";
			
			return $query->whereRaw("LOWER(notification_data.title) LIKE ?", [$key])
						->orwhereRaw("LOWER(notification_data.message) LIKE ?", [$key]);
        }
			
    }
	
	public function scopeStatus($query, $status)
    {
        if ($status) {
			return $query->where("status", $status);
        }
			
    }
	
	public function scopeType($query, $type)
    {
        if ($type) {
			return $query->where("type", $type);
        }
			
    }
	
	public function scopeCreatedDate($query, $date)
    {
        if ($date) {
			return $query->whereDate('created_at',$date);
        }
    }
}
