import Vue from "vue";
import VCalendar from "v-calendar";
import Entry from "./Entry.vue";
import { store } from "./Store/store";

Vue.use(VCalendar);
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyC9ciir8FyuSNCm7JrCJk5d7xpOoNC2ghk",
        libraries: "places"
    }
});

new Vue({
    el: ".wrapper-content",
    store,
    render: h => h(Entry),
    created : async function() {
        await this.$store.dispatch("loadCredentials");
        await this.$store.dispatch("getOederFromSession");
        await this.$store.dispatch("loadAddresses");
        await this.$store.dispatch('loadCards');
    }
});
