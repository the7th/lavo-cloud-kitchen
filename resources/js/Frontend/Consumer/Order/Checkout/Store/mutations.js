import mainmutations from '../../../../app/store/mutations';

const modulemutations = {
    updateState : (state, payload) => state[payload.key] = payload.value,
    updateStateObject : (state, payload) => state[payload.key] = Object.assign({}, payload.value),
    updateAddresses(state, addresses) {
        state.addresses = addresses
    },
    updateSelectedAddress(state, selectedAddress) {
        state.selectedAddress = selectedAddress
    },
    updateCardsInfo(state, cards) {
        state.cards = cards;
    },
    updateDeliveryDate(state,deliveryDate) {
        state.form.delivery_date = deliveryDate
    },
    updateDeliveryTime(state,deliveryTime) {
        state.form.delivery_time = deliveryTime
    },
    updateSelectedCard(state, selected_card) {
      state.form.selected_card = selected_card
    }
}

export const mutations = {...mainmutations, ...modulemutations}
