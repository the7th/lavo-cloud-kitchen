import axios from "axios";
import mainactions from "../../../../app/store/actions";

const moduleactions = {
    updateState: (context, payload) =>
        context.commit("updateState", {
            key: payload.key,
            value: payload.value
        }),

    updateStateObject: (context, payload) =>
        context.commit("updateStateObject", {
            key: payload.key,
            value: payload.value
        }),

    async loadAddresses({ dispatch, commit, state }) {
        let token = localStorage.getItem('token');

        try {
            //ONLY HERE : GET AND SET CREDENTIAL FROM SERVER
            const response = await axios.get(
                window.public_url + `/api/auth/address-list`,
                {
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json",
                        "X-Requested-With": "XMLHttpRequest"
                    }
                }
            );
            commit("updateAddresses", response.data.data);
        } catch (error) {
            // catches both errors
        }
    },

    async loadCards ({ commit }) {

        let token = window.localStorage.getItem("token")

        return await axios
            .get(window.location.protocol +'//'+window.location.hostname+'/api/consumer/cards', {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                }
            })
            .then((response) => {
                // console.log(response.data.data)
                commit("updateCardsInfo", response.data.data);
            })
            .catch(error => {
                //Fail
                // if(error.response.status === 401){
                //     eventBus.$emit("openExpiredAlertModal");
                // }
            });

    },
};

export const actions = { ...mainactions, ...moduleactions };
