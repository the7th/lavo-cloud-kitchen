import maingetters from '../../../../app/store/getters';

const modulegetters = {
    form : (state) => state.form,
    address : (state) => state.address,
    payment : (state) => state.payment,
    zip : (state) => state.zip,
    addresses : (state) => state.addresses,
    selectedAddress : (state) => state.selectedAddress,
    cards: (state) => state.cards,
    delivery_date: (state) => state.form.delivery_date,
    delivery_time: (state) => state.form.delivery_time,
    selected_card : (state) => state.form.selected_card,
}

export const getters = {...maingetters, ...modulegetters}
