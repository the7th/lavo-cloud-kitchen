import mainstate from '../../../../app/store/state';

const modulestate = {
    form : {
        upload : '',
        name : '',
        email : '',
        birthday : '',
        gender : '',
        delivery_date: '',
        delivery_time: '',
        selected_card: 0,
    },

    address : {
        name: '',
        line_1 : '',
        line_2 : '',
        zip_code : '',
        state: '',
        note: '',
    },

    payment : {
        name: '',
        number: '',
        exp_month: '',
        exp_year: '',
        cvc_code: '',
    },

    zip : {
        zip_code : '',
    },
    addresses : [],
    selectedAddress : 0,
    cards: [],
}

export const state = {...mainstate, ...modulestate}
