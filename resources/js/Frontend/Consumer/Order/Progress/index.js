import Vue from 'vue';

import Entry from './Entry.vue';
import { store } from "./../Checkout/Store/store";

new Vue({
    el : '.wrapper-content',
    store,
    render : h => h(Entry),
    created : async function() {
        await this.$store.dispatch("loadCredentials");
        await this.$store.dispatch("getOederFromSession");
    }
});
