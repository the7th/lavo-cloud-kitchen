import Vue from 'vue';
import { store } from '../../Layouts/Header/Consumer/Store/store.js';
import VueAgile from 'vue-agile'
import Entry from './Entry.vue';
import VueScrollTo from 'vue-scrollto';

Vue.use(VueAgile)

new Vue({
    el : '.wrapper-content',
    store,
    render : h => h(Entry),
    created() {
        this.$store.dispatch('getOederFromSession');
        setTimeout(function () {
            VueScrollTo.scrollTo('#sroll-menu', 500);
        }, 3000);
    },
});
