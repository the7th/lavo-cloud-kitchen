import Vue from 'vue';
import { store } from '../../Layouts/Header/Consumer/Store/store.js';
import VueAgile from 'vue-agile'
import VueGeolocation from "vue-browser-geolocation";
import Entry from './Entry.vue';
import VueMeta from "vue-meta";

Vue.use(VueAgile)
Vue.use(VueGeolocation);
Vue.use(VueMeta);

new Vue({
    el : '.wrapper-content',
    store,
    render : h => h(Entry),
    created() {
        this.$store.dispatch('getOederFromSession');
    }
});
