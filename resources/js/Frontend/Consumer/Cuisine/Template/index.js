import Vue from 'vue';

import Entry from './Entry.vue';

new Vue({
    el : '.wrapper-content',
    render : h => h(Entry)
});
