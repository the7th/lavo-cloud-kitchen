import Vue from 'vue'
window.$ = require('jquery');
import VCalendar from 'v-calendar';
import { store } from './Store/store.js';
import Vuex from 'vuex';
import { mapGetters } from 'vuex'
import VueMeta from 'vue-meta'


import Entry from './Entry.vue';

Vue.use(VCalendar);
Vue.use(Vuex);
Vue.use(VueMeta);


new Vue({
    el : '.wrapper-content',
    store,

    render : h => h(Entry),
    data: () => ({
        addresses: '',
    }),
    // computed: Vuex.mapState(['credentials', 'addresses']),

    mounted() {
        this.addresses = this.$store.getters.addresses;
    },

    async created() {
        await this.$store.dispatch('loadCredentials');
        await this.$store.dispatch('loadAddresses');
        await this.$store.dispatch('loadCards');
    },
})
