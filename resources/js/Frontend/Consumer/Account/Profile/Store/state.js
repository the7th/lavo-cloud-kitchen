
export const state = {

    signup_type : 'default',

    form : {
        upload : '',
        name : '',
        email : '',
        birthdate : '',
        gender : '',
        number: '',
    },

    address : {
        name: '',
        line_1 : '',
        line_2 : '',
        zip_code : '',
        state: '',
        note: '',
    },

    payment : {
        name: '',
        number: '',
        exp_month: '',
        exp_year: '',
        cvc_code: '',
    },

    token: '',

    credentials: [],

    addresses: [],

    cards: [],
}
