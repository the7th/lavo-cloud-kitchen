export const getters = {
    form : (state) => state.form,
    address : (state) => state.address,
    payment : (state) => state.payment,
    addresses: (state) => state.addresses,
    cards: (state) => state.cards,
    signup_type: (state) => state.signup_type,
}
