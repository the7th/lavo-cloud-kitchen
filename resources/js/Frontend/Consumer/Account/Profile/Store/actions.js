const axios = require('axios').default;

import { state } from './state.js';

export const protocol = window.location.protocol;
export const hostname = window.location.hostname;

export const actions = {

    updateState : (context, payload) => context.commit('updateState', {
        key : payload.key,
        value : payload.value
    }),

    updateStateObject : (context, payload) => context.commit('updateStateObject', {
        key : payload.key,
        value : payload.value
    }),

    async loadCredentials ({ commit }) {

        let token = window.localStorage.getItem("token")

        return await axios
            .get(window.location.protocol +'//'+window.location.hostname+'/api/auth/user', {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                }
            })
            .then((response) => {
                commit("updateLoginCredentials", response.data.data);
            })
            .catch(error => {
                //Fail
                if(error.response.status === 401){
                    eventBus.$emit("openExpiredAlertModal");
                }
            });

    },

    async loadAddresses ({ dispatch, commit }) {

         dispatch('loadCredentials').then(() => {

            let token = localStorage.getItem('token');

            axios
                .get(protocol + '//' + hostname +'/api/auth/address-list', {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest',
                    }
                })

                .then((response) => {
                    commit('updateAddresses', response.data.data)
                })
                .catch(error => {
                    //Fail
                });

        })

    },

    async loadCards ({ commit }) {

        let token = window.localStorage.getItem("token")

        return await axios
            .get(window.location.protocol +'//'+window.location.hostname+'/api/consumer/cards', {
                headers: {
                    'Authorization': 'Bearer ' + token,
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                }
            })
            .then((response) => {
                // console.log(response.data.data)
                commit("updateCardsInfo", response.data.data);
            })
            .catch(error => {
                //Fail
                // if(error.response.status === 401){
                //     eventBus.$emit("openExpiredAlertModal");
                // }
            });

    },

}



