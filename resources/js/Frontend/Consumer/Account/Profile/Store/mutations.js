import moment from 'moment'

export const mutations = {
    updateState : (state, payload) => state[payload.key] = payload.value,
    updateStateObject : (state, payload) => state[payload.key] = Object.assign({}, payload.value),

    updateLoginCredentials(state, credentials) {
        state.form.name = credentials.name
        state.form.email = credentials.email
        state.form.number = credentials.contact_number
        state.form.birthdate = moment(String( credentials.birthdate )).format('DD/MM/YYYY')
        state.form.gender = credentials.gender
        state.form.image_filename = credentials.image_path
        state.form.image_path = credentials.image_path
        state.token = credentials.access_token
        state.signup_type = credentials.facebook_id != '' ? 'social' : 'default'
    },

    updateAddresses(state, addresses) {
        state.addresses = addresses
    },

    updateCardsInfo(state, cards) {
        state.cards = cards;
    }


}
