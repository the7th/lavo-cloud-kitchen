import Vue from 'vue'
window.$ = require('jquery');
import VCalendar from 'v-calendar';
import { store } from './Store/store.js';

import Entry from './Entry.vue';

Vue.use(VCalendar);

new Vue({
    el : '.wrapper-content',
    store,
    render : h => h(Entry)
})
