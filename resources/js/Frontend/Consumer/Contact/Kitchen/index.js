import Vue from 'vue'
window.$ = require('jquery')
import { store } from './Store/store.js';

import Entry from './Entry.vue';

new Vue({
    el : '.wrapper-content',
    store,
    render : h => h(Entry)
})
