import Vue from 'vue';
import VueAgile from 'vue-agile'

import Entry from './Entry.vue';

Vue.use(VueAgile)


new Vue({
    el : '.wrapper-content',
    render : h => h(Entry)
});
