import Vue from 'vue';
import Entry from './Entry.vue';

new Vue({
    el : '.wrapper-footer',
    render : h => h(Entry)
});
