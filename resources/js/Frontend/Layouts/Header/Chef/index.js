import Vue from 'vue';
import Entry from './Entry.vue';

new Vue({
    el : '.wrapper-header',
    render : h => h(Entry)
});
