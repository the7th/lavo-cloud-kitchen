import Vue from "vue";
window.$ = require("jquery");
import { store } from "./Store/store.js";
import Entry from "./Entry.vue";

window.eventBus = new Vue();

new Vue({
    el: ".wrapper-header",
    store,
    render: h => h(Entry),
    created() {
        this.$store.dispatch("loadData"); // dispatch credentials
    }
});
