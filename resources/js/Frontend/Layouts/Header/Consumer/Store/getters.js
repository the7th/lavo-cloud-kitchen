export const getters = {
    isMenu : (state) => state.isMenu,
    isNavigation : (state) => state.isNavigation,
    credentials: (state) => state.credentials,
    auth: (state) => state.auth,
    basketState: (state) => state.basketState,
    orderCart: (state) => state.orderCart,
}
