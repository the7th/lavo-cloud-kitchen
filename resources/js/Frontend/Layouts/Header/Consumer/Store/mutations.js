export const mutations = {
    updateState : (state, payload) => state[payload.key] = payload.value,
    updateStateObject : (state, payload) => state[payload.key] = Object.assign({}, payload.value),
    updateLoginCredentials(state, credentials) {

        state.credentials = credentials.data;

        if (credentials.status == "success") {
            state.auth = true
            if (window.localStorage.getItem("token") === null) {

            }
        } else {
            state.auth = false
            window.localStorage.clear()
        }

    },

    updateBasketState : (state, basketState) => {state.basketState = basketState;},

    updateAuth(state, payload) {
        state.auth = payload
    },
    updateOrderCartListFromSession : (state, order) => {state.orderCart.push(order);},
    updateOrderCartList : (state, payload) => {
        let food_infoId = payload.food_info.id;
        let food_infoName = payload.food_info.name;
        let order = payload.order;

        let orderCartFoodInfoid = state.orderCart.findIndex( x => x.id === food_infoId );
        if(orderCartFoodInfoid  === -1){
            payload.food_info.order.push(order);
            state.orderCart.push(payload.food_info);
        }else{
            state.orderCart[orderCartFoodInfoid].order.push(order);
        }

        orderCartFoodInfoid = state.orderCart.findIndex( x => x.id === food_infoId );
    },
    calculatePrice : (state, payload) => {

        if(payload){
            let foodinfoindex = payload.foodinfoindex;
            let orderIndex = payload.orderIndex;
            let updQuantity = parseFloat(payload.quantity);
            state.orderCart[foodinfoindex].order[orderIndex].quantity = updQuantity;
            if(updQuantity == 0){
                state.orderCart[foodinfoindex].order.splice(orderIndex, 1);
                if(state.orderCart[foodinfoindex].order.length == 0){
                    state.orderCart.splice(foodinfoindex, 1);
                }
            }
        }

        for (let foodinfokey in state.orderCart) {
            let foodinfo = state.orderCart[foodinfokey];
            for (let orderkey in foodinfo.order) {
                let order = foodinfo.order[orderkey];
                let menuPrice = parseFloat( order.menu_price );
                let quantity = parseFloat( order.quantity );

                for (let sub_itemkey in order.sub_items) {
                    let sub_item = order.sub_items[sub_itemkey];
                    for (let secondary_sub_itemkey in sub_item.secondary_sub_items) {
                        let secondary_sub_item = sub_item.secondary_sub_items[secondary_sub_itemkey];
                        let secondary_sub_itemPrice = secondary_sub_item.price;
                        menuPrice += secondary_sub_itemPrice;
                    }
                }

                state.orderCart[foodinfokey].order[orderkey].total_price_exc_sst = menuPrice * quantity;

                // console.log(state.orderCart[foodinfokey].order[orderkey].total_price_exc_sst)
            }
        }
    },

    updateVoucher : (state, payload) => {


        // console.log(state.orderCart)


        if (payload.status == "success")
        {

            state.voucher = parseFloat(payload.data.value).toFixed(2);

            // console.log(state.voucher)

            let voucher = state.voucher;

            if (voucher > 0)
            {
                state.total = parseFloat(state.total - voucher).toFixed(2)
            }




        } else {
            state.voucher = 0
        }


    },

    updateTotal : (state, payload) => {
        state.total = parseFloat(payload).toFixed(2);
    }


}
