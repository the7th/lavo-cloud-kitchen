import { state } from "./state";

const axios = require("axios").default;

export const actions = {
    updateState: (context, payload) =>
        context.commit("updateState", {
            key: payload.key,
            value: payload.value
        }),

    updateStateObject: (context, payload) =>
        context.commit("updateStateObject", {
            key: payload.key,
            value: payload.value
        }),

    loadData({ commit }) {

        if (window.localStorage.getItem("token") !== null) {

            let token = window.localStorage.getItem("token")

                axios
                    .get(window.location.protocol +'//'+window.location.hostname+'/api/auth/user', {
                        headers: {
                            'Authorization': 'Bearer ' + token,
                            'Content-Type': 'application/json',
                            'X-Requested-With': 'XMLHttpRequest',
                        }
                    })
                    .then((response) => {
                        commit("updateLoginCredentials", response.data);
                    })




        } else {


            axios.get(window.location.protocol +
                "//" +
                window.location.hostname +
                "/get-token"
            )
                .then(function (response){
                    if (response.data.status === true)
                    {
                        window.localStorage.setItem("token", response.data.token)

                        let token = window.localStorage.getItem("token")

                        axios
                            .get(window.location.protocol +'//'+window.location.hostname+'/api/auth/user', {
                                headers: {
                                    'Authorization': 'Bearer ' + token,
                                    'Content-Type': 'application/json',
                                    'X-Requested-With': 'XMLHttpRequest',
                                }
                            })
                            .then((response) => {
                                commit("updateLoginCredentials", response.data);
                            })



                    }
                });
        }






    },
    calculatePrice: ({ commit, state }, payload) => {
        commit("calculatePrice", payload);

        axios
            .post(window.public_url + `/set-order`, { orders: state.orderCart })
            .then(response => {
                //Success
            })
            .catch(e => {
                //Fail
            });
    },
    async getOederFromSession({ commit }) {
        try {
            //ONLY HERE : GET AND SET CART LIST FROM SERVER SESSION
            const response = await axios.get(window.public_url + `/set-order`);
            console.log( JSON.stringify(response.data) )
            if (response.data.length > 0) {
                for (let key in response.data) {
                    commit("updateOrderCartListFromSession", response.data[key]);
                }
            } else {
                //Cart session is empty
            }
        } catch (error) {
            // catches both errors
        }
    },

    async jomUpdateVoucher({ commit, state }, payload)
    {
        commit('updateVoucher', payload)
    }
};
