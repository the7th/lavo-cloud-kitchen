export default {
    updateState : (state, payload) => state[payload.key] = payload.value,
    updateStateObject : (state, payload) => state[payload.key] = Object.assign({}, payload.value),

    updateLoginCredentials(state, credentials) {
        state.form.name = credentials.name
        state.form.email = credentials.email
        state.form.number = credentials.contact_number
        state.token = credentials.access_token

        state.credentials = credentials.data;

        if (credentials.status == true) {
            state.auth = true
        } else {
            state.auth = false
        }
    },
    updateBasketState : (state, basketState) => {state.basketState = basketState;},

    updateAuth(state, payload) {
        state.auth = payload
    },

    updateOrderCartList : (state, order) => {state.orderCart.push(order);},
    calculatePrice : (state, payload) => {

        if(payload){
            var index = payload.index;
            var updQuantity = parseFloat(payload.quantity);
            state.orderCart[index].quantity = updQuantity;
        }

        var ai = 0, alen = state.orderCart.length;
        while (ai < alen) {
            var setting = state.orderCart[ai].settings;
            var menuPrice = parseFloat( state.orderCart[ai].menu_price );
            var quantity = parseFloat( state.orderCart[ai].quantity );
            var bi = 0, blen = setting.length;
            while (bi < blen) {
                var option = setting[bi].options;
                var ci = 0, clen = option.length;
                while (ci < clen) {
                    var optionPrice = parseFloat(option[ci].price);
                    menuPrice += optionPrice;
                    ci++;
                }
                bi++;
            }

            state.orderCart[ai].total_price_exc_sst = menuPrice * quantity;
            ai++
        }
    },
    generatecheckoutSummary : (state) => {
        let orderCart = { ...state.orderCart };
        let subTotal = 0;

        for (let food_infokey in orderCart) {
            let food_info = orderCart[food_infokey];
            for (let orderkey in food_info.order) {
                let order = food_info.order[orderkey];
                let totalExcSST = parseFloat(order.total_price_exc_sst);
                subTotal += totalExcSST;
            }
        }

        let voucherDiscountAmount = 0;
        let deliveryFee = 4;
        let seviceTax = 6;
        let seviceTaxAmount = (subTotal + deliveryFee) * (seviceTax / 100);
        let totalIncSST =
            subTotal +
            deliveryFee +
            seviceTaxAmount -
            voucherDiscountAmount;

        state.checkOutSummary =  {
            subtotal: subTotal.toFixed(2),
            deliveryfees: deliveryFee.toFixed(2),
            servicetaxamount: seviceTaxAmount.toFixed(2),
            total_price_inc_sst: totalIncSST.toFixed(2)
        };
        
    }

}
