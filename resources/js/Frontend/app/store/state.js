export default {
    isMenu : false,
    isNavigation : false,
    credentials: [],
    auth: false,
    basketState: 'inactive',
    orderCart: [],
    checkOutSummary: null
}
