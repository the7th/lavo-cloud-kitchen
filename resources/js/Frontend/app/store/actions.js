import { state } from "./state";

const axios = require("axios").default;

export default {
    updateState: (context, payload) =>
        context.commit("updateState", {
            key: payload.key,
            value: payload.value
        }),

    updateStateObject: (context, payload) =>
        context.commit("updateStateObject", {
            key: payload.key,
            value: payload.value
        }),

    loadData({ commit }) {
        axios
            .get(
                window.location.protocol +
                    "//" +
                    window.location.hostname +
                    "/seeAuth"
            )
            .then(response => {
                commit("updateLoginCredentials", response.data);
            });
    },
    async loadCredentials({ commit }) {

        let token = window.localStorage.getItem("token");

        try {
            //ONLY HERE : GET AND SET CREDENTIAL FROM SERVER
            const response = await axios.get(
                window.public_url + `/api/auth/user`,
                {
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json",
                        "X-Requested-With": "XMLHttpRequest"
                    }
                }
            );

            let data = response.data;
            commit("updateLoginCredentials", data);
        } catch (error) {
            // catches both errors
            if(error.response.status === 401){
                eventBus.$emit("openExpiredAlertModal");
            }
        }
    },
    calculatePrice: ({ commit, state }, payload) => {
        commit("calculatePrice", payload);
        commit("generatecheckoutSummary");
        axios
            .post(window.public_url + `/set-order`, { orders: state.orderCart })
            .then(response => {
                //Success
            })
            .catch(e => {
                //Fail
            });
    },
    async getOederFromSession({ commit }) {
        try {
            //ONLY HERE : GET AND SET CART LIST FROM SERVER SESSION
            const response = await axios.get(window.public_url + `/set-order`);
            if (response.data.length > 0) {
                for (let key in response.data) {
                    commit("updateOrderCartList", response.data[key]);
                }

                commit("generatecheckoutSummary");
            } else {
                //Cart session is empty
            }
        } catch (error) {
            // catches both errors
        }
    }
};
