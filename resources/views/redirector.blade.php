<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<p>Redirecting...</p>
<script>
    let redirect = window.localStorage.getItem('redirect')
    window.localStorage.removeItem('redirect')

    window.location.href = redirect

</script>
</body>
</html>
