@extends('Frontend/Layouts/layout')

@section('title', 'About Us')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/main.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/about.js') }}" defer></script>
@endsection
