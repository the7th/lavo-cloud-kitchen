@extends('Frontend/Layouts/layout')

@section('title', 'Chef Kitchens')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/chef.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/chef/index.js') }}" defer></script>
@endsection
