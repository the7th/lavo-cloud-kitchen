<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="Lavo Cloud Kitchen : ">

        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" type="image/x-icon" sizes="48x48" href="{{ asset('frontend/img/favicon/favicon.ico') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('frontend/img/favicon/apple-touch-icon.png') }}">
        <title>@yield('title') | Lavo Cloud Kitchen</title>


        <link href="{{ asset('frontend/css/fontawesome.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/global.css') }}" rel="stylesheet">
        @yield('stylesheet')

    </head>
    <body>

        <section class='layout-wrapper'>
            <div class='wrapper-header'></div>
            <div class='wrapper-content'></div>
            <div class='wrapper-footer'></div>
        </section>


    </body>

    <script>
        var public_url = '{{ url('/') }}';

        var menu_routes = {
            'frontend.home' : '{{ route('frontend.home') }}',
            'frontend.register' : '{{ route('frontend.register') }}',
            'frontend.consumer' : '{{ route('frontend.consumer') }}',
            'frontend.consumer.menulisting' : '{{ route('frontend.consumer.menulisting') }}',
            'frontend.consumer.chef' : '{{ route('frontend.consumer.chef') }}',
            'frontend.consumer.term' : '{{ route('frontend.consumer.term') }}',
            'frontend.consumer.privacy' : '{{ route('frontend.consumer.privacy') }}',
            'frontend.consumer.profile' : '{{ route('frontend.consumer.profile') }}',
            'frontend.consumer.gcash' : '{{ route('frontend.consumer.gcash') }}',
            'frontend.consumer.myorder' : '{{ route('frontend.consumer.myorder') }}',
            'frontend.consumer.contact.kitchen' : '{{ route('frontend.consumer.contact.kitchen') }}',
            'frontend.consumer.contact.delivery' : '{{ route('frontend.consumer.contact.delivery') }}',
            'frontend.consumer.promotion.template' : '{{ route('frontend.consumer.promotion.template') }}',
            'frontend.consumer.order.checkout' : '{{ route('frontend.consumer.order.checkout') }}',
            'frontend.consumer.order.failed' : '{{ route('frontend.consumer.order.failed') }}',
            'frontend.consumer.order.progress' : '{{ route('frontend.consumer.order.progress') }}',
            'frontend.consumer.order.review' : '{{ route('frontend.consumer.order.review') }}',
            'frontend.consumer.cuisine.template' : '{{ route('frontend.consumer.cuisine.template') }}',
            'frontend.chef' : '{{ route('frontend.chef') }}',
            'frontend.about' : '{{ route('frontend.about') }}',
            'frontend.event' : '{{ route('frontend.event') }}',
        };

        var current_route = '{{ url()->current() }}';
    </script>
    <script src="{{ asset('frontend/vue/footer.js') }}" defer></script>
    @yield('javascript')
</html>
