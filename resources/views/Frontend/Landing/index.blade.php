@extends('Frontend/Layouts/layout')

@section('title', 'Home')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/main.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/landing.js') }}" defer></script>
@endsection
