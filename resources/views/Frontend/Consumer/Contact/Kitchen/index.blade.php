@extends('Frontend/Layouts/layout')

@section('title', 'Contact Us - kitchen')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/contact/kitchen.js') }}" defer></script>
@endsection
