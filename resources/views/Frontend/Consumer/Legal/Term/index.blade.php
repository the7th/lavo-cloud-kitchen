@extends('Frontend/Layouts/layout')

@section('title', 'Terms & Conditions')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/legal/term.js') }}" defer></script>
@endsection
