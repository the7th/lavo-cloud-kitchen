@extends('Frontend/Layouts/layout')

@section('title', 'Privacy Policy')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/legal/privacy.js') }}" defer></script>
@endsection
