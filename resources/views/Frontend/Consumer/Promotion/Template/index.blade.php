@extends('Frontend/Layouts/layout')

@section('title', 'Template Promotion')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/promotion/template.js') }}" defer></script>
@endsection
