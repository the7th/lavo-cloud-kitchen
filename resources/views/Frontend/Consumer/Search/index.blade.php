@extends('Frontend/Layouts/layout')

@section('title', 'Search Result')

@section('javascript')
    <script>
        var searchStr = '{{ $searchStr }}';
    </script>
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/search.js') }}" defer></script>
@endsection
