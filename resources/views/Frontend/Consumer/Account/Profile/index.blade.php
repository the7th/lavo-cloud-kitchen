@extends('Frontend/Layouts/layout')

@section('title', 'Profile')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/account/profile.js') }}" defer></script>
@endsection
