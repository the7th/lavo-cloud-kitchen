@extends('Frontend/Layouts/layout')

@section('title', 'My G Cash')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/account/gcash.js') }}" defer></script>
@endsection
