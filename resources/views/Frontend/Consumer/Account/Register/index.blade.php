@extends('Frontend/Layouts/layout')

@section('title', 'Register')

@section('javascript')


    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => Auth::user(),
            'api_token' => (Auth::user()) ? Auth::user()->api_token : null
        ]) !!};

    </script>


    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/account/register.js') }}" defer></script>
@endsection
