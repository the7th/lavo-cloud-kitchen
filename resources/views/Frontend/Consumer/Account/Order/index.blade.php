@extends('Frontend/Layouts/layout')

@section('title', 'My Order')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/account/order.js') }}" defer></script>
@endsection
