@extends('Frontend/Layouts/layout')

@section('title', 'Checkout')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/order/checkout.js') }}" defer></script>
@endsection
