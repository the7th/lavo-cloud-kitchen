@extends('Frontend/Layouts/layout')

@section('title', 'In Progress')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/order/progress.js') }}" defer></script>
@endsection
