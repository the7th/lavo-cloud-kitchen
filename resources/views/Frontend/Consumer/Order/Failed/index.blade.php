@extends('Frontend/Layouts/layout')

@section('title', 'Failed Payment')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/order/failed.js') }}" defer></script>
@endsection
