@extends('Frontend/Layouts/layout')

@section('title', 'Order Completed')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/order/review.js') }}" defer></script>
@endsection
