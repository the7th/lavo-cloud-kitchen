@extends('Frontend/Layouts/layout')

@section('title', 'Food Delivery')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/index.js') }}" defer></script>
@endsection
