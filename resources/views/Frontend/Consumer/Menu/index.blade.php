@extends('Frontend/Layouts/layout')

@section('title', 'Menu')

@section('javascript')
    <script>
        var menuId = '{{ $menuId }}';
    </script>
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/menu.js') }}" defer></script>
@endsection
