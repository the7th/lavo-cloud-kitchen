@extends('Frontend/Layouts/layout')

@section('title', 'Menu Listing')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/menulisting.js') }}" defer></script>
@endsection
