@extends('Frontend/Layouts/layout')

@section('title', 'Our Chef')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/chef.js') }}" defer></script>
@endsection
