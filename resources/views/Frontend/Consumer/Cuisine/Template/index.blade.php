@extends('Frontend/Layouts/layout')

@section('title', 'Template Cuisine Details')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/consumer.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/consumer/cuisine/template.js') }}" defer></script>
@endsection
