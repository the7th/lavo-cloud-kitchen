@extends('Frontend/Layouts/layout')

@section('title', 'Event')

@section('javascript')
    <script src = "{{ asset('frontend/vue/header/main.js') }}" defer></script>
    <script src = "{{ asset('frontend/vue/event/index.js') }}" defer></script>
@endsection
