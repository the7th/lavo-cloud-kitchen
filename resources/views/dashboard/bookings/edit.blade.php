@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('bookings.index') }}">Bookings</a></li>
		<li class="breadcrumb-item active">View Order</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>View Order</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('bookings.edit', $data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label>Chef Name:</label>
					<input type="text" class="form-control" value="{{ $data->user->name }}" disabled>
				</div>
				<div class="form-group">
					<label>Booked At:</label>
					<input type="text" class="form-control" value="{{ $data->created_at }}" disabled>
				</div>
				<div class="form-group">
					<label>Booked Kitchen:</label>
					<input type="text" class="form-control" value="{{ $data->kitchen->name }} ({{ $data->kitchenArea->name }})" disabled>
				</div>
				<div class="form-group">
					<label>Booking Price:</label>
					<input type="text" class="form-control" value="RM {{ $data->price }}" disabled>
				</div>
				<div class="form-group{{ $errors->has('images') ? ' has-danger' : '' }}">
					<table class="table table-bordered text-center">
						<tr>
							<th>Booking Period</th>
						</tr>
						<tr>
							<td>
								From {{ $data->booking_start_date }} To {{ $data->booking_end_date }}<br />
								@if( $data->booking_type == 'session' )
								<small>
									<b>Timeslots :</b> <br />
									@foreach(json_decode($data->session_timeslots,true) as $timeslot)
									{{ $timeslot }}<br />
									@endforeach
								</small>
								@else
									<small> 09:00 - 21:00 </small>
								@endif
							</td>
						</tr>
					</table>
				</div>
				
				<div class="form-group">
					<label for="payment_status">Payment Status:</label>
					<select class="form-control" id="payment_status" name="payment_status" required>
						@foreach(config('global.payment_statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('payment_status', $data->payment_status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div> 
@endsection