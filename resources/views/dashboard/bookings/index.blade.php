@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Bookings</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-filter"></i>Filter Bookings</div>
		<div class="card-body bg-light">
			<form class="form-inline" action="{{ route('bookings.index') }}">
				<div class="container-fluid">
					<div class="row m-2">
						<div class="col-md-3">
							<label class="control-label">Keyword:</label>
						</div>
						<div class="col-md-3">
							{!! Form::text('keyword', $request->keyword, array('placeholder' => 'Keyword','class' => 'form-control')) !!}   
						</div>
						<div class="col-md-3">
							<label class="control-label">Booking Type:</label>
						</div>
						<div class="col-md-3">
							<select class="form-control" id="type" name="type">
								@foreach(config('global.kitchen_booking_types') as $key=>$value)
									<option value="{{ $key }}" {{ $key == $request->type ? 'selected':'' }}>{{ $value }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row m-2">
						<div class="col-md-3">
							<label class="control-label">From:</label>
						</div>
						<div class="col-md-3">
							<input type="date" class="form-control" name="from" data-date-format="yyyy-mm-dd"  value="{{ $request->from }}">
						</div>
						<div class="col-md-3">
							<label class="control-label">Until:</label>
						</div>
						<div class="col-md-3">
							<input type="date" class="form-control" name="to" data-date-format="yyyy-mm-dd"  value="{{ $request->to }}">
						</div>
					</div>
					<hr>
					<div class="row m-2">
						<div class="col-md-12 text-right">
							<button type="submit" class="btn btn-primary mb-6">Filter</button>
						</div>
					</div>
				</div> 
			</form>
		</div>
	</div>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Bookings</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		<div class="card-body">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Booked By</th>
						<th>Type</th>
						<th>Booking Period</th>
						<th>Booking Price</th>
						<th>Payment Status</th>
						<th>Created At</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $value)
					<tr>
						<td>{{ $value->id }}</td>
						<td>{{ $value->user->name }}</td>
						<td>{{ $value->kitchen->name }} ({{ $value->kitchenArea->name }})</td>
						<td>
							{{ $value->booking_start_date }} - {{ $value->booking_end_date }}<br />
							<hr>
							@if( $value->booking_type == 'session' )
							<small>
								<b>Timeslots :</b> <br />
								@foreach(json_decode($value->session_timeslots,true) as $timeslot)
								{{ $timeslot }}<br />
								@endforeach
							</small>
							@else
								<small> 09:00 - 21:00 </small>
							@endif
						</td>
						<td>{{ $value->price }}</td>
						<td class="text-{{ $value->payment_status == 'paid' ? 'success':'danger' }}">{{ strtoupper($value->payment_status) }}</td>
						<td>{{ $value->created_at }}</td>
						<td>
							<div class="dropdown">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									Action
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="{{ route('bookings.edit', $value->id) }}">Edit</a>
									<a class="dropdown-item" href="{{ route('bookings.createPDF', $value->id) }}">Receipt</a>
								</div>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		{!!$data->appends(Request::only('keyword','type','from','to'))->links() !!}
	</div>
</div>
                
@endsection