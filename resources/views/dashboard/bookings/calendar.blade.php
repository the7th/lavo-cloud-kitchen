@extends('layouts.dashboard')
<style>
    * {
        box-sizing: border-box;
    }
    
    ul {
        list-style-type: none;
    }
    
    .month {
        padding: 10px 10;
        width: 100%;
        background: #1abc9c;
        text-align: center;
    }
    
    .month ul {
        margin: 0;
        padding: 0;
    }
    
    .month ul li {
        color: white;
        font-size: 20px;
        text-transform: uppercase;
        letter-spacing: 3px;
    }
    
    .month .prev {
        float: left;
        padding-top: 10px;
    }
    
    .month .next {
        float: right;
        padding-top: 10px;
    }
    
    .weekdays {
        margin: 0;
        padding: 10px 0;
        background-color: #ddd;
    }
    
    .weekdays li {
        display: inline-block;
        width: 13.6%;
        color: #666;
        text-align: center;
    }
    
    .days {
        padding: 10px 0;
        background: #eee;
        margin: 0;
    }
    
    .days li {
        list-style-type: none;
        display: inline-block;
        width: 13.6%;
        text-align: center;
        margin-bottom: 5px;
        font-size: 12px;
        color: #777;
    }
    
    .days li .active {
        padding: 5px;
        background: #1abc9c;
        color: white !important
    }
	
	.days li .selected {
        padding: 5px;
        background: #00bcd4;
        color: white !important
    }
	
	.days li .partly_booked {
        padding: 5px;
        background: #ffc107;
        color: white !important
    }
    /* Add media queries for smaller screens */
    
    @media screen and (max-width:720px) {
        .weekdays li,
        .days li {
            width: 13.1%;
        }
    }
    
    @media screen and (max-width: 420px) {
        .weekdays li,
        .days li {
            width: 12.5%;
        }
        .days li .active {
            padding: 2px;
        }
    }
    
    @media screen and (max-width: 290px) {
        .weekdays li,
        .days li {
            width: 12.2%;
        }
    }
</style>
@section('content')
<div class="content">
    <div class="container-fluid">
        <h1>test</h1>
		{{--
        <h4>Service Request (test) - Step 5</h4>
        <h4>Select the date for servicing : </h4>
		--}}
        <hr>

		<a href="#" class="btn btn-warning">Partly Booked</a>
		<a href="#" class="btn btn-info">Selected</a>
      
		<div>
			<div class="month">
				<ul>
					<li class="prev"> <a href="{{ route('fullcalendar.index') }}?year={{ $calendar['year'] }}&month={{ $calendar['month'] }}&to=prev">&#10094;</a></li>
					<li class="next"> <a href="{{ route('fullcalendar.index') }}?year={{ $calendar['year'] }}&month={{ $calendar['month'] }}&to=next">&#10095;</a></li>
					<li>
						{{ $calendar['month'] }}
						<br>
						<span style="font-size:18px">{{ $calendar['year'] }}</span>
					</li>
				</ul>
			</div>

			<ul class="weekdays">
				<li>Mo</li>
				<li>Tu</li>
				<li>We</li>
				<li>Th</li>
				<li>Fr</li>
				<li>Sa</li>
				<li>Su</li>
			</ul>

			<ul class="days">
				@php($counter=1)
				@foreach($weekdays as $day) 
					@if($calendar['days'][0]['name'] == $day)
						@break
					@else
						<li></li>
					@endif 
				@endforeach
				@foreach($calendar['days'] as $day)
					@if( $day['day_date'] == $calendar['current_date'])
						<li><a id="booking_date" href="{{ route('fullcalendar.index') }}?date={{ $calendar['year'].'-'.$calendar['month_no'].'-'.$day['number'] }}&year={{ $calendar['year'] }}&month={{ $calendar['month'] }}&to=none"><span class="active">{{ $day['number'] }}</span></a></li>
					@elseif( request()->date && Carbon\Carbon::parse(request()->date)->format('d') == $day['number'])
						<li><a id="booking_date" href="{{ route('fullcalendar.index') }}?date={{ $calendar['year'].'-'.$calendar['month_no'].'-'.$day['number'] }}&year={{ $calendar['year'] }}&month={{ $calendar['month'] }}&to=none"><span class="selected">{{ $day['number'] }}</span></a></li>
					@elseif($day['has_booking'] == 'yes')
						<li><a id="booking_date" href="{{ route('fullcalendar.index') }}?date={{ $calendar['year'].'-'.$calendar['month_no'].'-'.$day['number'] }}&year={{ $calendar['year'] }}&month={{ $calendar['month'] }}&to=none"><span class="partly_booked">{{ $day['number'] }}</span></a></li>
					@else
						<li><a id="booking_date" href="{{ route('fullcalendar.index') }}?date={{ $calendar['year'].'-'.$calendar['month_no'].'-'.$day['number'] }}&year={{ $calendar['year'] }}&month={{ $calendar['month'] }}&to=none">{{ $day['number'] }}</a></li>
					@endif 
				@endforeach
			</ul>
		</div>
		<hr>
		
		<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Bookings</div>
			@if (session('message'))
				<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
			@endif
			
			@if (count($data) != 0)
			<div class="card-body">
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Booked By</th>
							<th>Booking Type</th>
							<th>Booking Period</th>
							<th>Booking Price</th>
							<th>Payment Status</th>
							<th>Created At</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach($data as $value)
						<tr>
							<td>{{ $value->id }}</td>
							<td>{{ $value->user->name }}</td>
							<td>{{ $value->ref_type }}</td>
							<td>
								{{ $value->booking_start_date }} - {{ $value->booking_end_date }}<br />
								<hr>
								@if( $value->booking_type == 'session' )
								<small>
									<b>Timeslots :</b> <br />
									@foreach(json_decode($value->session_timeslots,true) as $timeslot)
									{{ $timeslot }}<br />
									@endforeach
								</small>
								@else
									<small> 09:00 - 21:00 </small>
								@endif
							</td>
							<td>{{ $value->price }}</td>
							<td class="text-{{ $value->status == 'unpaid' ? 'success':'danger' }}">{{ strtoupper($value->payment_status) }}</td>
							<td>{{ $value->created_at }}</td>
							<td>
								<div class="dropdown">
									<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
										Action
									</button>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="{{ route('bookings.edit', $value->id) }}">Edit</a>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			@else
				<div class="alert {{ Session::get('alert-class') }}">No record.</div>
			@endif
		</div>
    </div>
</div>
@endsection