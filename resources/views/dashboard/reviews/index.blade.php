@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Reviews</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-filter"></i>Filter Reviews</div>
		<div class="card-body bg-light">
			<form class="form-inline" action="{{ route('reviews.index') }}">
				<div class="container-fluid">
					<div class="row m-2">
						<div class="col-md-3">
							<label class="control-label">Keyword:</label>
						</div>
						<div class="col-md-3">
							{!! Form::text('keyword', $request->keyword, array('placeholder' => 'Keyword','class' => 'form-control')) !!}   
						</div>
						<div class="col-md-3">
							<label class="control-label">Reviews On:</label>
						</div>
						<div class="col-md-3">
							<select class="form-control" id="type" name="type">
								@foreach(config('global.review_types') as $key=>$value)
									<option value="{{ $key }}" {{ $key == $request->type ? 'selected':'' }}>{{ $value }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row m-2">
						<div class="col-md-3">
							<label class="control-label">From:</label>
						</div>
						<div class="col-md-3">
							<input type="date" class="form-control" name="from" data-date-format="yyyy-mm-dd"  value="{{ $request->from }}">
						</div>
						<div class="col-md-3">
							<label class="control-label">Until:</label>
						</div>
						<div class="col-md-3">
							<input type="date" class="form-control" name="to" data-date-format="yyyy-mm-dd"  value="{{ $request->to }}">
						</div>
					</div>
					<hr>
					<div class="row m-2">
						<div class="col-md-12 text-right">
							<button type="submit" class="btn btn-primary mb-6">Filter</button>
						</div>
					</div>
				</div> 
			</form>
		</div>
	</div>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Bookings</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		<div class="card-body">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Reviewed By</th>
						<th>Reviewed On</th>
						<th>Rate</th>
						<th>Approval Status</th>
						<th>Created At</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $value)
					<tr>
						<td>{{ $value->id }}</td>
						<td>{{ $value->user->name }}</td>
						<td>
							@if($value->ref_type == 'menu')
								{{ $value->menu->name }} : By {{ $value->menu->user->name }}
							@else
								{{ $value->chef->name }}
							@endif
						</td>
						<td>{{ $value->rate }}</td>
						@if($value->approval_status == 'pending')
						<td class="text-warning">{{ strtoupper($value->approval_status) }}</td>	
						@elseif($value->approval_status == 'approved')
						<td class="text-success">{{ strtoupper($value->approval_status) }}</td>	
						@else
						<td class="text-danger">{{ strtoupper($value->approval_status) }}</td>
						@endif
						<td>{{ $value->created_at }}</td>
						<td>
							<div class="dropdown">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									Action
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="{{ route('reviews.edit', $value->id) }}">View</a>
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#approvalModal_{{ $value->id }}">Approval</a>
								</div>
							</div>
						</td>
					</tr>
					<div class="modal fade" id="approvalModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">Approve or Reject Record?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">
								Current Status : {{ strtoupper($value->approval_status) }}<hr><br />
								Select "Approve" below if you are ready to approve the record.<br />
								Select "Reject" below if you are ready to reject the record.
							</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<form id="delete-form" action="{{ route('reviews.approval',$value->id) }}" method="POST" style="display: none;">
									@csrf
									<button type="submit" class="btn btn-success" name="approval" value="approved">Approve</button>
									<button type="submit" class="btn btn-danger" name="approval" value="rejected">Reject</button>
								</form>
							</div>
						  </div>
						</div>
					</div>
				@endforeach
				</tbody>
			</table>
		</div>
		{!!$data->appends(Request::only('keyword','type','from','to'))->links() !!}
	</div>
</div>
                
@endsection