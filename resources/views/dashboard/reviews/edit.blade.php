@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('reviews.index') }}">Reviews</a></li>
		<li class="breadcrumb-item active">View Review</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>View Review</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('reviews.edit', $data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label>Reviewed By:</label>
					<input type="text" class="form-control" value="{{ $data->user->name }}" disabled>
				</div>
				<div class="form-group">
					<label>Reviewed On:</label>
					@if($data->ref_type == 'menu')
						<input type="text" class="form-control" value="{{ $data->menu->name }} : By {{ $data->menu->user->name }}" disabled>
					@else
						{{ $value->chef->name }}
					@endif
				</div>
				<div class="form-group">
					<label>Rate:</label>
					<input type="text" class="form-control" value="{{ $data->rate }}" disabled>
				</div>
				<div class="form-group">
					<label>Text:</label>
					<textarea class="form-control" rows="5" disabled>{{ $data->text }}</textarea>
				</div>
				<div class="form-group">
					<label for="approval_status">Approval Status:</label>
					<select class="form-control" id="approval_status" name="approval_status" required>
						@foreach(config('global.approval_statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('approval_status', $data->approval_status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div> 
@endsection