@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('kitchen-areas.index') }}">Kitchen Areas</a></li>
		<li class="breadcrumb-item active">Create kitchen Area</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create kitchen Area</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('kitchen-areas.create') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
				</div>
				
				<div class="form-group">
					<label for="address">Address:</label>
					<textarea class="form-control" rows="5" id="address" name="address" required>{{ old('address') }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="state">State:</label>
					<select class="form-control" id="state" name="state" required>
						@foreach(config('global.states') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('state') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="city">City:</label>
					<input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" required>
				</div>
				
				<div class="form-group">
					<label for="postal_code">Postal Code:</label>
					<input type="text" class="form-control" id="postal_code" name="postal_code" value="{{ old('postal_code') }}" required>
				</div>
				
				<div class="form-group">
					<label for="long">Longitude:</label>
					<input type="text" class="form-control" id="long" name="long" value="{{ old('long') }}" required>
				</div>
				
				<div class="form-group">
					<label for="lat">Latitude:</label>
					<input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat') }}" required>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>   
@endsection