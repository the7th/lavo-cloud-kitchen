@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Kitchens</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Kitchens</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		{{--
		<div class="card-body">
			<form class="form-inline" action="{{ route('kitchens.index') }}">
				{!! Form::text('keyword', $request->keyword, array('placeholder' => 'Keyword','class' => 'form-control')) !!}   
				<button type="submit" class="btn btn-primary mb-6">Submit</button>
			 </form>
		</div>
		--}}
		<div class="card-body">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $value)
					<tr>
						<td>{{ $value->id }}</td>
						<td>{{ $value->name }} ({{ $value->kitchenArea->name }})</td>
						<td class="text-{{ $value->status == 'active' ? 'success':'danger' }}">{{ strtoupper($value->status) }}</td>
						<td>
							<div class="dropdown">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									Action
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="{{ route('kitchens.edit', $value->id) }}">Edit</a>
									{{--
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#deleteModal_{{ $value->id }}">Delete</a>
									--}}
								</div>
							</div>
						</td>
					</tr>
					{{--
					<div class="modal fade" id="deleteModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">Delete Record?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">Select "Delete" below if you are ready to delete the record.</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<form id="delete-form" action="{{ route('kitchens.delete',$value->id) }}" method="POST" style="display: none;">
									@csrf
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</div>
						  </div>
						</div>
					</div>
					--}}
				@endforeach
				</tbody>
			</table>
		</div>
		{!!$data->appends(Request::only('keyword'))->links() !!}
	</div>
</div>
                
@endsection