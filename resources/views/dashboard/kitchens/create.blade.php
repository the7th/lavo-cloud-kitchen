@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('kitchens.index') }}">Kitchens</a></li>
		<li class="breadcrumb-item active">Create kitchen</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create kitchen</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('kitchens.create') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="kitchen_area">Kitchen Area:</label>
					<select class="form-control" id="kitchen_area" name="kitchen_area" required>
						@foreach($kitchen_areas as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('kitchen_area') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
				</div>
				
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea class="form-control" rows="5" id="description" name="description" required>{{ old('description') }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="session_price">Session Price:</label>
					<input type="number" class="form-control" id="session_price" name="session_price" step="0.01" value="{{ old('session_price') }}" required>
				</div>
				
				<div class="form-group">
					<label for="weekly_price">Weekly Price:</label>
					<input type="number" class="form-control" id="weekly_price" name="weekly_price" step="0.01" value="{{ old('weekly_price') }}" required>
				</div>
				
				<div class="form-group">
					<label for="monthly_price">Monthly Price:</label>
					<input type="number" class="form-control" id="monthly_price" name="monthly_price" step="0.01" value="{{ old('monthly_price') }}" required>
				</div>
				<div class="form-group">
					<label for="images">Image (maximum 3):</label>
					<input type="file" class="form-control-file" id="images" name="images[]" multiple>
				</div>
				<div class="form-group">
					<label for="is_halal">Is Halal:</label>
					<select class="form-control" id="is_halal" name="is_halal" required>
						@foreach(config('global.yes_or_no') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('is_halal') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="room_type">Room Type:</label>
					<select class="form-control" id="room_type" name="room_type" required>
						@foreach(['share' => 'Share', 'single' => 'Single'] as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('room_type') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="category">Category:</label>
					<select class="form-control" id="category" name="category" required>
						@foreach(config('global.kitchen_categories') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('category') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="equipment_fixed">Equipment Fixed:</label>
					<select class="form-control" id="equipment_fixed" name="equipment_fixed" required>
						@foreach(config('global.yes_or_no') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('equipment_fixed') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="assign_automatically">Assign Automatically:</label>
					<select class="form-control" id="assign_automatically" name="assign_automatically" required>
						@foreach(config('global.yes_or_no') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('assign_automatically') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div id="equipment_info" class="form-group">
					<label for="description">Equipment Info:</label>
					<textarea class="form-control" rows="5" id="equipment_info" name="equipment_info" required>{{ old('description') }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
 
<script> 
var image_limit = 3;
$('#images').change(function(){
	var files = $(this)[0].files;
	if(files.length > image_limit){
		alert("You can only select max "+image_limit+" images.");
		$('#images').val('');
		return false;
	}else{
		return true;
	}
});  
@endsection