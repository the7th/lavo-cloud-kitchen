@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('orders.index') }}">Orders</a></li>
		<li class="breadcrumb-item active">View Order</li>
	</ol>

	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>View Order</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			@if(Auth::user()->isAdmin())
			<form action="{{ route('orders.edit', $data->id) }}" method="POST" enctype="multipart/form-data">
			@else
			<form action="{{ route('order-details.update') }}" method="POST" enctype="multipart/form-data">
			@endif
				@csrf
				<div class="form-group">
					<label>Customer Name:</label>
					<input type="text" class="form-control" value="{{ $data->user->name }}" disabled>
				</div>
				<div class="form-group">
					<label>Ordered At:</label>
					<input type="text" class="form-control" value="{{ $data->created_at }}" disabled>
				</div>
				<div class="form-group">
					<label>Customer Address:</label>
					<input type="text" class="form-control" value="{{ $data->address }}, {{ $data->postal_code }}, {{ $data->city }}, {{ $data->state }}" disabled>
				</div>
				<div class="form-group">
					<label>Delivery Date & Time:</label>
					<input type="text" class="form-control" value="{{ $data->delivery_date }} {{ $data->delivery_timing }}" disabled>
				</div>
				<div class="form-group">
					<div class="card-header"><i class="fas fa-table mr-1"></i>Delivery Information
						<a class="btn btn-sm" data-toggle="collapse" href="#deliveryinfocollapse" role="button" aria-expanded="false" aria-controls="collapseExample">
							View
						</a>
					</div>
					<div class="collapse" id="deliveryinfocollapse">
						<div class="card card-body">
							<table class="table table-bordered text-center">
								<tr>
									<th>Status Time</th>
									<th>Status</th>
									<th>Status Code</th>
									<th>Rider Name</th>
									<th>Rider Plate Number</th>
									<th>Rider Contact</th>
								</tr>
								@foreach($deliveryInfo as $info)
								<tr>
									<td>
										{{ $info->jobstatustime }}
									</td>
									<td>
										{{ $info->jobdescription }}
									</td>
									<td>
										{{ $info->jobstatus }}
									</td>
									<td>
										{{ $info->ridername }}
									</td>
									<td>
										{{ $info->ridercarplatenumber }}
									</td>
									<td>
										{{ $info->ridercontact }}
									</td>
								</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>
				<div class="form-group{{ $errors->has('images') ? ' has-danger' : '' }}">
					<table class="table table-bordered text-center">
						<tr>
							<th>Order Status</th>
							<th>Name</th>
							<th>Quantity</th>
							<th>Remarks</th>
							<th>Options</th>
							<th>Price</th>
						</tr>
						@php($total = 0.00)
						@php($order_status = '')
						@foreach($items as $item)
							<tr>
								<td>
									{{ ucfirst($item->order_status) }}
								</td>
								<td>
									{{ $item->menu->name }}
								</td>
								<td>
									{{ $item->quantity }}
								</td>
								<td>
									{{ $item->remarks ? $item->remarks : 'N/A'  }}
								</td>
								<td>
									@if($item->data)
									<table class="table table-bordered text-left">
										@if($options[$item->id])
											<tr>
												<th class="bg-light">
												Option:
												</th>
												<td class="bg-light">
												{{ $options[$item->id]['name'] }}
												</td>
											</tr>
											<tr>
												<th class="bg-light">
												Quantity:
												</th>
												<td class="bg-light">
												{{ $options[$item->id]['quantity'] }}
												</td>
											</tr>
											@foreach($options[$item->id]['items'] as $option_setting_item)
												<tr>
													<th>
													+
													</th>
													<td>
													{{ $option_setting_item['name'] }} <br />
													</td>
												</tr>
												@if(isset($option_setting_item['sub_items']))
													@foreach($option_setting_item['sub_items'] as $option_setting_sub_item)
														<tr>
															<th>
															+
															</th>
															<td>
															{{ $option_setting_sub_item['name'] }} <br />
															</td>
														</tr>
														@if(isset($option_setting_sub_item['sec_sub_items']))
															@foreach($option_setting_sub_item['sec_sub_items'] as $option_setting_sec_sub_item)
															<tr>
																<th>
																+
																</th>
																<td>
																{{ $option_setting_sec_sub_item['name'] }} <br />
																</td>
															</tr>
															@endforeach
														@endif
													@endforeach
												@endif
											@endforeach
										@endif
									</table>
									@else
										N/A
									@endif
								</td>
								<td>
									RM{{ $item->price }}
								</td>
							</tr>
							@php($total += $item->price)
							@php($order_status = $item->order_status)
						@endforeach
						<tr>
							<th colspan="5" class="text-right">Total Price excluding Delivery Fee and Service Tax</th>
							<th>RM{{ number_format((float)$total, 2, '.', '') }}</th>
						</tr>
					</table>
				</div>

				@if(Auth::user()->isAdmin())
				<div class="form-group">
					<label for="payment_status">Payment Status:</label>
					<select class="form-control" id="payment_status" name="payment_status" required>
						@foreach(config('global.payment_statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('payment_status', $data->payment_status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="order_status">Order Status:</label>
					<select class="form-control" id="order_status" name="order_status" required>
						@foreach(config('global.order_statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('order_status', $data->order_status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				@else
				<div class="form-group">
					<label for="order_status">Order Status:</label>
					<select class="form-control" id="order_status" name="order_status" required>
						@foreach(config('global.order_statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('order_status', $order_status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				@endif
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
@endsection
