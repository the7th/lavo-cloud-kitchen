@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Orders</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-filter"></i>Filter Orders</div>
		<div class="card-body bg-light">
			<form class="form-inline" action="{{ route('orders.index') }}">
				<div class="container-fluid">
					<div class="row m-2">
						<div class="col-md-3">
							<label class="control-label">Keyword:</label>
						</div>
						<div class="col-md-3">
							{!! Form::text('keyword', $request->keyword, array('placeholder' => 'Keyword','class' => 'form-control')) !!}   
						</div>
					</div>
					<div class="row m-2">
						<div class="col-md-3">
							<label class="control-label">From:</label>
						</div>
						<div class="col-md-3">
							<input type="date" class="form-control" name="from" data-date-format="yyyy-mm-dd"  value="{{ $request->from }}">
						</div>
						<div class="col-md-3">
							<label class="control-label">Until:</label>
						</div>
						<div class="col-md-3">
							<input type="date" class="form-control" name="to" data-date-format="yyyy-mm-dd"  value="{{ $request->to }}">
						</div>
					</div>
					<hr>
					<div class="row m-2">
						<div class="col-md-12 text-right">
							<button type="submit" class="btn btn-primary mb-6">Filter</button>
						</div>
					</div>
				</div> 
			</form>
		</div>
	</div>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Orders</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		<div class="card-body">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Ordered By</th>
						<th>Price</th>
						<th>Payment Status</th>
						<th>Order Status</th>
						<th>Created At</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $value)
					<tr>
						<td>{{ $value->id }}</td>
						<td>{{ $value->user->name }}</td>
						<td>{{ $value->price }}</td>
						<td class="text-{{ $value->payment_status == 'paid' ? 'success':'danger' }}">{{ strtoupper($value->payment_status) }}</td>
						<td class="text-{{ $value->order_status == 'pickup' ? 'success':'warning' }}">{{ strtoupper($value->order_status) }}</td>
						<td>{{ $value->created_at }}</td>
						<td>
							<div class="dropdown">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									Action
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="{{ route('orders.edit', $value->id) }}">View</a>
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#statusModal_{{ $value->id }}">Set Status</a>
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#refundModal_{{ $value->id }}">Refund</a>
								</div>
							</div>
						</td>
					</tr>
					<div class="modal fade" id="statusModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">Update Order?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">
								<form action="{{ route('orders.updateStatus',$value->id) }}" method="POST">
									<div class="form-group">
										<label for="order_status">Order Status:</label>
										<select class="form-control" id="order_status" name="order_status" required>
											@foreach(config('global.order_statuses') as $key=>$arr_value)
												<option value="{{ $key }}" {{ $key == $value->order_status ? 'selected':'' }}>{{ $arr_value }}</option>
											@endforeach
										</select>
									</div>
								
									@csrf
									<button type="submit" class="btn btn-danger">Submit</button>
								</form>
							
							</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							</div>
						  </div>
						</div>
					</div>
					<div class="modal fade" id="refundModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">Refund order?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">Select "Confirm" below if you are ready to refund Order ID: {{ $value->id }} with amount RM{{ $value->price }}.</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<form id="delete-form" action="{{ route('orders.refund',$value->id) }}" method="POST" style="display: none;">
									@csrf
									<button type="submit" class="btn btn-danger">Confirm</button>
								</form>
							</div>
						  </div>
						</div>
					</div>
				@endforeach
				</tbody>
			</table>
		</div>
		{!!$data->appends(Request::only('keyword','type','from','to'))->links() !!}
	</div>
</div>
                
@endsection