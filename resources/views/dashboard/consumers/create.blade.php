@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('non-employees.index') }}">Non-Employees</a></li>
		<li class="breadcrumb-item active">Create Non-Employee</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create Non-Employee</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('non-employees.create') }}" method="POST">
				@csrf
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
				</div>
				<div class="form-group">
					<label for="nric">NRIC:</label>
					<input type="number" class="form-control" id="nric" name="nric" value="{{ old('nric') }}" required>
				</div>
				<div class="form-group">
					<label for="company">Company Name:</label>
					<input type="text" class="form-control" id="company" name="company" value="{{ old('company') }}" required>
				</div>
				<div class="form-group">
					<label for="email">Email address:</label>
					<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
				</div>
				<div class="form-group">
					<label for="password">Password:</label>
					<input type="password" class="form-control" placeholder="Enter password" name="password" id="password" required>
				</div>
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
                
@endsection