@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('consumers.index') }}">Consumers</a></li>
		<li class="breadcrumb-item active">Edit Consumer</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Edit Non-Employee</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('consumers.edit', $data->id) }}" method="POST">
				@csrf
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name', $data->name) }}" required>
				</div>
				
				<div class="form-group">
					<label for="name">Contact Number:</label>
					<input type="number" class="form-control" id="contact_number" name="contact_number" value="{{ old('contact_number', $data->contact_number) }}" required>
				</div>
				
				<div class="form-group">
					<label for="email">Email address:</label>
					<input type="email" class="form-control" id="email" name="email" value="{{ old('email', $data->email) }}" required>
				</div>
				
				<hr>
				
				<div class="card">
					<div class="card-header">Address List</div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th>Address</th>
									<th>State</th>
									<th>City</th>
									<th>Postal Code</th>
								</tr>
							</thead>
							<tbody>
							@foreach($data->addresses as $value)
								<tr>
									<td>{{ $value->address }}</td>
									<td>{{ $value->state }}</td>
									<td>{{ $value->city }}</td>
									<td>{{ $value->postal_code }}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
				
				<hr>
				
				{{--
				<div class="form-group">
					<label for="password">Password:</label>
					<input type="password" class="form-control" placeholder="Enter password" name="password" id="password">
				</div>
				--}}
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status', $data->status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
                
@endsection