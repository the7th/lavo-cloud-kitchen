@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Finance Transactions</li>
        </ol>

        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Finance Transaction</div>
            @if (session('message'))
                <div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
            @endif
            <div class="card-body">


            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name / User ID  </th>
                        <th>Order ID</th>
                        <th>Type</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $txn)
                        <tr>
                        <td>{{ $txn->user->name }} / {{ $txn->user_id }}</td>
                        <td> {{ $txn->order_id }} </td>
                        <td> {{ $txn->type }} </td>
                        <td>RM {{ $txn->amount }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
