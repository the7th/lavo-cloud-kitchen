@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">User Wallet</li>
        </ol>

        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>User Wallet</div>
            @if (session('message'))
                <div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
            @endif
            <div class="card-body">


            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <td>{{$user->user_id}}</td>
                        <td> {{$user->user->name}}  </td>
                        <td>RM {{$user->balance}}</td>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
