@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('commission-categories.index') }}">Commission Categories</a></li>
		<li class="breadcrumb-item active">Edit Commission Categoriy</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Edit Commission Category</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('commission-categories.edit',$data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name',$data->name) }}" required>
				</div>
				
				<div class="form-group">
					<label for="type">Commission Type:</label>
					<select class="form-control" id="type" name="type" required>
						@foreach(config('global.commission_types') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('type', $data->type) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="value">Value:</label>
					<input type="number" class="form-control" id="value" name="value" value="{{ old('value',$data->value) }}" step="0.01" required>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status', $data->status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>         
@endsection