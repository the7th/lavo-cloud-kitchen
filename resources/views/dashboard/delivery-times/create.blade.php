@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('delivery-times.index') }}">Delivery Times</a></li>
		<li class="breadcrumb-item active">Create Delivery Time</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create Delivery Time</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('delivery-times.create') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="title">Title:</label>
					<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" required>
				</div>
				
				<div class="form-group">
					<label for="discount_type">Times:</label>
					<table class="table">
						<thead>
							<tr class="bg-dark text-white">
								<th>Monday</th>
								<th>Tuesday</th>
								<th>Wednesday</th>
								<th>Thursday</th>
								<th>Friday</th>
								<th>Saturday</th>
								<th>Sunday</th>
							</tr>
							<tr class="bg-secondary text-white">
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="monday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="tuesday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="wednesday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="thursday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="friday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="saturday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
								<th>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" name="sunday_enabled" class="form-check-input">Enable
									  </label>
									</div>
								</th>
							</tr>
						</thead>
						<thead>
						<tr class="bg-light">
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="monday_select_all" name="monday_select_all" class="form-check-input">Select All
								  </label>
								</div>
							</th>
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="tuesday_select_all" name="tuesday_enabled" class="form-check-input">Select All
								  </label>
								</div>
							</th>
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="wednesday_select_all" name="wednesday_enabled" class="form-check-input">Select All
								  </label>
								</div>
							</th>
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="thursday_select_all" name="thursday_enabled" class="form-check-input">Select All
								  </label>
								</div>
							</th>
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="friday_select_all" name="friday_enabled" class="form-check-input">Select All
								  </label>
								</div>
							</th>
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="saturday_select_all" name="saturday_enabled" class="form-check-input">Select All
								  </label>
								</div>
							</th>
							<th>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" id="sunday_select_all" name="sunday_enabled" class="form-check-input">Select All
								  </label>
								</div>
							</th>
						</tr>
						</thead>
						<tbody>
							@php($day_no = 1)
							@foreach(config('global.hourly') as $key=>$value)
							<tr>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="monday_all_times_{{ $day_no }}" name="monday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="tuesday_all_times_{{ $day_no }}" name="tuesday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="wednesday_all_times_{{ $day_no }}" name="wednesday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="thursday_all_times_{{ $day_no }}" name="thursday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="friday_all_times_{{ $day_no }}" name="friday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="saturday_all_times_{{ $day_no }}" name="saturday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
								<td>
									<div class="form-check-inline">
									  <label class="form-check-label">
										<input type="checkbox" id="sunday_all_times_{{ $day_no }}" name="sunday_times[]" class="form-check-input" value="{{ $key }}">{{ $value }}
									  </label>
									</div>
								</td>
							</tr>
							@php($day_no++)
							@endforeach
						</tbody>
					</table>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							@if($key == old('status'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>

<script>  
	$(document).ready(function(){
		$('#monday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#monday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#monday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
		
		$('#tuesday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#tuesday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#tuesday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
		
		$('#wednesday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#wednesday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#wednesday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
		
		$('#thursday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#thursday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#thursday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
		
		$('#friday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#friday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#friday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
		
		$('#saturday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#saturday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#saturday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
		
		$('#sunday_select_all').click(function(event) {   
			if(this.checked) {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#sunday_all_times_' + i ).prop('checked', this.checked)
				}
			}
			else {
				var i;
				for (i = 1; i <= 24; i++) {
					 $( '#sunday_all_times_' + i ).prop('checked', false)
				}
			}
		}); 
	});
</script>          
@endsection