@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>
	{{--
	<div class="row">
		<div class="col-xl-3 col-md-6">
			<div class="card bg-primary text-white mb-4">
				<div class="card-body">Primary Card</div>
				<div class="card-footer d-flex align-items-center justify-content-between">
					<a class="small text-white stretched-link" href="#">View Details</a>
					<div class="small text-white"><i class="fas fa-angle-right"></i></div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-md-6">
			<div class="card bg-warning text-white mb-4">
				<div class="card-body">Warning Card</div>
				<div class="card-footer d-flex align-items-center justify-content-between">
					<a class="small text-white stretched-link" href="#">View Details</a>
					<div class="small text-white"><i class="fas fa-angle-right"></i></div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-md-6">
			<div class="card bg-success text-white mb-4">
				<div class="card-body">Success Card</div>
				<div class="card-footer d-flex align-items-center justify-content-between">
					<a class="small text-white stretched-link" href="#">View Details</a>
					<div class="small text-white"><i class="fas fa-angle-right"></i></div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-md-6">
			<div class="card bg-danger text-white mb-4">
				<div class="card-body">Danger Card</div>
				<div class="card-footer d-flex align-items-center justify-content-between">
					<a class="small text-white stretched-link" href="#">View Details</a>
					<div class="small text-white"><i class="fas fa-angle-right"></i></div>
				</div>
			</div>
		</div>
	</div>
	--}}
	<div class="row">
		<div class="col-xl-10">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-area mr-1"></i>Total Sales ({{ $past_7_days_date }} - {{ $today_date }}) : RM{{ $total_sales }}</div>
				<div class="card-body"><canvas id="salesChart" width="100%" height="40"></canvas></div>
			</div>
		</div>
		{{--
		<div class="col-xl-6">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-area mr-1"></i>Area Chart Example</div>
				<div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
			</div>
		</div>
		<div class="col-xl-6">
			<div class="card mb-4">
				<div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Bar Chart Example</div>
				<div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
			</div>
		</div>
		--}}
	</div>
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Sales ({{ $past_7_days_date }} - {{ $today_date }})</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Customer</th>
							<th>Paid Amount (RM)</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Order ID</th>
							<th>Customer</th>
							<th>Paid Amount (RM)</th>
						</tr>
					</tfoot>
					<tbody>
						@foreach($paid_orders as $paid_order)
						<tr>
							<td>{{ $paid_order->id }}</td>
							<td>{{ $paid_order->user->name }}</td>
							<td>{{ $paid_order->price }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Total Amount</th>
                        <th>Commision to Lavo</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Total Amount</th>
                        <th>Commision to Lavo</th>
                    </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>MYR {{ $total_sales }}</td>
{{--                            number_format(100.30, 2, '.', '')--}}
                            <td>MYR {{ number_format($total_sales * ($comm/100), 2, '.', '')  }}</td>
                        </tr>
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
@endsection
