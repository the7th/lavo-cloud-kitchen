@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Edit Profile</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Edit Profile</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('profile.edit') }}" method="POST">
				@csrf
				@if(Auth::user()->type == 'chef')
				<div class="form-group">
					<label for="brand_name">Restaurant/Brand Name:</label>
					<input type="text" class="form-control" id="brand_name" name="brand_name" value="{{ old('brand_name', $data->brand_name) }}" required>
				</div>
				
				<div class="form-group">
					<label for="cuisine_types">Cuisine Types:</label>
					<select class="form-control selectpicker" multiple="multiple" id="cuisine_types" name="cuisine_types[]" required>
						@foreach($cuisine_types as $key=>$value)
							@if($key == old('cuisine_types'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@elseif( !old('cuisine_types') && in_array($key, $data->cuisineTypes->pluck('cuisine_type_id')->toArray()) )
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				@endif
				
				<div class="form-group">
					<label for="name">Full Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name', $data->name) }}" required>
				</div>
				
				
				<div class="form-group">
					<label for="name">Contact Number:</label>
					<input type="number" class="form-control" id="contact_number" name="contact_number" value="{{ old('contact_number', $data->contact_number) }}" required>
				</div>
				
				<div class="form-group">
					<label for="email">Email address:</label>
					<input type="email" class="form-control" id="email" name="email" value="{{ old('email', $data->email) }}" required>
				</div>
				
				@if(Auth::user()->type == 'chef')
				<div class="form-group">
					<label for="name">Position:</label>
					<input type="text" class="form-control" id="position" name="position" value="{{ old('position', $data->position) }}" required>
				</div>
				
				<div class="form-group">
					<label for="brand_name">Base Type:</label>
					<select class="form-control" id="base_type" name="base_type" required>
						<option value="" selected>Nothing Selected</option>
						@foreach(['Company','Home'] as $value)
							@if($value == old('base_type'))
								<option value="{{ $value }}" selected>{{ $value }}</option>
							@elseif(!old('base_type') && $value == $data->base_type)
								<option value="{{ $value }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $value }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div id="company_info">
					<div class="form-group">
						<label for="company">Company Name:</label>
						@if($data->chefCompany)
							<input type="text" class="form-control" id="company" name="company_name" value="{{ old('company_name', $data->chefCompany->company_name) }}" required>
						@else
							<input type="text" class="form-control" id="company" name="company_name" value="{{ old('company_name') }}" required>
						@endif
					</div>
					
					<div class="form-group">
						<label for="company">Company Registration Number:</label>
						@if($data->chefCompany)
							<input type="text" class="form-control" id="company_registration_number" name="company_registration_number" value="{{ old('company_registration_number', $data->chefCompany->company_registration_number) }}" required>
						@else
							<input type="text" class="form-control" id="company_registration_number" name="company_registration_number" value="{{ old('company_registration_number') }}" required>
						@endif
					</div>
				</div>
				@endif
				
				<div class="form-group">
					<label for="password">Reset Password:</label>
					<input type="password" class="form-control" placeholder="Enter password" name="password" id="password">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
 
<script>  
$(document).ready(function(){
	if ( $('#base_type').val() == 'Company')
	{
		$("#company_info").show();
	}
	else
	{
		$("#company_info").hide();
	}
	
    $('#base_type').on('change', function() {
		if ( this.value == 'Company')
		{
			$("#company_info").show();
		}
		else
		{
			$("#company_info").hide();
		}
    });
});
</script>  
@endsection