@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('menus.index') }}">Menus</a></li>
		<li class="breadcrumb-item active">Create Menu</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create Menu</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('menus.create') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="menu_category">Menu Categories:</label>
					<select class="form-control" id="menu_category" name="menu_category" required>
						@foreach($menu_categories as $key=>$value)
							@if($key == old('menu_category'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
				</div>
				
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea class="form-control" rows="5" id="description" name="description" required>{{ old('description') }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="price">Price:</label>
					<input type="number" class="form-control" id="price" name="price" step="0.01" value="{{ old('price') }}" required>
				</div>
				{{--
				<div class="form-group">
					<label for="settings">Meal Settings:</label>
				</div>
				
				<div class="table-responsive">  
					<table class="table table-bordered" id="meal_dynamic_field">  
						<tr>  
							<td><input type="text" name="meal_setting_types[]" placeholder="Enter setting type" class="form-control type_list" /></td>  
							<td>
								<table class="table table-bordered" id="meal_dynamic_item">  
									<tr>  
										<td>
											<input type="text" name="meal_type_items_1_1" placeholder="Enter type item" class="form-control type_items" />
										</td>
										<td>
											<input type="number" placeholder="Enter item price" class="form-control" id="meal_type_item_prices" name="meal_type_item_prices_1_1" step="0.01">
										</td>
										<td>
											<button type="button" name="meal_add_item" id="meal_add_item" class="btn btn-success">+</button>
										</td>
									<tr>
								</table>	  
							</td>  
							<td><button type="button" name="meal_add" id="meal_add" class="btn btn-success">Add More</button></td>  
						</tr>  
					</table>  
				</div>
				
				<div class="form-group">
					<label for="settings">Settings:</label>
				</div>
				
				<div class="table-responsive">  
					<table class="table table-bordered" id="dynamic_field">  
						<tr>  
							<td><input type="text" name="setting_types[]" placeholder="Enter setting type" class="form-control type_list" /></td>  
							<td>
								<table class="table table-bordered" id="dynamic_item">  
									<tr>  
										<td>
											<input type="text" name="type_items_1_1" placeholder="Enter type item" class="form-control type_items" />
										</td>
										<td>
											<input type="number" placeholder="Enter item price" class="form-control" id="type_item_prices" name="type_item_prices_1_1" step="0.01">
										</td>
										<td>
											<button type="button" name="add_item" id="add_item" class="btn btn-success">+</button>
										</td>
									<tr>
								</table>	  
							</td>  
							<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
						</tr>  
					</table>  
				</div>
				--}}
				<div class="form-group">
					<label for="cover_image">Cover Image</label>
					<input type="file" class="form-control-file" id="cover_image" name="cover_image" required>
				</div>
				
				<div class="form-group">
					<label for="images">Image (maximum 3):</label>
					<input type="file" class="form-control-file" id="images" name="images[]" multiple required>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							@if($key == old('status'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>   

<script> 
$(document).ready(function(){      
	var i=1;  
	
	

	$('#add').click(function(){  
		i++;  
		var dynamic_field_string = '<tr id="row'+i+'" class="dynamic-added">' +
					'<td><input type="text" name="setting_types[]" placeholder="Enter setting type" class="form-control type_list" /></td>' +
					'<td>' +
					'<table class="table table-bordered" id="dynamic_item'+i+'">' +
					'<tr id="item_row'+i+'_1">' +  
						'<td>' +
							'<input type="text" name="type_items_'+i+'_1" placeholder="Enter type item" class="form-control type_items" />' +
						'</td>' +
						'<td>' +
							'<input type="number" placeholder="Enter item price" class="form-control" id="type_item_prices" name="type_item_prices_'+i+'_1" step="0.01">' +
						'</td>' +
						'<td>' +
							'<button type="button" name="add_item_'+i+'_1" id="add_item_'+i+'_1" class="btn btn-success">+</button>' +
						'</td>' +
					'<tr>' +
					'</table>' +
					'</td>' +
					'<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Remove</button></td>' +
					'<tr>';
		$('#dynamic_field').append(dynamic_field_string); 

		var j=1;  
		$('#add_item_'+i+'_'+j).click(function(){  
			j++;  
			var dynamic_item_string = '<tr id="item_row_'+i+'_'+j+'" class="dynamic-added">' +
						'<td><input type="text" name="type_items_'+i+'_'+j+'" placeholder="Enter type item" class="form-control type_item_list" /></td>' +
						'<td><input type="number" placeholder="Enter item price" class="form-control" id="type_item_prices" name="type_item_prices_'+i+'_'+j+'" step="0.01"></td>' +
						'<td><button type="button" name="remove" id="'+i+'_'+j+'" class="btn btn-danger item_btn_remove">X</button></td>' +
						'<tr>';
			$('#dynamic_item'+i).append(dynamic_item_string);  
		});  


		$(document).on('click', '.item_btn_remove', function(){  
			var button_id = $(this).attr("id");   
			$('#item_row_'+button_id+'').remove();  
		});  
	});  


	$(document).on('click', '.btn_remove', function(){  
		var button_id = $(this).attr("id");   
		$('#row'+button_id+'').remove();  
	});  
	
	var j=1;  
	$('#add_item').click(function(){  
		j++;  
		var dynamic_item_string = '<tr id="item_row_'+j+'" class="dynamic-added">' +
					'<td><input type="text" name="type_items_1_'+j+'" placeholder="Enter type item" class="form-control type_item_list" /></td>' +
					'<td><input type="number" placeholder="Enter item price" class="form-control" id="type_item_prices" name="type_item_prices_1_'+j+'" step="0.01"></td>' +
					'<td><button type="button" name="remove" id="'+j+'" class="btn btn-danger item_btn_remove">X</button></td>' +
					'<tr>';
		$('#dynamic_item').append(dynamic_item_string);  
	});  


	$(document).on('click', '.item_btn_remove', function(){  
		var button_id = $(this).attr("id");   
		$('#item_row_'+button_id+'').remove();  
	});  
	
	
	//meal_Settings
	var i=1;  
	
	

	$('#meal_add').click(function(){  
		i++;  
		var dynamic_field_string = '<tr id="meal_row'+i+'" class="meal_dynamic-added">' +
					'<td><input type="text" name="meal_setting_types[]" placeholder="Enter setting type" class="form-control type_list" /></td>' +
					'<td>' +
					'<table class="table table-bordered" id="meal_dynamic_item'+i+'">' +
					'<tr id="item_row'+i+'_1">' +  
						'<td>' +
							'<input type="text" name="meal_type_items_'+i+'_1" placeholder="Enter type item" class="form-control type_items" />' +
						'</td>' +
						'<td>' +
							'<input type="number" placeholder="Enter item price" class="form-control" id="meal_type_item_prices" name="meal_type_item_prices_'+i+'_1" step="0.01">' +
						'</td>' +
						'<td>' +
							'<button type="button" name="meal_add_item_'+i+'_1" id="meal_add_item_'+i+'_1" class="btn btn-success">+</button>' +
						'</td>' +
					'<tr>' +
					'</table>' +
					'</td>' +
					'<td><button type="button" name="meal_remove" id="'+i+'" class="btn btn-danger meal_btn_remove">Remove</button></td>' +
					'<tr>';
		$('#meal_dynamic_field').append(dynamic_field_string); 

		var j=1;  
		$('#meal_add_item_'+i+'_'+j).click(function(){  
			j++;  
			var dynamic_item_string = '<tr id="meal_item_row_'+i+'_'+j+'" class="meal_dynamic-added">' +
						'<td><input type="text" name="meal_type_items_'+i+'_'+j+'" placeholder="Enter type item" class="form-control meal_type_item_list" /></td>' +
						'<td><input type="number" placeholder="Enter item price" class="form-control" id="meal_type_item_prices" name="meal_type_item_prices_'+i+'_'+j+'" step="0.01"></td>' +
						'<td><button type="button" name="meal_remove" id="'+i+'_'+j+'" class="btn btn-danger meal_item_btn_remove">X</button></td>' +
						'<tr>';
			$('#meal_dynamic_item'+i).append(dynamic_item_string);  
		});  


		$(document).on('click', '.meal_item_btn_remove', function(){  
			var button_id = $(this).attr("id");   
			$('#meal_item_row_'+button_id+'').remove();  
		});  
	});  


	$(document).on('click', '.meal_btn_remove', function(){  
		var button_id = $(this).attr("id");   
		$('#meal_row'+button_id+'').remove();  
	});  
	
	var j=1;  
	$('#meal_add_item').click(function(){  
		j++;  
		var dynamic_item_string = '<tr id="meal_item_row_'+j+'" class="meal_dynamic-added">' +
					'<td><input type="text" name="meal_type_items_1_'+j+'" placeholder="Enter type item" class="form-control meal_type_item_list" /></td>' +
					'<td><input type="number" placeholder="Enter item price" class="form-control" id="meal_type_item_prices" name="meal_type_item_prices_1_'+j+'" step="0.01"></td>' +
					'<td><button type="button" name="meal_remove" id="'+j+'" class="btn btn-danger meal_item_btn_remove">X</button></td>' +
					'<tr>';
		$('#meal_dynamic_item').append(dynamic_item_string);  
	});  


	$(document).on('click', '.meal_item_btn_remove', function(){  
		var button_id = $(this).attr("id");   
		$('#meal_item_row_'+button_id+'').remove();  
	});  
}); 
	
var image_limit = 3;
$('#images').change(function(){
	var files = $(this)[0].files;
	if(files.length > image_limit){
		alert("You can only select max "+image_limit+" images.");
		$('#images').val('');
		return false;
	}else{
		return true;
	}
});
</script>       
@endsection