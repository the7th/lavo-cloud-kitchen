@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('menu-setting-secondary-sub-items.index',$menu_setting_sub_item->id) }}">Menu Setting Sub Items</a></li>
		<li class="breadcrumb-item active">Edit Setting Secondary Sub-Item ({{ $data->menuSettingSubItem->name }} - {{ $data->name }})</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create Setting Secondary Sub-Item ({{ $data->menuSettingSubItem->name }} - {{ $data->name }})</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('menu-setting-secondary-sub-items.edit',$data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Name: *</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name',$data->name) }}" required>
				</div>
				
				<div class="form-group">
					<label for="description">Price: </label>
					<input type="number" placeholder="Enter item price" class="form-control" id="price" name="price" step="0.01" value="{{ old('price',$data->price) }}">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>         
@endsection