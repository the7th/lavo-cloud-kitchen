@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('menu-setting-items.index',$menu_setting->id) }}">Menu Setting Items</a></li>
		<li class="breadcrumb-item active">Edit Setting Item ({{ $menu_setting->menu->name }} - {{ $menu_setting->name }})</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Edit Setting Item ({{ $menu_setting->menu->name }} - {{ $menu_setting->name }})</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('menu-setting-items.edit',$data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Name: *</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name',$data->name) }}" required>
				</div>
				
				<div class="form-group">
					<label for="description">Price: </label>
					<input type="number" placeholder="Enter item price" class="form-control" id="price" name="price" step="0.01" value="{{ old('name',$data->price) }}">
				</div>
				
				<div class="form-group">
					<label for="description">Description: *</label>
					<textarea class="form-control" rows="5" id="description" name="description" required>{{ old('description',$data->description) }}</textarea>
				</div>
				
				@if($data->image_path)
				<div class="form-group{{ $errors->has('images') ? ' has-danger' : '' }}">
					<table class="table table-bordered text-center">
						<tr>
							<th>Image</th>
							<th>Action</th>
						</tr>
						<tr>
							<td>
								<a href="{{ env('APP_URL').'/'.$data->image_path.'/'.$data->image_filename }}" target="_blank"><img src="{{ env('APP_URL').'/'.$data->image_path.'/'.$data->image_filename }}" class="img-thumbnail" width="15%" height="auto"></a>
							</td>
							<td>
								<div class="form-check-inline">
								  <label class="form-check-label">
									<input type="checkbox" name="delete_image" class="form-check-input" value="{{ $data->id }}">Delete
								  </label>
								</div>
							</td>
						</tr>
					</table>
				</div>
				@endif
				
				<div class="form-group">
					@if($data->image_path)
					<label for="images">Replace Image</label>
					<input type="file" class="form-control-file" id="images" name="image">
					@else
					<label for="images">Add Image *</label>
					<input type="file" class="form-control-file" id="images" name="image" required>
					@endif
					
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>         
@endsection