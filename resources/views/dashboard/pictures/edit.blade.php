@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('pictures.index') }}">Pictures</a></li>
		<li class="breadcrumb-item active">Edit Picture</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Edit Picture</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('pictures.edit',$data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="start_date">Start Date:</label>
					<input type="date" class="form-control" id="start_date" name="start_date" value="{{ old('start_date',$data->start_date) }}">
				</div>
				
				<div class="form-group">
					<label for="end_date">End Date:</label>
					<input type="date" class="form-control" id="end_date" name="end_date" value="{{ old('end_date',$data->end_date) }}">
				</div>
				
				<div class="form-group">
					<label for="url">URL:</label>
					<input type="text" class="form-control" id="url" name="url" value="{{ old('url',$data->url) }}">
				</div>
				
				<div class="form-group">
					<img src="{{ env('APP_URL') }}/{{ $data->path }}/{{ $data->filename }}" class="img-thumbnail" width="10%" height="auto">
					<a href="{{ env('APP_URL') }}/{{ $data->path }}/{{ $data->filename }}" >View</a>
				</div>
				
				<div class="form-group">
					<label for="images">Replace Image</label>
					<input type="file" class="form-control-file" id="images" name="image">
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status', $data->status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>         
@endsection