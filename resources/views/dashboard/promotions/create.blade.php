@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('promotions.index') }}">Promotions</a></li>
		<li class="breadcrumb-item active">Create Promotion</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create Promotion</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('promotions.create') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="title">Title:</label>
					<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" required>
				</div>
				
				<div class="form-group">
					<label for="type">Type:</label>
					<select class="form-control" id="type" name="type" required>
						@foreach(config('global.promotion_types') as $key=>$value)
							@if($key == old('type'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group" id="free_delivery_for" style="display:none;">
					<label for="free_delivery_type">Free Delivery Type:</label>
					<select class="form-control" id="free_delivery_type" name="free_delivery_type">
						@foreach(['cuisine'=>'Cuisine','brand'=>'Brand'] as $key=>$value)
							@if($key == old('free_delivery_type'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group" id="cuisine" style="display:none;">
					<label for="cuisine_ref_id">Cuisine:</label>
					<select class="form-control" id="cuisine_ref_id" name="cuisine_ref_id">
						@foreach($menu_categories as $key=>$value)
							@if($key == old('type'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group" id="brand" style="display:none;">
					<label for="brand_ref_id">Brand:</label>
					<select class="form-control" id="brand_ref_id" name="brand_ref_id">
						@foreach($chef_companies as $key=>$value)
							@if($key == old('type'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group" id="rate_type_div" style="display:none;">
					<label for="rate_type">Rate Type:</label>
					<select class="form-control" id="rate_type" name="rate_type">
						@foreach(config('global.rate_types') as $key=>$value)
							@if($key == old('type'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group" id="rate_value_div" style="display:none;">
					<label for="rate_value">Rate Value:</label>
					<input type="number" class="form-control" id="rate_value" name="rate_value" value="{{ old('rate_value') }}" step="0.01">
				</div>
				
				<div class="form-group" id="free_delivery_rate_value_div" style="display:none">
					<label for="free_delivery_rate_value">Rate Value (Percentage):</label>
					<input type="number" class="form-control" id="free_delivery_rate_value" name="free_delivery_rate_value" value="{{ old('free_delivery_rate_value') }}" step="0.01">
				</div>
				
				<div class="form-group">
					<label for="start_date">Start Date:</label>
					<input type="date" class="form-control" id="start_date" name="start_date" value="{{ old('start_date') }}" required>
				</div>
				
				<div class="form-group">
					<label for="end_date">End Date:</label>
					<input type="date" class="form-control" id="end_date" name="end_date" value="{{ old('end_date') }}" required>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							@if($key == old('status'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>

<script>  
$(document).ready(function(){
    $('#type').on('change', function() {
		if ( this.value == 'cuisine')
		{
			$("#cuisine").show();
			$("#brand").hide();
			$("#free_delivery_rate_value_div").hide();
			$("#rate_type_div").show();
			$("#rate_value_div").show();
			$('#rate_type').prop('required',true);
			$('#rate_value').prop('required',true);
		}
		else if ( this.value == 'brand')
		{
			$("#brand").show();
			$("#cuisine").hide();
			$("#free_delivery_rate_value_div").hide();
			$("#rate_type_div").show();
			$("#rate_value_div").show();
			$('#rate_type').prop('required',true);
			$('#rate_value').prop('required',true);
		}
		else if ( this.value == 'free_delivery')
		{
			$("#cuisine").show();
			$("#brand").hide();
			$("#rate_type_div").hide();
			$("#rate_value_div").hide();
			$("#rate_value_div").hide();
			$("#free_delivery_for").show();
			/*
			$("#free_delivery_rate_value").show();
			$('#free_delivery_rate_value').prop('required',true);
			*/
			$('#rate_type').prop('required',false);
			$('#rate_value').prop('required',false);
		}
		else
		{
			$("#cuisine").hide();
			$("#brand").hide();
			$("#rate_type_div").hide();
			$("#rate_value_div").hide();
			/*
			$("#free_delivery_rate_value_div").hide();
			*/
		}
    });
	
	$('#free_delivery_type').on('change', function() {
		if ( this.value == 'cuisine')
		{
			$("#cuisine").show();
			$("#brand").hide();
			$('#cuisine_ref_id').prop('required',true);
		}
		else
		{
			$("#cuisine").hide();
			$("#brand").show();
			$('#brand_ref_id').prop('required',true);
		}
    });
});
</script>
</script>         
@endsection