@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('promo-codes.index') }}">Promo Codes</a></li>
		<li class="breadcrumb-item active">Edit Promo Code</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Edit Promo Code</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('promo-codes.edit',$data->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="code">Code:</label>
					<input type="text" class="form-control" id="code" name="code" value="{{ old('code',$data->code) }}" required>
				</div>
				
				<div class="form-group">
					<label for="discount_type">Discount Type:</label>
					<select class="form-control" id="discount_type" name="discount_type" required>
						@foreach(config('global.discount_types') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('discount_type', $data->discount_type) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="discount_value">Discount Value:</label>
					<input type="number" class="form-control" id="discount_value" name="discount_value" value="{{ old('discount_value',$data->discount_value) }}" step="0.01" required>
				</div>
				
				<div class="form-group">
					<label for="redemption_amount">Redemption Amount:</label>
					<input type="number" class="form-control" id="redemption_amount" name="redemption_amount" value="{{ old('redemption_amount',$data->redemption_amount) }}" required>
				</div>
				
				<div class="form-group">
					<label for="start_date">Start Date:</label>
					<input type="date" class="form-control" id="start_date" name="start_date" value="{{ old('start_date',$data->start_date) }}" required>
				</div>
				
				<div class="form-group">
					<label for="end_date">End Date:</label>
					<input type="date" class="form-control" id="end_date" name="end_date" value="{{ old('end_date',$data->end_date) }}" required>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('status', $data->status) ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>         
@endsection