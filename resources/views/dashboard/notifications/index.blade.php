@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Notifications </li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Notifications</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		<div class="card-body">
			<form class="form-inline" action="{{ route('notifications.index') }}">
				{!! Form::text('keyword', $request->keyword, array('placeholder' => 'Keyword','class' => 'form-control mb-3 mr-sm-3')) !!}   
				{!! Form::select('status', [''=>'Select Status','pending'=>'Pending','published'=>'Published'],$request->status, array('class' => 'form-control mb-3 mr-sm-3')) !!}
				{{--
				{!! Form::select('user_type', [''=>'Select Type','employee'=>'Employee','non-employee'=>'Non-Employee'],$request->user_type, array('class' => 'form-control mb-3 mr-sm-3')) !!}
				--}}
				{!! Form::date('date', $request->date, array('class' => 'form-control mb-3 mr-sm-3')) !!}   
				<button type="submit" class="btn btn-primary  mb-3 mr-sm-3">Submit</button>
			 </form>
		</div>
		<div class="card-body overflow-auto">
			<table class="table">
				<thead>
					<tr>
						<th style="width: 20%">Title</th>
						<th style="width: 50%">Message</th>
						<th style="width: 10%">Status</th>
						<th style="width: 10%">Created At</th>
						<th style="width: 10%"></th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $value)
					<tr>
						<td>{{ $value->title }}</td>
						<td>{{ $value->message }}</td>
						<td class="text-{{ $value->status == 'published' ? 'success':'danger' }}">{{ strtoupper($value->status) }}</td>
						<td>{{ $value->created_at }}</td>
						<td>
							<div class="dropdown">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									Action
								</button>
								<div class="dropdown-menu">
									@if($value->status != 'published')
									<a class="dropdown-item" href="{{ route('notifications.edit', $value->id) }}">Edit</a>
									{{--
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#activationModal_{{ $value->id }}">{{ $value->status != 'active' ? 'Activate' : 'Deactivate' }}</a>
									--}}
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#publishModal_{{ $value->id }}">Publish</a>
									@endif
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#deleteModal_{{ $value->id }}">Delete</a>
								</div>
							</div>
						</td>
					</tr>
					{{--
					<div class="modal fade" id="activationModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">{{ $value->status != 'active' ? 'Activate' : 'Deactivate' }} Record?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">Select "{{ $value->status != 'active' ? 'Activate' : 'Deactivate' }}" below if you are ready to {{ $value->status != 'active' ? 'approve' : 'deactivate' }} the record.</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<form id="delete-form" action="{{ route('notifications.activation',$value->id) }}" method="POST" style="display: none;">
									@csrf
									<button type="submit" class="btn btn-danger" name="status" value="{{ $value->status != 'active' ? 'active' : 'inactive' }}">{{ $value->status != 'active' ? 'Activate' : 'Deactivate' }}</button>
								</form>
							</div>
						  </div>
						</div>
					</div>
					--}}
					<div class="modal fade" id="publishModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">Publish Record?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">Select "Publish" below if you are ready to publish the record.</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<form id="delete-form" action="{{ route('notifications.publish',$value->id) }}" method="POST" style="display: none;">
									@csrf
									<button type="submit" class="btn btn-danger">Publish</button>
								</form>
							</div>
						  </div>
						</div>
					</div>
					<div class="modal fade" id="deleteModal_{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-header">
							  <h5 class="modal-title" id="exampleModalLabel">Delete Record?</h5>
							  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							  </button>
							</div>
							<div class="modal-body">Select "Delete" below if you are ready to delete the record.</div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<form id="delete-form" action="{{ route('notifications.delete',$value->id) }}" method="POST" style="display: none;">
									@csrf
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</div>
						  </div>
						</div>
					</div>
				@endforeach
				</tbody>
			</table>
		</div>
		{!!$data->appends(Request::only('keyword','user_type','date'))->links() !!}
	</div>
</div>
                
@endsection