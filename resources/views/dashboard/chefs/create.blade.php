@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{ route('chefs.index') }}">Chefs</a></li>
		<li class="breadcrumb-item active">Create Chef</li>
	</ol>
	
	<div class="card mb-4">
		<div class="card-header"><i class="fas fa-table mr-1"></i>Create Chef</div>
		@if (session('message'))
			<div class="alert {{ Session::get('alert-class') }}">{{ session('message') }}</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				   @foreach ($errors->all() as $error)
					 <li>{{ $error }}</li>
				   @endforeach
				</ul>
			</div>
		@endif
		<div class="card-body">
			<form action="{{ route('chefs.create') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="brand_name">Accout Type:</label>
					<select class="form-control" id="chef_account_type" name="chef_account_type" required>
						<option value="" selected>Nothing Selected</option>
						@foreach(['public' => 'Public', 'private' => 'Private'] as $key => $value)
							@if($key == old('chef_account_type'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="brand_name">Restaurant/Brand Name:</label>
					<input type="text" class="form-control" id="brand_name" name="brand_name" value="{{ old('brand_name') }}" required>
				</div>
				
				<div class="form-group">
					<label for="cuisine_types">Cuisine Types:</label>
					<select class="form-control selectpicker" multiple="multiple" id="cuisine_types" name="cuisine_types[]" required>
						@foreach($cuisine_types as $key=>$value)
							@if($key == old('cuisine_types'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="name">Full Name:</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
				</div>
				
				<div class="form-group">
					<label for="name">Contact Number:</label>
					<input type="number" class="form-control" id="contact_number" name="contact_number" value="{{ old('contact_number') }}" required>
				</div>
				
				<div class="form-group">
					<label for="email">Email address:</label>
					<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
				</div>
				
				<div class="form-group">
					<label for="name">Position:</label>
					<input type="text" class="form-control" id="position" name="position" value="{{ old('position') }}" required>
				</div>
				
				<div class="form-group">
					<label for="brand_name">Base Type:</label>
					<select class="form-control" id="base_type" name="base_type" required>
						<option value="" selected>Nothing Selected</option>
						@foreach(['Company','Home'] as $value)
							@if($value == old('base_type'))
								<option value="{{ $value }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $value }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				
				<div id="company_info">
					<div class="form-group">
						<label for="company">Company Name:</label>
						<input type="text" class="form-control" id="company" name="company_name" value="{{ old('company_name') }}">
					</div>
					
					<div class="form-group">
						<label for="company">Company Registration Number:</label>
						<input type="text" class="form-control" id="company_registration_number" name="company_registration_number" value="{{ old('company_registration_number') }}">
					</div>
				</div>
				
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea class="form-control" rows="5" id="description" name="description" required>{{ old('description') }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="url">URL:</label>
					<input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}" required>
				</div>
				
				<div class="form-group">
					<label for="delivery_time_id">Delivery Time Schedule:</label>
					<select class="form-control"id="delivery_time_id" name="delivery_time_id">
						@foreach($delivery_times as $key=>$value)
							<option value="{{ $key }}" {{ $key == old('delivery_time_id') ? 'selected':'' }}>{{ $value }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="form-group">
					<label for="images">Image</label>
					<input type="file" class="form-control-file" id="images" name="image" required>
				</div>
				
				<div class="form-group">
					<label for="password">Password:</label>
					<input type="password" class="form-control" placeholder="Enter password" name="password" id="password" required>
				</div>
				
				<div class="form-group">
					<label for="status">Status:</label>
					<select class="form-control" id="status" name="status" required>
						@foreach(config('global.statuses') as $key=>$value)
							@if($key == old('status'))
								<option value="{{ $key }}" selected>{{ $value }}</option>
							@else
								<option value="{{ $key }}">{{ $value }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>   

<script>  
$(document).ready(function(){
	$("#company_info").hide();
	
    $('#base_type').on('change', function() {
		if ( this.value == 'Company')
		{
			$("#company_info").show();
			$('#company').prop('required',true);
			$('#company_registration_number').prop('required',true);
		}
		else
		{
			$("#company_info").hide();
		}
    });
});
</script>       
@endsection