<p>Thank you for your order, {{ $name }} </p>

<p>You can track the progress of your order at: <a href="https://lavocloudkitchen.com/frontend/consumer/order/progress?id={{$order->id}}">https://lavocloudkitchen.com/frontend/consumer/order/progress?id={{$order->id}}</a>

</p>
