@extends('layouts.shop')

@section('content')

<div class="card mt-4">
	<img class="card-img-top img-fluid" src="http://placehold.it/900x400" alt="">
	<div class="card-body">
		<h3 class="card-title">Product Name</h3>
		<h4>$24.99</h4>
		<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium animi perspiciatis molestias iure, ducimus!</p>
		<span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
		4.0 stars
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">
				<div class="input-group">
					<span class="input-group-btn">
						<button type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">
						  <span class="glyphicon glyphicon-minus">-</span>
						</button>
					</span>
					<input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
					<span class="input-group-btn">
						<button type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
							<span class="glyphicon glyphicon-plus">+</span>
						</button>
					</span>
				</div>
			</div>
		</div>
    </div>
	<div class="card-footer">
		<a class="btn btn-primary pull-right" href="{{ url('checkout') }}"
		   onclick="event.preventDefault();
						 document.getElementById('checkout-form').submit();">
			{{ __('Add Order') }}
		</a>

		<form id="checkout-form" action="{{ url('checkout') }}" method="POST" style="display: none;">
			@csrf
		</form>
	</div>
</div>
<!-- /.card -->

<!--
<div class="card card-outline-secondary my-4">
  <div class="card-header">
	Product Reviews
  </div>
  <div class="card-body">
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
	<small class="text-muted">Posted by Anonymous on 3/1/17</small>
	<hr>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
	<small class="text-muted">Posted by Anonymous on 3/1/17</small>
	<hr>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
	<small class="text-muted">Posted by Anonymous on 3/1/17</small>
	<hr>
	<a href="#" class="btn btn-success">Leave a Review</a>
  </div>
</div>
<!-- /.card -->

<script>
$(document).ready(function(){

var quantitiy=0;
   $('.quantity-right-plus').click(function(e){
        
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
            
            $('#quantity').val(quantity + 1);

          
            // Increment
        
    });

     $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
      
            // Increment
            if(quantity>0){
            $('#quantity').val(quantity - 1);
            }
    });
    
});
</script>

@endsection