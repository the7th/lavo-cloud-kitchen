<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Lavo - Cloud Kitchen | Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ env('APP_URL') }}/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/main.css">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/style.css">

</head>

<body>
<div class="bgheroimage menufontstyle">
    <!-- Image and text -->
    <nav class="navbar navbar-expand-lg navbar-light bg-spegradient" style="position: absolute;z-index: 9;width: 100%;">
        <a class="navbar-brand brandset" href="main"><img src="{{ env('APP_URL') }}/img/logo.svg" width="152" height="95" class="d-inline-block align-top" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Kitchen <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="delivery">Food Delivery</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Events</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="herocontainer">
        <div class="row" style="height: 100%;margin-right: 0px;margin-left: 0px;">
            <div class="col-md-6 herocontentleft">
                <div id="admitleft" class="admitstyle">I'M <br /> HUNGRY</div>
                <div class="herodiscLeft hide">Satisfy your cravings by getting the food you love from your favourite Chef</div>
                <div><a href="delivery" id="heroBtnLeft" id="heroBtnLeft" class="heroBtnRed">Order Now <img id="heroBtnImgLeft" src="{{ env('APP_URL') }}/img/next_light.svg" class="d-inline-block align-top" alt=""></a>
                </div>
            </div>
            <div class="col-md-6 herocontentright">
                <div style="text-align: right;">
                    <div class="admitstyleactive">I'M  <br /> A COOK</div>
                    <div>
                        <div class="herodiscRight hide">Rent a fully equipped commercial kitchen so that you can start your food business quickly.</div>
                    </div>
                    <div style="width: 100%;"><a href="#" class="heroBtnWhite">Know More <img src="{{ env('APP_URL') }}/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<img src="assets/img/banner1.png" alt="">-->
</div>
<content>
    <div class="mastarea"><h1 class="mastquote">Let's build bridges, <span style="color: #C01F25">together.</span></h1></div>
    <div class="infoarea">
        <div class="container row infoboxstyle">
            <div class="col-md-6"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/img/ourstory.png" class="d-inline-block align-top" alt=""></div>
            <div class="col-md-6" style="position: relative;">
                <span class="markingstyles">Discoveries</span>
                <h1 class="titleinfo">Our Story</h1>
                <p class="textcontent">It all starts with a digital platform. Through this platform, Lavo  Cloud Kitchen connects people who are looking for good food with home cooks and foodpreneurs who are ready to provide them. The best part is, we also take care of the delivery so businesses focus on producing good meals and they are delivered fresh to the customers right to their doorstep – a truly advantageous arrangement for all!</p>
                <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                <img src="{{ env('APP_URL') }}/img/element1.png" style="position: absolute;    right: -100px;bottom: -70px;" alt="">
            </div>
        </div>
        <div class="container container-color" style="padding: 100px 0px 100px 0px"></div>


        <div id="carouselExampleIndicators" class="carousel slide carouselbg" data-interval="false">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner carousel-inner-custom">
                <div class="carousel-item active">
                    <div class="container row infoboxstyle">
                        <div class="col-md-6"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/img/eventstn.png" class="d-inline-block align-top" alt=""></div>
                        <div class="col-md-6">
                            <img src="{{ env('APP_URL') }}/img/element2.png" style="position: absolute;right: -140px;top: -50px;" alt="">
                            <span class="markingstyles">Discover</span>
                            <h1 class="titleinfo">Upcoming Events</h1>
                            <p class="textcontent">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                            <div class="event-name">Weekend Bakery Workshop
                            </div>
                            <div class="event-time">December 1 | Saturday | 1PM
                            </div>
                            <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                            <img src="{{ env('APP_URL') }}/img/element3.png" style="position: absolute;    right: -100px;bottom: -70px;" alt="">
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container row infoboxstyle">
                        <div class="col-md-6"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/img/eventstn.png" class="d-inline-block align-top" alt=""></div>
                        <div class="col-md-6">
                            <img src="{{ env('APP_URL') }}/img/element2.png" style="position: absolute;right: -140px;top: -50px;" alt="">
                            <span class="markingstyles">Discover</span>
                            <h1 class="titleinfo">Upcoming Events</h1>
                            <p class="textcontent">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                            <div class="event-name">Weekend Bakery Workshop
                            </div>
                            <div class="event-time">December 1 | Saturday | 1PM
                            </div>
                            <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                            <img src="{{ env('APP_URL') }}/img/element3.png" style="position: absolute;    right: -100px;bottom: -70px;" alt="">
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container row infoboxstyle">
                        <div class="col-md-6"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/img/eventstn.png" class="d-inline-block align-top" alt=""></div>
                        <div class="col-md-6">
                            <img src="{{ env('APP_URL') }}/img/element2.png" style="position: absolute;right: -140px;top: -50px;" alt="">
                            <span class="markingstyles">Discover</span>
                            <h1 class="titleinfo">Upcoming Events</h1>
                            <p class="textcontent">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                            <div class="event-name">Weekend Bakery Workshop
                            </div>
                            <div class="event-time">December 1 | Saturday | 1PM
                            </div>
                            <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                            <img src="{{ env('APP_URL') }}/img/element3.png" style="position: absolute;    right: -100px;bottom: -70px;" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev c-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon c-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next c-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon c-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>



    </div>
</content>
<footer class="footer">
    <div class="row footerarea">
        <div class="col-md-6" style="margin: auto;width: 50%;"><span class="copyrightclause">Copyright © 2020 LAVO CLOUD KITCHEN All rights reserved.</span></div>
        <div class="col-md-6" style="margin: auto;width: 50%;text-align: right;"><span class="followus">Follow Us </span>
            <img src="{{ env('APP_URL') }}/img/facebook.png" class="d-inline-block" alt="">
            <img src="{{ env('APP_URL') }}/img/youtube.png" class="d-inline-block" alt="">
            <img src="{{ env('APP_URL') }}/img/instagram.png" class="d-inline-block" alt="">
            <img src="{{ env('APP_URL') }}/img/twitter.png" class="d-inline-block" alt="">
            <img src="{{ env('APP_URL') }}/img/linkedin.png" class="d-inline-block" alt="">


            <a id="movetotopbtn"></a>

        </div>
    </div>
</footer>
<!-- JS here -->

<!-- Jquery, Popper, Bootstrap -->
<script src="{{ env('APP_URL') }}/js/jquery-3.5.1.min.js"></script>
<script src="{{ env('APP_URL') }}/js/bootstrap.min.js"></script>
<script src="{{ env('APP_URL') }}/js/main.js"></script>
<script type = "text/javascript" language = "javascript">
    $(document).ready(function() {

        $('.herocontentleft').hover(

            function () {
                $('.herodiscRight').addClass( "hide" );
                $('.herodiscLeft').removeClass('hide');

                //admitleft
                $('#admitleft').addClass( "admitstyleactive" ).removeClass('admitstyle');

                $('#heroBtnLeft').addClass( "heroBtnWhite" ).removeClass('heroBtnRed');
                $('#heroBtnImgLeft').prop('src', "{{ env('APP_URL') }}/img/next_red.svg");

                //selectionmarker
                $('.herocontentleft').addClass( "selectionmarker" );
                $('.herocontentright').removeClass('selectionmarker');
            },

            function () {
                $('.herodiscLeft').addClass( "hide" );
                $('.herodiscRight').removeClass('hide');

                //admitleft
                $('#admitleft').addClass( "admitstyle" ).removeClass('admitstyleactive');

                $('#heroBtnLeft').addClass( "heroBtnRed" ).removeClass('heroBtnWhite');
                $('#heroBtnImgLeft').prop('src', "{{ env('APP_URL') }}/img/next_light.svg");

                //selectionmarker
                $('.herocontentright').addClass( "selectionmarker" );
                $('.herocontentleft').removeClass('selectionmarker');
            }
        );


    });
</script>
</body>
</html>
