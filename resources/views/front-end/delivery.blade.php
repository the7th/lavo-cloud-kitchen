<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Lavo - Cloud Kitchen | Food Delivery</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{ env('APP_URL') }}/cloud-kitchen/assets/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="{{ env('APP_URL') }}/cloud-kitchen/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ env('APP_URL') }}/cloud-kitchen/assets/css/main.css">
  <link rel="stylesheet" href="{{ env('APP_URL') }}/cloud-kitchen/assets/css/style.css">
  <link rel="stylesheet" href="{{ env('APP_URL') }}/cloud-kitchen/assets/css/deliverystyle.css">

</head>

<body>
    <div class="menufontstyle">
    <!-- Image and text -->
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand brandset" href="main"><img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/logo.svg" width="152" height="95" class="d-inline-block align-top" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">

            <div style="width: 100%">
             <form class="form-inline my-2 my-lg-0" style="padding-left: 40px;">
                <input class="form-control form-control-lg mr-lg-2 searchbox" style="width: 100%;" type="search" placeholder="Search">
            </form>
            </div>

            <div style="float: right;min-width: 800px">
            <ul class="navbar-nav ml-auto">

              <li class="nav-item ">
                <a class="nav-link" href="#">Kitchen</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Food Delivery <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Events</a>
              </li>
              <li class="nav-item">
                <a href="#" class="loginBtnRed"> <img class="login-logo"  src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/login.svg"  alt=""> Login </a>
              </li>
            </ul>
            </div>

          </div>
        </nav>
        
      </div> 

<div class="highlight-area">
      <div class="container pad-top-30"></div>
<!-- C A R O U S E L START-->

      <div id="carousel-delivery-highlight" class="carousel slide" data-interval="false">

      <div class="carousel-inner deli-hl-carousel-inner-custom">
        <div class="carousel-item active">
            <div class="container row">
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img1.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img2.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img3.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img4.png" class="d-inline-block align-top" alt=""></div>
            </div>
          </div>
        <div class="carousel-item">
            <div class="container row">
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img1.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img2.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img3.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img4.png" class="d-inline-block align-top" alt=""></div>
            </div>
          </div>
        <div class="carousel-item">
            <div class="container row">
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img1.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img2.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img3.png" class="d-inline-block align-top" alt=""></div>
                <div class="col-md-3"><img class="img-fluid mx-auto" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/highlight_img4.png" class="d-inline-block align-top" alt=""></div>
            </div>
          </div>

      </div>
      <a class="carousel-control-prev deli-hl-c-prev" href="#carousel-delivery-highlight" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon deli-hl-c-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next deli-hl-c-next" href="#carousel-delivery-highlight" role="button" data-slide="next">
        <span class="carousel-control-next-icon deli-hl-c-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
<!-- C A R O U S E L   END -->
    <div class="container pad-top-30"></div>
<!-- C A R O U S E L START-->

<div id="carousel-delivery-banner" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators ca-indicator-position carousel-indicators-custom">
    <li data-target="#carousel-delivery-banner" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-delivery-banner" data-slide-to="1"></li>
    <li data-target="#carousel-delivery-banner" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/banner_img2.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/banner_img2.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/banner_img2.png" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carousel-delivery-banner" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-delivery-banner" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



<!-- C A R O U S E L   END -->
  <div class="container pad-top-30"></div>
  <div class="container">
    <div class="title-area">
        <h1 class="title-font"><span class="title-markingstyles">Most</span> Popular</h1>
    </div>
  </div>

<!-- C A R O U S E L    M O S T   P O P U L A R   START-->

      <div id="carousel-delivery-mostpopular" class="carousel slide" data-interval="false">

      <div class="carousel-inner deli-hl-carousel-inner-custom">
        <div class="carousel-item active">
<!--M O S T   P O P U L A R   )NE-->
          <div class="container row">
              <div class="col-md-3 card-pad">
                <div class="card card-mp mb-2">
                    <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/nasi_lemak.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Nasi Lemak Kak Nor</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/curry_mee.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">ABCurry Mee</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/kolo_mee.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Sarawak Kolo Mee</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/roti_john.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Roti John</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
          </div>
<!--M O S T   P O P U L A R   ONE-->
          </div>
        <div class="carousel-item">
<!--M O S T   P O P U L A R   TWO-->
          <div class="container row">
              <div class="col-md-3 card-pad">
                <div class="card card-mp mb-2">
                  <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/nasi_lemak.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Nasi Lemak Kak Nor</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/curry_mee.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">ABCurry Mee</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/kolo_mee.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Sarawak Kolo Mee</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/roti_john.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Roti John</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
          </div>
<!--M O S T   P O P U L A R   TWO-->
          </div>
        <div class="carousel-item">
<!--M O S T   P O P U L A R   THREE-->
          <div class="container row">
              <div class="col-md-3 card-pad">
                <div class="card card-mp mb-2">
                  <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/nasi_lemak.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Nasi Lemak Kak Nor</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/curry_mee.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">ABCurry Mee</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/kolo_mee.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Sarawak Kolo Mee</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
              <div class="col-md-3 card-pad">
                      <div class="card card-mp mb-2">
                        <a class="card-link" href="#LINKTOFOODDETAILS">
                        <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/roti_john.png" alt="Card image cap">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-6 card-title-2">Roti John</div>
                            <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                          </div>
                          <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                          <h4 class="card-title">RM 10.90</h4>
                        </div>
                      </a>
                </div>
              </div>
          </div>
<!--M O S T   P O P U L A R   THREE-->
          </div>

      </div>
      <a class="carousel-control-prev deli-hl-c-prev" href="#carousel-delivery-mostpopular" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon deli-hl-c-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next deli-hl-c-next" href="#carousel-delivery-mostpopular" role="button" data-slide="next">
        <span class="carousel-control-next-icon deli-hl-c-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
<!-- C A R O U S E L    M O S T   P O P U L A R   END-->


<div class="container pad-top-30"></div>
  <div class="container row" style="justify-content: center;">
    <a href="#" class="heroBtnRed">View More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_light.svg" class="d-inline-block align-top more-btn-pad" alt=""></a>
  </div>
<div class="container pad-top-30"></div>
</div>


</div>
<div class="content-area content-area-custom">
  <div class="container pad-top-30"></div>
  <div class="container">
    <div class="title-area">
        <h1 class="title-font"><span class="title-markingstyles">Our</span> Chefs</h1>
    </div>
  </div>
<!-- C A R O U S E L   O U R   C H E F S   START-->

      <div id="carousel-delivery-ourchefs" class="carousel slide" data-interval="false">

      <div class="carousel-inner deli-hl-carousel-inner-custom">
        <div class="carousel-item active">
<!--O U R   C H E F S   )NE-->
  <div class="container row">
      <div class="col-md-3 card-pad">
        <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef1.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Great Food</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef2.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Eat Clean & Healthy</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef3.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Wonder Kitchen</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef4.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">A little more kitchen</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
  </div>
<!--M O S T   P O P U L A R   ONE-->
          </div>
        <div class="carousel-item">
<!--O U R   C H E F S   TWO-->
  <div class="container row">
      <div class="col-md-3 card-pad">
        <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef1.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Great Food</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef2.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Eat Clean & Healthy</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef3.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Wonder Kitchen</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef4.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">A little more kitchen</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
  </div>
<!--O U R   C H E F S   TWO-->
          </div>
        <div class="carousel-item">
<!--O U R   C H E F S   THREE-->
  <div class="container row">
      <div class="col-md-3 card-pad">
        <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef1.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Great Food</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef2.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Eat Clean & Healthy</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef3.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">Wonder Kitchen</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling-red">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/chef4.png" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-chef-title-2">A little more kitchen</h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <div class="menufontstyle"><a href="#" class="linkstyles">Know More <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_red.svg" class="d-inline-block align-top" alt=""></a></div>
                </div>
        </div>
      </div>
  </div>
<!--O U R   C H E F S   THREE-->
          </div>

      </div>
      <a class="carousel-control-prev deli-hl-c-prev" href="#carousel-delivery-ourchefs" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon deli-hl-c-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next deli-hl-c-next" href="#carousel-delivery-ourchefs" role="button" data-slide="next">
        <span class="carousel-control-next-icon deli-hl-c-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

<!-- C A R O U S E L   O U R   C H E F S   END-->

<div class="container pad-top-30"></div>
  <div class="container row" style="justify-content: center;">
    <a href="#" class="heroBtnRed">View All <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/next_light.svg" class="d-inline-block align-top more-btn-pad" alt=""></a>
  </div>
<div class="container pad-top-30"></div>

<!-- A L L   C U I S I N E   A R E A   START-->
<div class="content-area content-area-custom">
  <div class="container pad-top-30"></div>
  <div class="container">
    <div class="title-area">
        <h1 class="title-font"><span class="title-markingstyles">All</span> Cuisine</h1>
    </div>
  </div>
  <!--R O W   O N E-->
  <div class="container row">
      <div class="col-md-3 card-pad">
        <div class="card mb-2 card-styling">
          <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/nasi_kukus.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Nasi Kukus Campur</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>
                  
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/noodle1.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Mayzi Noodle House</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/noodle2.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Pork Noodle</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>                  
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/fried_chicken.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Korean Fried Chicken</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>      
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
  </div>
  <div class="container pad-top-20"></div>
  <!--R O W   T W O-->
  <div class="container row">
      <div class="col-md-3 card-pad">
        <div class="card mb-2 card-styling">
          <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/nasi_kukus.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Nasi Kukus Campur</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>
                  
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/noodle1.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Mayzi Noodle House</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/noodle2.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Pork Noodle</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>                  
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/fried_chicken.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Korean Fried Chicken</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>      
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
  </div>
  <div class="container pad-top-20"></div>
  <!--R O W   T H R E E-->
  <div class="container row">
      <div class="col-md-3 card-pad">
        <div class="card mb-2 card-styling">
          <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/nasi_kukus.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Nasi Kukus Campur</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>
                  
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/noodle1.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Mayzi Noodle House</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/noodle2.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Pork Noodle</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>                  
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
      <div class="col-md-3 card-pad">
              <div class="card mb-2 card-styling">
                <a class="card-link" href="#LINKTOFOODDETAILS">
                <img class="card-img-top" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/fried_chicken.png" alt="Card image cap">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 card-title-2">Korean Fried Chicken</div>
                    <div class="col-md-6 score-card"><img class="score-img" src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/delivery/score.svg" alt="Score"> <span class="font-lavo-red">4.3</span>/5</div>
                  </div>      
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit.</p>
                  <h4 class="card-title">RM 10.90</h4>
                </div>
              </a>
        </div>
      </div>
  </div>
  <div class="container pad-top-20"></div>
<!-- A L L   C U I S I N E   A R E A   END-->
<div class="container pad-top-30"></div>
<footer class="footer">
  <div class="row footerarea">
    <div class="col-md-6" style="margin: auto;width: 50%;"><span class="copyrightclause">Copyright © 2020 LAVO CLOUD KITCHEN All rights reserved.</span></div>
    <div class="col-md-6" style="margin: auto;width: 50%;text-align: right;"><span class="followus">Follow Us </span>
      <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/facebook.png" class="d-inline-block" alt="">
      <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/youtube.png" class="d-inline-block" alt="">
      <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/instagram.png" class="d-inline-block" alt="">
      <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/twitter.png" class="d-inline-block" alt="">
      <img src="{{ env('APP_URL') }}/cloud-kitchen/assets/img/linkedin.png" class="d-inline-block" alt="">


      <a id="movetotopbtn"></a>

    </div>
  </div>
</footer>
    <!-- JS here -->

    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{ env('APP_URL') }}/cloud-kitchen/assets/js/jquery-3.5.1.min.js"></script>
    <script src="{{ env('APP_URL') }}/cloud-kitchen/assets/js/bootstrap.min.js"></script>
    <script src="{{ env('APP_URL') }}/cloud-kitchen/assets/js/main.js"></script>


</body>
</html>