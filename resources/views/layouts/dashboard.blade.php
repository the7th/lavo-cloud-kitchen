<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content=""><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - {{ config('app.name', 'Laravel') }}</title>
        <link href="{{ env('APP_URL') }}/startbootstrap-sb-admin/css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>

		<!--Jquery -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.html">{{ config('app.name', 'Laravel') }}</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
			{{--
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
			--}}
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
						{{--
						<a class="dropdown-item" href="#">Settings</a>
						<a class="dropdown-item" href="#">Activity Log</a>

                        <div class="dropdown-divider"></div>
						--}}
						<a class="dropdown-item" href="{{ route('profile.edit') }}">Profile</a>
						<a class="dropdown-item" href="{{ route('logout') }}"
						   onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();">
							{{ __('Logout') }}
						</a>

						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <a class="nav-link" href="{{ route('home') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
							</a>
							@if(Auth::user()->isAdmin())
							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKitchenAreas" aria-expanded="false" aria-controls="collapseKitchenAreas"
                                ><div class="sb-nav-link-icon"><i class="fa fa-map"></i></div>
                                Kitchen Areas
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseKitchenAreas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('kitchen-areas.index') }}">View Areas</a>
									<a class="nav-link" href="{{ route('kitchen-areas.create') }}">Create</a>
								</nav>
                            </div>
							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKitchens" aria-expanded="false" aria-controls="collapseKitchens"
                                ><div class="sb-nav-link-icon"><i class="fa fa-university"></i></div>
                                Kitchens
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseKitchens" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('kitchens.index') }}">View Kitchens</a>
									<a class="nav-link" href="{{ route('kitchens.create') }}">Create</a>
								</nav>
                            </div>
							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMenues" aria-expanded="false" aria-controls="collapseMenues"
                                ><div class="sb-nav-link-icon"><i class="fa fa-shopping-basket"></i></div>
                                Menus
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseMenues" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('menus.index') }}">View All</a>
									{{--
									<a class="nav-link" href="{{ route('menus.create') }}">Create</a>
									--}}
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseChefs" aria-expanded="false" aria-controls="collapseChefs">
								<div class="sb-nav-link-icon"><i class="fa fa-address-card"></i></div>
                                Chefs
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseChefs" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('chefs.index') }}">View All</a>
									<a class="nav-link" href="{{ route('chefs.create') }}">Create</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConsumers" aria-expanded="false" aria-controls="collapseConsumers"
                                ><div class="sb-nav-link-icon"><i class="fa fa-users"></i></div>
                                Consumers
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseConsumers" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('consumers.index') }}">View All</a>
									{{--
									<a class="nav-link" href="{{ route('consumers.create') }}">Create</a>
									--}}
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBookings" aria-expanded="false" aria-controls="collapseBookings"
                                ><div class="sb-nav-link-icon"><i class="fa fa-calendar-check"></i></div>
                                Bookings
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseBookings" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('bookings.index') }}">View All</a>
									<a class="nav-link" href="{{ route('fullcalendar.index') }}">Calendar</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOrders" aria-expanded="false" aria-controls="collapseOrders"
                                ><div class="sb-nav-link-icon"><i class="fa fa-cart-plus"></i></div>
                                Orders
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseOrders" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('orders.index') }}">View All</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReviews" aria-expanded="false" aria-controls="collapseReviews" ><div class="sb-nav-link-icon"><i class="fa fa-comment"></i></div>
                                Reviews
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseReviews" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('reviews.index') }}">View All</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBanners" aria-expanded="false" aria-controls="collapseBanners"
                                ><div class="sb-nav-link-icon"><i class="fa fa-newspaper"></i></div>
                                Banners
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseBanners" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('banners.index') }}">View All</a>
									<a class="nav-link" href="{{ route('banners.create') }}">Create</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePictures" aria-expanded="false" aria-controls="collapsePictures"
                                ><div class="sb-nav-link-icon"><i class="fa fa-image"></i></div>
                                Pictures
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapsePictures" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('pictures.index') }}">View All</a>
									<a class="nav-link" href="{{ route('pictures.create') }}">Create</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePromotions" aria-expanded="false" aria-controls="collapsePromotions"
                                ><div class="sb-nav-link-icon"><i class="fa fa-gift"></i></div>
                                Promotions
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapsePromotions" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('promotions.index') }}">View All</a>
									<a class="nav-link" href="{{ route('promotions.create') }}">Create</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePromoCodes" aria-expanded="false" aria-controls="collapsePromoCodes"
                                ><div class="sb-nav-link-icon"><i class="fa fa-tags"></i></div>
                                Promo Codes
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapsePromoCodes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('promo-codes.index') }}">View All</a>
									<a class="nav-link" href="{{ route('promo-codes.create') }}">Create</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDeliveryTimes" aria-expanded="false" aria-controls="collapseDeliveryTimes"
                                ><div class="sb-nav-link-icon"><i class="fa fa-clock"></i></div>
                                Delivery Times
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseDeliveryTimes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('delivery-times.index') }}">View All</a>
									<a class="nav-link" href="{{ route('delivery-times.create') }}">Create</a>
								</nav>
                            </div>
							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCommission" aria-expanded="false" aria-controls="collapseCommission"
                                ><div class="sb-nav-link-icon"><i class="fa fa-coins"></i></div>
                                Commission
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseCommission" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('commission-categories.index') }}">View All Categories</a>
									<a class="nav-link" href="{{ route('commission-categories.create') }}">Create Categories</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseStaticPages" aria-expanded="false" aria-controls="collapseStaticPages"
                                ><div class="sb-nav-link-icon"><i class="fa fa-newspaper"></i></div>
                                Static Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseStaticPages" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('static-pages.index') }}">View All</a>
									<a class="nav-link" href="{{ route('static-pages.create') }}">Create</a>
								</nav>
                            </div>

                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFinance" aria-expanded="false" aria-controls="collapseFinance"
                            ><div class="sb-nav-link-icon"><i class="fa fa-newspaper"></i></div>
                                Finance
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseFinance" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{ route('users.finance.transactions') }}">View Transaction</a>
                                    <a class="nav-link" href="{{ route('users.wallet') }}">View Wallets</a>
                                </nav>
                            </div>

							@endif

							@if(Auth::user()->type == 'chef')
							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMenus" aria-expanded="false" aria-controls="collapseMenus"
                                ><div class="sb-nav-link-icon"><i class="fa fa-shopping-basket"></i></div>
                                Menus
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseMenus" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('menus.index') }}">View All</a>
									<a class="nav-link" href="{{ route('menus.create') }}">Create</a>
								</nav>
                            </div>

							<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOrders" aria-expanded="false" aria-controls="collapseOrders"
                                ><div class="sb-nav-link-icon"><i class="fa fa-cart-plus"></i></div>
                                Orders
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
							</a>
                            <div class="collapse" id="collapseOrders" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
									<a class="nav-link" href="{{ route('orders.index') }}">View All</a>
								</nav>
                            </div>
							@endif
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        {{ strtoupper(auth::user()->type) }}
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    @yield('content')
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2019</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ env('APP_URL') }}/startbootstrap-sb-admin/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="{{ env('APP_URL') }}/startbootstrap-sb-admin/assets/demo/chart-area-demo.js"></script>
		
		@if(isset($paid_orders))
		<script> 
			// Set new default font family and font color to mimic Bootstrap's default styling
			Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
			Chart.defaults.global.defaultFontColor = '#292b2c';

			// Area Chart Example
			var ctx = document.getElementById("salesChart");
			var labels_array = [<?php echo $dates; ?>];
			var data_array = [<?php echo $data; ?>];
			
			var myLineChart = new Chart(ctx, {
			  type: 'line',
			  data: {
				labels: labels_array,
				datasets: [{
				  label: "RM",
				  lineTension: 0.3,
				  backgroundColor: "rgba(2,117,216,0.2)",
				  borderColor: "rgba(2,117,216,1)",
				  pointRadius: 5,
				  pointBackgroundColor: "rgba(2,117,216,1)",
				  pointBorderColor: "rgba(255,255,255,0.8)",
				  pointHoverRadius: 5,
				  pointHoverBackgroundColor: "rgba(2,117,216,1)",
				  pointHitRadius: 50,
				  pointBorderWidth: 2,
				  data: data_array,
				}],
			  },
			  options: {
				scales: {
				  xAxes: [{
					time: {
					  unit: 'date'
					},
					gridLines: {
					  display: false
					},
					ticks: {
					  maxTicksLimit: 7
					}
				  }],
				  yAxes: [{
					ticks: {
					  min: 0,
					  max: {{ $max_data }},
					  maxTicksLimit: 5
					},
					gridLines: {
					  color: "rgba(0, 0, 0, .125)",
					}
				  }],
				},
				legend: {
				  display: false
				}
			  }
			});
		</script> 
		@endif
		
        <script src="{{ env('APP_URL') }}/startbootstrap-sb-admin/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ env('APP_URL') }}/startbootstrap-sb-admin/assets/demo/datatables-demo.js"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script>

		<!-- Summernote-->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

		<script>
		$(document).ready(function() {
			$('.summernote').summernote({
				height:300,
			});
		});
		</script>
    </body>
</html>
