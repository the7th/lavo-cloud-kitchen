<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cloud Kitchen Booking Receipt</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
	<table class="table table-bordered">
    <thead>
      <tr class="table-primary">
        <td colspan="2" class="text-center">Official Receipt</td>
      </tr>
	  <tr class="table-default">
        <td>Receive from: Cloud Kitche Sdn Bhd</td>
        <td>Date: {{ Carbon\Carbon::now()->format('Y-m-d') }}</td>
      </tr>
      </thead>
    </table>
    <table class="table table-bordered">
    <thead>
	  <tr class="table-default">
        <th width="80%">Particulars</th>
        <th width="20%">Amount (RM)</th>
      </tr>
      </thead>
      <tbody>
        <tr>
            <td>
				{{ $booking->kitchen->name }} ({{ $booking->kitchenArea->name }}) <br/>
				Booking Type : {{ ucfirst($booking->booking_type) }} <br/>
				Period : {{ $booking->booking_start_date }} - {{ $booking->booking_end_date }}<br />
				@if( $booking->booking_type == 'session' )
				<small>
					<b>Timeslots :</b> <br />
					@foreach(json_decode($booking->session_timeslots,true) as $timeslot)
					{{ $timeslot }}<br />
					@endforeach
				</small>
				@else
					<small> 09:00 - 21:00 </small>
				@endif
			</td>
            <td>{{ $booking->price }}</td>
        </tr>
		<tr class="table-default">
			<td class="text-right">Total</td>
			<td>{{ $booking->price }}</td>
		  </tr>
      </tbody>
    </table>
	<table class="table table-bordered">
    <thead>
	  <tr class="table-default">
        <td>	
			Receiver : {{ $booking->user->name }}<br />
			Email Address : {{ $booking->user->email }}<br />
			Contact Number : {{ $booking->user->contact_number }}<br />
		</td>
      </tr>
      </thead>
    </table>
  </body>
</html>