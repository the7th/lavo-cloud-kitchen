@extends('layouts.shop')

@section('content')
<div class="card mt-4">
  <div class="card-body">
	<h3 class="card-title">Checkout</h3>
	<h4>Total Amount : RM24.99</h4>
	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium animi perspiciatis molestias iure, ducimus!</p>
  </div>
</div>
<div class="card mt-4">
	<br />
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title">
							<div class="row">
								<div class="col-md-6">
									<h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
								</div>
								<div class="col-md-6">
									<button type="button" class="btn btn-primary pull-right" onclick="event.preventDefault();
													 document.getElementById('shopping-form').submit();">
										<i class="fa fa-cart-plus"></i> Continue shopping
									</button>

									<form id="shopping-form" action="{{ url('/') }}" method="get" style="display: none;">
										@csrf
									</form>
								</div>
							</div>
						</div>
					</div>
					<br />
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2"><img class="img-responsive" src="http://placehold.it/100x70">
							</div>
							<div class="col-md-4">
								<h4 class="product-name"><strong>Product name</strong></h4><h4><small>Product description</small></h4>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-6 text-right my-auto">
										<h6><strong>25.00 <span class="text-muted">x</span></strong></h6>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control input-sm" value="1">
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-danger">
											<i class="fa fa-trash"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<!--
						<hr>
						<div class="row">
							<div class="col-md-2"><img class="img-responsive" width="100%" height="auto" src="http://placehold.it/100x70">
							</div>
							<div class="col-md-4">
								<h4 class="product-name"><strong>Product name</strong></h4><h4><small>Product description</small></h4>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-6 text-right my-auto">
										<h6><strong>25.00 <span class="text-muted">x</span></strong></h6>
									</div>
									<div class="col-md-4">
										<input type="text" class="form-control input-sm" value="1">
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-danger">
											<i class="fa fa-trash"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						-->
					</div>
					<div class="panel-footer">
						<div class="row text-center">
							<div class="col-md-9">
								<h4 class="text-right">Total <strong>$50.00</strong></h4>
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-success btn-block">
									Checkout
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br />
@endsection