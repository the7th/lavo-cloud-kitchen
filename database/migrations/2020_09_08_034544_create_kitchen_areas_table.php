<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKitchenAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchen_areas', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name',255);
			$table->string('address',255);
			$table->string('state',50);
			$table->string('city',50);
			$table->string('postal_code',25);
            $table->string('status',15)->default('inactive');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kitchen_areas');
    }
}
