<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeliveryTimeIdToChefCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chef_companies', function (Blueprint $table) {
            $table->integer('delivery_time_id')->unsigned()->after('company_registration_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chef_companies', function (Blueprint $table) {
            $table->dropColumn('delivery_time_id');
        });
    }
}
