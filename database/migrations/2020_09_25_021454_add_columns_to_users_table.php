<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('image_path',255)->nullable()->after('base_type');
            $table->string('image_filename',255)->nullable()->after('base_type');
            $table->string('url',255)->nullable()->after('base_type');
            $table->string('description')->after('base_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('image_path');
            $table->dropColumn('image_filename');
            $table->dropColumn('url');
            $table->dropColumn('description');
        });
    }
}
