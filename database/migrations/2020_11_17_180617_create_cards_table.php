<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->string('four_digits')->nullable();
            $table->text('token')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('users_wallets', function (Blueprint $table) {
            $table->id();
            $table->decimal('balance', 10,2);
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('financial_txn', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->string('type');
            $table->decimal('amount','10', 2);
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->text('merchant_txn_id')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
        Schema::dropIfExists('users_wallets');
        Schema::dropIfExists('financial_txn');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('merchant_txn_id');
        });

    }
}
