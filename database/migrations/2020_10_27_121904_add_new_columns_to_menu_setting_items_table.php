<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToMenuSettingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_setting_items', function (Blueprint $table) {
            $table->string('image_filename',255)->nullable()->after('price');
            $table->string('image_path',255)->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_setting_items', function (Blueprint $table) {
            $table->dropColumn('image_filename');
            $table->dropColumn('image_filename');
        });
    }
}
