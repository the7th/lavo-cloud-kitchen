<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_times', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title',255);
			$table->string('monday_enabled',15)->default('no');
			$table->string('monday_times')->nullable();
			$table->string('tuesday_enabled',15)->default('no');
			$table->string('tuesday_times')->nullable();
			$table->string('wednesday_enabled',15)->default('no');
			$table->string('wednesday_times')->nullable();
			$table->string('thursday_enabled',15)->default('no');
			$table->string('thursday_times')->nullable();
			$table->string('friday_enabled',15)->default('no');
			$table->string('friday_times')->nullable();
			$table->string('saturday_enabled',15)->default('no');
			$table->string('saturday_times')->nullable();
			$table->string('sunday_enabled',15)->default('no');
			$table->string('sunday_times')->nullable();
            $table->string('status',15)->default('inactive');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_times');
    }
}
