<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLamboTrailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lambo_trails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant_txn_id',50);
            $table->string('jobstatus',20);
            $table->string('jobdescription',100);
            $table->timestamp('jobstatustime',0);
            $table->string('ridername',100);
            $table->string('ridercarplatenumber',50);
            $table->string('ridercontact',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lambo_trails');
    }
}
