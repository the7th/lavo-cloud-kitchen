<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/front-end/main', function () {
    return view('front-end.index');
});

Route::get('/front-end/delivery', function () {
    return view('front-end.delivery');
});

Route::get('/products/show', function () {
    return view('products.show');
});

Route::post('/checkout', function () {
    return view('checkout.show');
});

Route::post('/table', function () {
    return view('dashboard.table');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/page/{title}', 'StaticPageController@show')->name('show');

Route::get('/success-reset-password', function () {
	return view('success-reset-password');
});

Route::group(['prefix' => 'daily-self-declaration', 'as' => 'daily-self-declaration.'], function () {
	Route::get('/show-form', ['as'=>'show_form','uses'=>'DailySelfDeclarationController@showForm']);
	Route::post('/store-form', ['as'=>'store_form','uses'=>'DailySelfDeclarationController@storeForm','middleware' => 'auth:api']);
	Route::get('/successfully-submitted', ['as'=>'successfully_submitted','uses'=>'DailySelfDeclarationController@successfullySubmitted']);
});

Route::group(['middleware' => 'auth'], function () {
	//Profile
	Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
		Route::get('/edit', ['as'=>'edit','uses'=>'ProfileController@edit']);
		Route::post('/edit', ['as'=>'update','uses'=>'ProfileController@update']);
	});

	//Chefs
	Route::group(['prefix' => 'chefs', 'as' => 'chefs.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'ChefController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'ChefController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'ChefController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'ChefController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'ChefController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'ChefController@delete']);
		Route::get('/export', ['as'=>'export','uses'=>'ChefController@export']);
	});

	//Consumers
	Route::group(['prefix' => 'consumers', 'as' => 'consumers.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'ConsumerController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'ConsumerController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'ConsumerController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'ConsumerController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'ConsumerController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'ConsumerController@delete']);
	});

	//Kitchen areas
	Route::group(['prefix' => 'kitchen-areas', 'as' => 'kitchen-areas.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'KitchenAreaController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'KitchenAreaController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'KitchenAreaController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'KitchenAreaController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'KitchenAreaController@update']);
	});

	//Kitchens
	Route::group(['prefix' => 'kitchens', 'as' => 'kitchens.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'KitchenController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'KitchenController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'KitchenController@store'])->middleware('optimizeImages');
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'KitchenController@edit'])->middleware('optimizeImages');
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'KitchenController@update']);
	});

	//Menus
	Route::group(['prefix' => 'menus', 'as' => 'menus.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'MenuController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'MenuController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'MenuController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'MenuController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'MenuController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'MenuController@delete']);
		Route::post('/approval/{id}', ['as'=>'approval','uses'=>'MenuController@approval']);
	});

	//Menu Settings
	Route::group(['prefix' => 'menu-settings', 'as' => 'menu-settings.'], function () {
		Route::get('/{menu_id}', ['as'=>'index','uses'=>'MenuSettingController@index']);
		Route::get('/create/{menu_id}', ['as'=>'create','uses'=>'MenuSettingController@create']);
		Route::post('/create/{menu_id}', ['as'=>'store','uses'=>'MenuSettingController@store'])->middleware('optimizeImages');
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'MenuSettingController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'MenuSettingController@update'])->middleware('optimizeImages');
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'MenuSettingController@delete']);
	});

	//Menu Setting Items
	Route::group(['prefix' => 'menu-setting-items', 'as' => 'menu-setting-items.'], function () {
		Route::get('/{menu_setting_id}', ['as'=>'index','uses'=>'MenuSettingItemController@index']);
		Route::get('/create/{menu_setting_id}', ['as'=>'create','uses'=>'MenuSettingItemController@create']);
		Route::post('/create/{menu_setting_id}', ['as'=>'store','uses'=>'MenuSettingItemController@store'])->middleware('optimizeImages');
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'MenuSettingItemController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'MenuSettingItemController@update'])->middleware('optimizeImages');
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'MenuSettingItemController@delete']);
	});

	//Menu Setting Sub Items
	Route::group(['prefix' => 'menu-setting-sub-items', 'as' => 'menu-setting-sub-items.'], function () {
		Route::get('/{menu_setting_item_id}', ['as'=>'index','uses'=>'MenuSettingSubItemController@index']);
		Route::get('/create/{menu_setting_item_id}', ['as'=>'create','uses'=>'MenuSettingSubItemController@create']);
		Route::post('/create/{menu_setting_item_id}', ['as'=>'store','uses'=>'MenuSettingSubItemController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'MenuSettingSubItemController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'MenuSettingSubItemController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'MenuSettingSubItemController@delete']);
	});

	//Menu Setting Secondary Sub Items
	Route::group(['prefix' => 'menu-setting-secondary-sub-items', 'as' => 'menu-setting-secondary-sub-items.'], function () {
		Route::get('/{menu_setting_sub_item_id}', ['as'=>'index','uses'=>'MenuSettingSecondarySubItemController@index']);
		Route::get('/create/{menu_setting_sub_item_id}', ['as'=>'create','uses'=>'MenuSettingSecondarySubItemController@create']);
		Route::post('/create/{menu_setting_sub_item_id}', ['as'=>'store','uses'=>'MenuSettingSecondarySubItemController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'MenuSettingSecondarySubItemController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'MenuSettingSecondarySubItemController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'MenuSettingSecondarySubItemController@delete']);
	});

	//Bookings
	Route::group(['prefix' => 'bookings', 'as' => 'bookings.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'BookingController@index']);
		Route::get('/calendar', ['as'=>'calendar','uses'=>'BookingController@calendar']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'BookingController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'BookingController@update']);
		Route::get('/pdf-receipt/{id}', ['as'=>'createPDF','uses'=>'BookingController@createPDF']);
	});

	//Orders
	Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'OrderController@index']);
		Route::get('/calendar', ['as'=>'calendar','uses'=>'OrderController@calendar']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'OrderController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'OrderController@update']);
		Route::post('/updateStatus/{id}', ['as'=>'updateStatus','uses'=>'OrderController@updateStatus']);
		Route::post('/refund/{id}', ['as'=>'refund','uses'=>'OrderController@refund']);
	});

	//Order Details
	Route::group(['prefix' => 'order-details', 'as' => 'order-details.'], function () {
		Route::post('/edit', ['as'=>'update','uses'=>'OrderDetailController@update']);
	});

	//Reviews
	Route::group(['prefix' => 'reviews', 'as' => 'reviews.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'ReviewController@index']);
		Route::post('/approval/{id}', ['as'=>'approval','uses'=>'ReviewController@approval']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'ReviewController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'ReviewController@update']);
	});

	//fullcalender
	Route::group(['prefix' => 'fullcalendar', 'as' => 'fullcalendar.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'FullCalendarController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'FullCalendarController@create']);
		Route::post('/update', ['as'=>'update','uses'=>'FullCalendarController@update']);
		Route::get('/delete', ['as'=>'delete','uses'=>'FullCalendarController@delete']);
	});

	//Banners
	Route::group(['prefix' => 'banners', 'as' => 'banners.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'BannerController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'BannerController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'BannerController@store'])->middleware('optimizeImages');
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'BannerController@edit'])->middleware('optimizeImages');
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'BannerController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'BannerController@delete']);
	});

	//Pictures
	Route::group(['prefix' => 'pictures', 'as' => 'pictures.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'PictureController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'PictureController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'PictureController@store'])->middleware('optimizeImages');
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'PictureController@edit'])->middleware('optimizeImages');
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'PictureController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'PictureController@delete']);
	});

	//Promotions
	Route::group(['prefix' => 'promotions', 'as' => 'promotions.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'PromotionController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'PromotionController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'PromotionController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'PromotionController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'PromotionController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'PromotionController@delete']);
	});

	//Promo Codes
	Route::group(['prefix' => 'promo-codes', 'as' => 'promo-codes.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'PromoCodeController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'PromoCodeController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'PromoCodeController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'PromoCodeController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'PromoCodeController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'PromoCodeController@delete']);
	});

	//Delivery Times
	Route::group(['prefix' => 'delivery-times', 'as' => 'delivery-times.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'DeliveryTimeController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'DeliveryTimeController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'DeliveryTimeController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'DeliveryTimeController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'DeliveryTimeController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'DeliveryTimeController@delete']);
	});

	//Commission Categories
	Route::group(['prefix' => 'commission-categories', 'as' => 'commission-categories.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'CommissionCategoryController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'CommissionCategoryController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'CommissionCategoryController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'CommissionCategoryController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'CommissionCategoryController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'CommissionCategoryController@delete']);
	});

	//Static Pages
	Route::group(['prefix' => 'static-pages', 'as' => 'static-pages.'], function () {
		Route::get('/', ['as'=>'index','uses'=>'StaticPageController@index']);
		Route::get('/create', ['as'=>'create','uses'=>'StaticPageController@create']);
		Route::post('/create', ['as'=>'store','uses'=>'StaticPageController@store']);
		Route::get('/edit/{id}', ['as'=>'edit','uses'=>'StaticPageController@edit']);
		Route::post('/edit/{id}', ['as'=>'update','uses'=>'StaticPageController@update']);
		Route::post('/delete/{id}', ['as'=>'delete','uses'=>'StaticPageController@delete']);
	});
});

// redirect to fb
Route::get('auth/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleFacebookCallback');


Route::get('/test', function () {
    return view('checkout.payment');
});

Route::get('/seeAuth', 'SiteController@auth');
Route::get('/siteLogout', 'SiteController@logout');


Route::post('/set-order', 'Api\ConsumerController@setOrder'); // push order into session
Route::get('/set-order', 'Api\ConsumerController@viewSetOrder'); // view order into session
Route::get('/clear-order', 'Api\ConsumerController@clearOrder'); // clear order in session


Route::get('/get-token', 'SiteController@getToken');

Route::get('/start-payment', 'SiteController@testPayment');

Route::post('/payment-redirect', 'SiteController@redirectPayment');
Route::post('/payment-callback', 'SiteController@paymentCallback');

Route::post('/preauth-payment-callback', 'SiteController@paymentCallbackPreAuth');
Route::post('/preauth-payment-redirect', 'SiteController@redirectPreAuth');

// FINANCE
Route::get('/finance/wallet', 'FinanceController@walletIndex')->name('users.wallet');
Route::get('/finance/transactions', 'FinanceController@txnIndex')->name('users.finance.transactions');

Route::get('/access-hash-gen', 'SiteController@accessHashGen');

Route::get('/redirecting', function (\Illuminate\Http\Request $request) {
   return view('redirector');
})->name('redirector');

Route::get('/search_place', 'SiteController@googleAutocomplete');
