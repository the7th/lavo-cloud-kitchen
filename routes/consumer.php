<?php

use Illuminate\Support\Facades\Route;

// THIS IS CONSUMER SIDE

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('frontend', '')->group(function() {
    Route::get('/home', 'Frontend\Landing\LandingController@index')->name('frontend.home');
    Route::get('/register', 'Frontend\Consumer\Account\Register\RegisterController@index')->name('frontend.register');

    Route::prefix('consumer', '')->group(function() {
        Route::get('/', 'Frontend\Consumer\Index\IndexController@index')->name('frontend.consumer');
        Route::get('/search/{searchStr?}', 'Frontend\Consumer\Search\SearchController@index')->name('frontend.consumer.search');
        Route::get('/menulisting', 'Frontend\Consumer\MenuListing\MenuListingController@index')->name('frontend.consumer.menulisting');
        Route::get('/menu/{menuId?}', 'Frontend\Consumer\Menu\MenuController@index')->name('frontend.consumer.menu');
        Route::get('/chef', 'Frontend\Consumer\Chef\ChefController@index')->name('frontend.consumer.chef');

        Route::prefix('promotion', '')->group(function() {
            Route::get('/template', 'Frontend\Consumer\Promotion\Template\TemplateController@index')->name('frontend.consumer.promotion.template');
        });

        Route::prefix('cuisine', '')->group(function() {
            Route::get('/template', 'Frontend\Consumer\Cuisine\Template\TemplateController@index')->name('frontend.consumer.cuisine.template');
        });

        Route::prefix('account', '')->group(function() {
            Route::get('/profile', 'Frontend\Consumer\Account\Profile\ProfileController@index')->name('frontend.consumer.profile');
            Route::get('/gcash', 'Frontend\Consumer\Account\GCash\GCashController@index')->name('frontend.consumer.gcash');
            Route::get('/myorder', 'Frontend\Consumer\Account\Order\OrderController@index')->name('frontend.consumer.myorder');
        });

        Route::prefix('contact', '')->group(function() {
            Route::get('/kitchen', 'Frontend\Consumer\Contact\Kitchen\KitchenController@index')->name('frontend.consumer.contact.kitchen');
            Route::get('/delivery', 'Frontend\Consumer\Contact\Delivery\DeliveryController@index')->name('frontend.consumer.contact.delivery');
        });

        Route::prefix('order', '')->group(function() {
            Route::get('/checkout', 'Frontend\Consumer\Order\Checkout\CheckoutController@index')->name('frontend.consumer.order.checkout');
            Route::get('/failed', 'Frontend\Consumer\Order\Failed\FailedController@index')->name('frontend.consumer.order.failed');
            Route::get('/progress', 'Frontend\Consumer\Order\Progress\ProgressController@index')->name('frontend.consumer.order.progress');
            Route::get('/review', 'Frontend\Consumer\Order\Review\ReviewController@index')->name('frontend.consumer.order.review');
        });

        Route::get('/terms-and-condition', 'Frontend\Consumer\Legal\Term\TermController@index')->name('frontend.consumer.term');
        Route::get('/privacy-policy', 'Frontend\Consumer\Legal\Privacy\PrivacyController@index')->name('frontend.consumer.privacy');
    });

    Route::prefix('chef', '')->group(function(){
        Route::get('/', 'Frontend\Chef\Index\IndexController@index')->name('frontend.chef');
        Route::get('/cubaan', 'Frontend\Chef\Index\IndexController@cubaan')->name('frontend.cubaan');

    });

    Route::get('/about-us', 'Frontend\About\AboutController@index')->name('frontend.about');

    Route::prefix('event', '')->group(function()  {
        Route::get('/', 'Frontend\Event\Index\IndexController@index')->name('frontend.event');
    });


});
