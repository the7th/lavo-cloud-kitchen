<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {

        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
        Route::post('update', 'Api\AuthController@update');
		Route::post('change-password', 'Api\AuthController@changePassword');
		Route::get('address-list', 'Api\AuthController@addressList');
		Route::post('add-address', 'Api\AuthController@addAddress');
		Route::post('delete-address', 'Api\AuthController@deleteAddress');

    });
});

Route::group([
    'prefix' => 'password'
], function () {
    Route::post('forgot', 'Api\Auth\PasswordResetController@create');
    Route::get('find/{token}', 'Api\Auth\PasswordResetController@find');
    Route::post('reset', 'Api\Auth\PasswordResetController@reset');
});

Route::group([
    'prefix' => 'notifications'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('/all', 'Api\NotificationController@index');
        Route::get('/show', 'Api\NotificationController@show');
    });
});



// CONSUMER
Route::group([
    'prefix' => 'consumer',
    'middleware' => ['web']
], function () {

    Route::get('/site-hero-image', 'Api\ConsumerController@siteHeroImage');
    Route::get('/site-news', 'Api\ConsumerController@siteNews');

    Route::get('/list-of-orders-not-checkout-yet', 'Api\ConsumerController@listOfOrdersNotCheckedOutYet');

    Route::get('/banner-delivery-landing-page', 'Api\ConsumerController@bannerDeliveryLandingPage');     //    banner-delivery-landing-page (the small one dekat atas, have parameter how many to output, sort by pageview)
    Route::get('/hero-delivery-landing-page', 'Api\ConsumerController@heroDeliveryLandingPage');
    Route::get('/list-of-foods-w-summary-w-rating-w-price', 'Api\ConsumerController@listOfFoodsWSummaryWRatingWPrice'); // // have parameter how many want to output, sort by page view


    Route::get('/cuisine-list', 'Api\ConsumerController@categoryList');




    Route::get('/food-info', 'Api\ConsumerController@foodInfo'); // get food info

    Route::get('/food-info-options', 'Api\ConsumerController@foodInfoOptions');




    // food info food info options


    Route::get('/list-of-chefs', 'Api\ConsumerController@listOfChefs');

    Route::get('/most-popular-foods', 'Api\ConsumerController@mostPopularFoods'); // // have parameter how many want to output, sort by page view

	Route::get('/list-of-reviews', 'Api\ConsumerController@reviewList');

	Route::get('/static-page-info', 'Api\ConsumerController@staticPageInfo');

//    most-popular-foods

    /*

    food-info-options // set meals // options

    delivery-time-in-delivery-details-in-checkout (dekat checkout)
    check-delivery

    check-voucher

    */

//    Route::get('/list-of-foods', '');


});

Route::group([
    'prefix' => 'consumer'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::post('/create-order', 'Api\ConsumerController@createOrder');
        Route::post('/re-order', 'Api\ConsumerController@reOrder');
		Route::get('/order-checkout', 'Api\ConsumerController@orderCheckout');
        Route::get('/order-list', 'Api\ConsumerController@orderList');
		Route::get('/order-status', 'Api\ConsumerController@orderStatus');
        Route::post('/create-review', 'Api\ConsumerController@createReview');
        Route::get('/promo-code-info', 'Api\ConsumerController@promoCodeInfo');
        Route::get('/cards', 'Api\ConsumerController@cards');
        Route::post('/card-delete', 'Api\ConsumerController@deleteCard');
        Route::post('/preauth-card-start', 'Api\ConsumerController@paymentPreAuth');
        Route::get('/transactions', 'Api\ConsumerController@transaction');
        Route::get('/wallet', 'Api\ConsumerController@userWallet');
        Route::get('/delivery', 'Api\ConsumerController@delivery');
        Route::get('/order-details', 'Api\ConsumerController@orderDetail');
    });
});



// CHEF
Route::group([
    'prefix' => 'chef',
    'middleware' => ['web']
], function () {
    Route::get('/list-of-kitchens', 'Api\ChefController@kitchenList');
});

Route::group([
    'prefix' => 'chef'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('/menu-list', 'Api\ChefController@menuList');
        Route::get('/menu-info', 'Api\ChefController@menuInfo');
        Route::post('/create-booking', 'Api\ChefController@createBooking');
    });
});

// ADMIN
//Sample for triggering  sending email
Route::get('sendmail','EmailController@sendmail');

// LAMBO
Route::post('lambowebhook','Api\LamboController@index');
