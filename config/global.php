<?php

return [
	'yes_or_no' => [
		'' => "None",
		'yes' => "Yes",
		'no' => "No"
    ],
	'statuses' => [
		'' => "None",
		'active' => "Active",
		'inactive' => "Inactive"
    ],
	'payment_statuses' => [
		'unpaid' => "Unpaid",
		'paid' => "Paid",
		'cancelled' => "Cancelled"
    ],
	'approval_statuses' => [
		'pending' => "Pending",
		'approved' => "Approved",
		'rejected' => "Rejected"
    ],
	'departments' => [
		'' => "None",
		'Department A' => "Department A",
		'Department B' => "Department B",
		'Department C' => "Department C",
		'Department D' => "Department D",
		'Department E' => "Department E",
    ],
	'user_types' => [
		'' => "None",
		'consumer' => "Consumer",
		'chef' => "Chef"
    ],
	'kitchen_booking_types' => [
		'' => "None",
		'session' => "Session",
		'weekly' => "Weekly",
		'monthly' => "Monthly"
    ],
	'review_types' => [
		'' => "None",
		'menu' => "Menu",
		'chef' => "Chef"
    ],
	'states' => [
		'' => 'Select State',
		'Johor' => 'Johor',
		'Kedah' => 'Kedah',
		'Kelantan' => 'Kelantan',
		'Kuala Lumpur' => 'Kuala Lumpur',
		'Labuan' => 'Labuan',
		'Melaka' => 'Melaka',
		'Negeri Sembilan' => 'Negeri Sembilan',
		'Pahang' => 'Pahang',
		'Penang' => 'Penang',
		'Perak' => 'Perak',
		'Perlis' => 'Perlis',
		'Sabah' => 'Sabah',
		'Sarawak' => 'Sarawak',
		'Selangor' => 'Selangor',
		'Terengganu' => 'Terengganu'
	],
	'kitchen_categories' => [
		'any' => 'Any',
		'asian' => 'Asian',
		'western' => 'Western',
		'desert' => 'Dessert',
		'bakery' => 'Bakery',
    ],
	'order_statuses' => [
		'pending' => 'Pending',
		'received' => 'Received',
		'preparing' => 'Preparing',
		'completed' => 'Completed',
		'delivering' => 'Delivering',
		'pickup' => 'Ready for Pickup',
		'cancelled' => 'Cancelled'
    ],
	'promotion_types' => [
		'' => 'None',
		'cuisine' => 'Cuisine',
		'brand' => 'Brand',
		'free_delivery' => 'Free Delivery'
    ],
	'rate_types' => [
		'' => 'None',
		'percentage' => 'Percentage',
		'fixed_rate' => 'Fixed Rate'
    ],
	'discount_types' => [
		'' => 'None',
		'percentage' => 'Percentage',
		'fixed_rate' => 'Fixed Rate',
		'free_delivery' => 'Free Delivery'
    ],
	'commission_types' => [
		'' => 'None',
		'percentage' => 'Percentage',
		'fixed_rate' => 'Fixed Rate'
    ],
	'hourly' => [
		'00:00' => '00:00',
		'01:00' => '01:00',
		'02:00' => '02:00',
		'03:00' => '03:00',
		'04:00' => '04:00',
		'05:00' => '05:00',
		'06:00' => '06:00',
		'07:00' => '07:00',
		'08:00' => '08:00',
		'09:00' => '09:00',
		'10:00' => '10:00',
		'11:00' => '11:00',
		'12:00' => '12:00',
		'13:00' => '13:00',
		'14:00' => '14:00',
		'15:00' => '15:00',
		'16:00' => '16:00',
		'17:00' => '17:00',
		'18:00' => '18:00',
		'19:00' => '19:00',
		'20:00' => '20:00',
		'21:00' => '21:00',
		'22:00' => '22:00',
		'23:00' => '23:00'
    ],
];

